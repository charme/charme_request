/*Chat Communications*/
      /*Expanding textarea*/
        $('#chat_message').elastic();
        $('#appointment_chat > .message-left > #msg_detail').hide();
        $('#appointment_chat > .message-right > #msg_detail').hide();

        $('#appointment_chat > li').on('click',$('li'),function(e){
              $(this).find('#msg_detail').fadeToggle('fast');
              console.log(this);
        });

        function showMsgDetails(ele){
          $(ele).find('#msg_detail').fadeToggle('fast');
        }



        /*Send chat message*/        
        $('#send_button').click(function(){
          sendChat();
        });
        
        function sendChat(){
            var message = $('#chat_message').val();
            if (!message) {return false;};
            /*empty input field*/
            $('#chat_message').val('');      
            var id0=$('#appointment_chat > li').last().attr('id');
              if (isNaN(id0)) {id0=0;};
              var id= Number(id0)+1;
              var time=moment().unix();
            var chat_message='<li class="message-left" id="'+id+'">'+
                                '<img src="'+chat_img+'" class="minilogo mdl-shadow--2dp">'+
                                '<div class="message mdl-shadow--2dp">'+
                                  '<p>'+message+'</p>'+                  
                                '</div>'+
                                '<div id="msg_detail" class="text_right">'+
                                  '<small data-livestamp="'+time+'"></small>'+
                                  '<small class="mdl-color-text--green" id="sent_status">&#10003;</small>'+
                                '</div>'+
                              '</li>';
                            $('#appointment_chat').append(chat_message);
                            document.getElementById( 'scrollTo' ).scrollIntoView();                            
            post_chat = new XMLHttpRequest();
              post_chat.onreadystatechange = function(){
                      if (post_chat.readyState==4 && post_chat.status==200) {
                        var chat_response = JSON.parse(post_chat.responseText);
                        //console.log(chat_response);
                        if (chat_response.status=='ok') {
                                  $("p:contains("+message+")").find('#sent_status').append('&#10003;');
                           //$('#appointment_chat').find('#'+chat_response.data.ChatMessage.id).find('#sent_status').append('&#10003;');
                        };

                        if (chat_response.status !=='ok') {
                          ////console.log(chat_response);   
                          /*When message fails to send*/                                          
                        };
                      }
                      if (post_chat.readyState!=4){
                       
                      }
                      if(post_chat.status==500){
                        $('#appointment_chat').find('#'+chat_response.data.ChatMessage.id).find('#sent_status').css('color','red');
                        $('#appointment_chat').find('#'+chat_response.data.ChatMessage.id).find('#sent_status').html('Not Sent');
                       
                      }
                    };
                    post_chat.open("POST",send_chat_url, true);
                    post_chat.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    post_chat.send("message="+message);
          }  
        /*Scroll to bottom of page*/
        var current_url= window.location.href.indexOf('chat');
        if (current_url > -1) {
          $(document).ready(function(){
                setTimeout(function () { goToSecondTab(); }, 2000);
                  function goToSecondTab() {
                    document.getElementById('scrollTo').scrollIntoView();
                    }
          });
        };
      /*End of chat communication*/