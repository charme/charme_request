 $('.update_close').click(function(){
        $('.live_updates').fadeOut('slow');
      });
      var pusher = new Pusher('d9a5d6f1f32449da826d', {
            encrypted: true
      });
      var current_id=$('#current_user').attr('data-id');
      var user_channel='charme-customer-'+current_id;
      var channel = pusher.subscribe(user_channel);

      /*Listen to chat pushes*/
          var chat_app_id=$('#scrollTo').attr('data-id');
          /*
             const CHAT = 'push_chat';
             const APPOINTMENT_START = 'push_appointment_start';
             const APPOINTMENT_STOP = 'push_appointment_stop';
             const APPOINTMENT_CANCEL = 'push_appointment_cancel';
             const APPOINTMENT_RESCHEDULE = 'push_appointment_reschedule';
             const APPOINTMENT_REMINDER = 'push_appointment_reminder';
             const LOCATION = 'push_location';
             const INBOX = 'push_inbox';
          */
          /*--------------------Chat messeages bind--------------------------*/
            channel.bind('push_chat', function(data) {
                console.log(data);
                if (data.appointment_id==chat_app_id && data.sent_by=='supplier') {
                  var chat_message='<li class="message-right">'+
                        '<img src="'+chat_img+'" title="'+data.sender_name+'" class="minilogo mdl-shadow--2dp">'+
                        '<div class="message mdl-shadow--2dp">'+
                          '<p>'+data.message+'</p>'+                  
                        '</div>'+
                        '<div id="msg_detail" class="text_left">'+
                        '<small data-livestamp="'+data.time_sent+'"></small>'+
                        '</div>'+
                      '</li>';
                  $('#appointment_chat').append(chat_message);
                }
                if (data.appointment_id!=chat_app_id) {
                  /*Show alert of message from another customer*/
                  $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+chat_url+data.appointment_id+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            data.sender_name+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            data.message+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast');
                }              
            }); 
          /*-----------------End of chat messages bind---------------------*/
          /*-------Inbox Bind----------*/
          channel.bind('push_inbox', function(data) {
                console.log(data);
                   /*Show alert of an inbox message*/
                  $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+inbox_url+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            data.sent_by+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            shortText(data.message)+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast');      
            }); 
          /*End of inbox binds*/
          /*-------------Location Bind----------*/
          channel.bind('push_location', function(data) {
                console.log(data);
                   /*Plot location on maps*/
                 
            }); 
          /*End of location bind*/
          /*-----------------Appointment Binds, includes start, stop, cancel, reschedule, reminder, location*/
            channel.bind('push_appointment_start', function(data) {
                console.log(data);
                   /*Show alert of an appointment that has started*/
                  $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+appointment_url+data.appointment_id+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            'Appointment'+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            'Your Appointment Just started'+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast');      
            }); 
          /*End of appointment Binds*/

      /*End of listen to chat push*/

      /*Callbacks when pusher is connected, connecting, unavailable,failed and disconnected*/
                  pusher.connection.bind('connected',function(){
                    console.log('pusher connected');
                    $('.chat').css('background-color','#F1F4F5');
                  });

                  pusher.connection.bind('connecting',function(){
                    console.log('pusher connecting');
                    $('#send_button').attr('disabled');
                    $('.chat').css('background-color','#FFF9C4');
                  });

                  pusher.connection.bind('unavailable',function(){
                    console.log('pusher unavailable');
                    $('.chat').css('background-color','#E57373');
                  });

                  pusher.connection.bind('failed',function(){
                    console.log('pusher failed');
                    $('.chat').css('background-color','#E57373');
                  });

                  pusher.connection.bind('disconnected',function(){
                    console.log('pusher disconnected');
                    $('.chat').css('background-color','#E57373');
                  });
      /*End of Callbacks to pusher connected*/      