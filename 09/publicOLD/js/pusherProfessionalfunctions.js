var pusher = new Pusher('d9a5d6f1f32449da826d', {
          encrypted: true
        });
        var current_id=$('#current_user').attr('data-id');
        var user_channel='charme-supplier-'+current_id;
        var channel = pusher.subscribe(user_channel);
        /*-----------------Chat message Bind------------*/
          /*listens to chat messages*/
          var chat_app_id=$('#scrollTo').attr('data-id');
          channel.bind('push_chat', function(data) {
              //console.log(data);
            if (data.appointment_id==chat_app_id && data.sent_by=='customer') {
              var chat_message='<li class="message-right" onclick="showMsgDetails(this)">'+
                      '<img src="'+chat_img+'" title="'+data.sender_name+'" class="minilogo mdl-shadow--2dp">'+
                      '<div class="message mdl-shadow--2dp">'+
                        '<p>'+data.message+'</p>'+                  
                      '</div>'+
                      '<div id="msg_detail" class="text_left">'+
                      '<small data-livestamp="'+data.time_sent+'"></small>'+
                      '</div>'+
                    '</li>';
              $('#appointment_chat').append(chat_message);
            }
            if (data.appointment_id!=chat_app_id) {
                  /*Show alert of message from another customer*/
                  $('.live_updates').fadeIn('fast');
                  $('.live_updates').html('<span class="update_close">'+
                                            '<i class="fa fa-times mdl-color-text--pink"></i>'+
                                      '</span>'+
                                      '<a href="'+chat_url+data.appointment_id+'">'+
                                        '<div class="update_img">'+      
                                          '<img src="'+img+'">'+
                                        '</div>'+
                                        '<div class="update_content">'+
                                          '<small class="update_title bold">'+
                                            data.sender_name+
                                          '</small>'+
                                          '<small class="update_msg">'+
                                            data.message+
                                          '</small>'+        
                                        '</div>'+
                                      '</a>');
                  $('.live_updates').delay(8000).fadeOut('fast');
                }                 
          }); 
        

        /*Callbacks when pusher is connected, connecting, unavailable,failed and disconnected*/
                pusher.connection.bind('connected',function(){
                  console.log('pusher connected');
                  //$('.chat').css('background-color','#F1F4F5');
                });

                pusher.connection.bind('connecting',function(){
                  console.log('pusher connecting');
                  $('#send_button').attr('disabled');
                  //$('.chat').css('background-color','#FFF9C4');
                });

                pusher.connection.bind('unavailable',function(){
                  console.log('pusher unavailable');
                  //$('.chat').css('background-color','#E57373');
                });

                pusher.connection.bind('failed',function(){
                  console.log('pusher failed');
                  //$('.chat').css('background-color','#E57373');
                });

                pusher.connection.bind('disconnected',function(){
                  console.log('pusher disconnected');
                  //$('.chat').css('background-color','#E57373');
                });
        /*End of Callbacks to pusher connected*/