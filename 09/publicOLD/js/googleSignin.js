          var googleUser = {};
          var startApp = function() {
            gapi.load('auth2', function(){
              // Retrieve the singleton for the GoogleAuth library and set up the client.
              auth2 = gapi.auth2.init({
                client_id: '609910267560-4ql3id0qnpsirjhps21ch55qoggrmogg.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin',
                // Request scopes in addition to 'profile' and 'email'
                //scope: 'additional_scope'
              });
              attachSignin(document.getElementById('google'));
            });
          };

          function attachSignin(element) {
            //console.log(element.id);
            auth2.attachClickHandler(element, {},
                function(googleUser) {
                  //console.log(googleUser);
                  var profile = googleUser.getBasicProfile();
                  var id = profile.getId();
                  var pic_url=profile.getImageUrl();
                  console.log(id);                  
                  var checkGoogleId= new XMLHttpRequest();
                  checkGoogleId.onreadystatechange = function(){
                    if (checkGoogleId.readyState==4 && checkGoogleId.status==200) {
                      var result=JSON.parse(checkGoogleId.responseText);
                      console.log('Request '+checkGoogleId.responseText);
                      if (result.status=='ok') {
                        window.location.replace(services_url);
                      };
                      if (result.status!='ok') {
                        $('#social_login').fadeOut('fast');
                        $('#social_login_error').text('Account details not found, Kindly register').fadeIn('fast');
                      };
                    }
                  }
                  checkGoogleId.open("POST",google_url, true);
                  checkGoogleId.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                  checkGoogleId.send("google_id="+id+'&pic_url='+pic_url);

                }, function(error) {
                  alert(JSON.stringify(error, undefined, 2));
                });
          }
          startApp();     