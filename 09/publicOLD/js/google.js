          var googleUser = {};
          var startApp = function() {
            gapi.load('auth2', function(){
              // Retrieve the singleton for the GoogleAuth library and set up the client.
              auth2 = gapi.auth2.init({
                client_id: '609910267560-4ql3id0qnpsirjhps21ch55qoggrmogg.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin',
                // Request scopes in addition to 'profile' and 'email'
                //scope: 'additional_scope'
              });
              attachSignin(document.getElementById('google'));
            });
          };

          function attachSignin(element) {
            //console.log(element.id);
            auth2.attachClickHandler(element, {},
                function(googleUser) {
                  console.log(googleUser);
                  var id_token = googleUser.getAuthResponse().id_token;
                  console.log(id_token);
                  var profile = googleUser.getBasicProfile();
                  var fullname= profile.getName();
                  $('#social_alerts').text('Hi '+fullname+', welcome to Charmé').fadeIn('fast');
                  /*Add to input field*/
                  $('#pic_url').val(profile.getImageUrl());
                  $('#first_name').val(profile.getFamilyName());
                  $('#first_name').parent().addClass('is-focused');
                  $('#last_name').val(profile.getGivenName());
                  $('#last_name').parent().addClass('is-focused');
                  $('#g_token').val(id_token);
                  $('#email').val(profile.getEmail());
                  $('#email').parent().addClass('is-focused');
                  $('#password').parent().hide('slow');
                  $('#confirm_password').parent().hide('slow');
                }, function(error) {
                  alert(JSON.stringify(error, undefined, 2));
                });
          }
          startApp();     