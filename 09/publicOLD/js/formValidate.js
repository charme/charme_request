        /*Return false form submit*/

            var validate_password_change= new FormValidator('password_change',
              [
                {
                  name: 'old_password',
                  display: 'Current Password',
                  rules: 'required|min_length[6]|alpha_numeric'
                },
                {
                  name: 'new_password',
                  display: 'New Password',
                  rules: 'required|min_length[6]|alpha_numeric'
                },
                {
                  name: 'confirm_new_password',
                  display: 'Confirm Password',
                  rules: 'required|min_length[6]|alpha_numeric|matches[new_password]'
                },
              ],
              function(errors,event){
                if (errors.length>0) {
                  //console.log(errors);
                  $('.password_errors').html(errors[0].message);
                    errors[0].element.focus();
                  return false;
                };

              });
            validate_password_change.setMessage('required', 'Kindly fill %s.');
        /*Send invite to a friend*/
                  $("#invite_friend").submit(function(event){ 

                      $('.invite_errors').html(' ').fadeIn('slow');
                      email= $('#email').val();

                      if (!email) {          
                        $('.invite_errors').html('Enter Email Address').fadeIn('slow');
                        return false;
                      event.preventDefault(); 
                      };
                  });
        /*Password Field toggle*/
          function showPassword(in_field){
            var id = $(in_field).prevAll('input').attr("id");
            var type = $(in_field).prevAll('input').attr("type");
              if (type=='password') {
              document.getElementById(id).type="text"; 
              $(in_field).html('<i class="fa fa-eye-slash"></i>');
              //console.log(type+" = " +id );        
              }

              if (type=='text') {
              $(in_field).html('<i class="fa fa-eye"></i>');
              document.getElementById(id).type="password";
              }
          }
        /*Validate Contact Form*/
        var validate_contact_customer_care= new FormValidator('contact_customer_care',
          [
            {
              name: 'title',
              rules: 'required'
            },
            {
              name: 'message',
              rules: 'required|max_length[140]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              $('.contact_customer_care_errors').html(errors[0].message);
                errors[0].element.focus();
              return false;
            };

          });
          validate_contact_customer_care.setMessage('required', 'Enter a %s.');
          
          
          /*Validate Gift a service */
          var validate_giftService= new FormValidator('giftService',
            [ 
              {
                name: 'name',
                display: "your friend's name",
                rules: 'required|alpha'            
              },  
              {
                name: 'email',
                display: "your friend's email",
                rules: 'required|valid_email'
              },         
              {
                name: 'phone',
                display: "your friend's phone number",
                rules: 'required|numeric|max_length[15]'            
              },                        
            ],
            function(errors,event){
              if (errors.length>0) {
                //console.log(errors);
                  $('.form_errors').html(errors[0].message);
                    errors[0].element.focus();
                  return false;    
              };
            });
          validate_giftService.setMessage('required', 'Enter %s.');

          /*Validate profile picture change*/
          var validate_changePic= new FormValidator('changePic',
            [
              {
                name: 'pics',
                display: 'Profile Picture',
                rules: 'required|is_file_type[gif,png,jpg]'
              }
            ],
            function(errors,event){
              if (errors.length>0) {
                //console.log(errors);
                  $('.pic_errors').html(errors[0].message);
                    errors[0].element.focus();
                  return false;    
              };
            });
          validate_changePic.setMessage('required', 'Select A Picture');
          validate_changePic.setMessage('is_file_type[gif,png,jpg]', 'Use a jpg, gif or png image');
          /*validate edit profile*/
         var validate_editProfile= new FormValidator('editProfile',
            [
              {
                name: 'email',
                display: 'email',
                rules: 'required|valid_email'
              },         
              {
                name: 'phone_no',
                display: 'Phone Number',
                rules: 'required|numeric|max_length[15]'            
              },            
              {
                name: 'first_name',
                display: 'First Name',
                rules: 'required|alpha'            
              },    
              {
                name: 'last_name',
                display: 'Last Name',
                rules: 'required|alpha'            
              }
            ],
            function(errors,event){
              if (errors.length>0) {
                //console.log(errors);
                //if (errors[0].id!='location') {
                  $('.form_errors').html(errors[0].message);
                    errors[0].element.focus();
                  return false;                
                //};
              };

            });
          validate_editProfile.setMessage('required', 'Fill %s.');