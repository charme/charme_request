/*Form validations*/
        /*Profile picture upload*/

        var validate_changePic= new FormValidator('changePic',
          [
            {
              name: 'pics',
              display: 'Profile Picture',
              rules: 'required|is_file_type[gif,png,jpg]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
                $('.pic_errors').html(errors[0].message);
                  errors[0].element.focus();
                return false;    
            };
          });
        validate_changePic.setMessage('required', 'Select A Picture');
        validate_changePic.setMessage('is_file_type[gif,png,jpg]', 'Use a jpg, gif or png image');
        
        /*Validate Contact Form*/
        var validate_contact_customer_care= new FormValidator('contact_customer_care',
          [
            {
              name: 'title',
              rules: 'required'
            },
            {
              name: 'message',
              rules: 'required|max_length[140]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              $('.contact_customer_care_errors').html(errors[0].message);
                errors[0].element.focus();
              return false;
            };

          });
          
        /*Validate account details*/
        var validate_account_form= new FormValidator('account_form',
          [
            {
              name: 'bank_name',
              display: 'Bank Name',
              rules: 'required'
            },
            {
              name: 'account_no',
              display: 'Account Number',
              rules: 'required|numeric|exact_length[10]'            
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              $('.account_errors').html(errors[0].message);
                errors[0].element.focus();
              return false;
            };

          });
        validate_account_form.setMessage('required', 'Kindly fill %s.');

        /*Validate change location form*/
        var validate_my_location= new FormValidator('my_location',
          [
            {
              name: 'address',
              display: 'Address',
              rules: 'required'
            },            
            {
              name: 'address_details',
              display: 'Address Description',
              rules: 'max_length[40]'            
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              //if (errors[0].id!='location') {
                $('.form_errors').html(errors[0].message);
                  errors[0].element.focus();
                return false;                
              //};
            };

          });
        validate_my_location.setMessage('required', 'Address not found.');
        validate_my_location.setMessage('greater_than[2]', 'Location not found.');

        var validate_editProfile= new FormValidator('editProfile',
          [
            {
              name: 'email',
              display: 'email',
              rules: 'required|valid_email'
            },         
            {
              name: 'phone_no',
              display: 'Phone Number',
              rules: 'required|numeric|max_length[15]'            
            },            
            {
              name: 'first_name',
              display: 'First Name',
              rules: 'required|alpha'            
            },    
            {
              name: 'last_name',
              display: 'Last Name',
              rules: 'required|alpha'            
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              //if (errors[0].id!='location') {
                $('.form_errors').html(errors[0].message);
                  errors[0].element.focus();
                return false;                
              //};
            };

          });
        validate_editProfile.setMessage('required', 'Fill %s.');

        /*Validation for password change*/
        var validate_password_change= new FormValidator('password_change',
          [
            {
              name: 'old_password',
              display: 'current password',
              rules: 'required|alpha_numeric'
            },
            {
              name: 'new_password',
              display: 'New Password',
              rules: 'required|alpha_numeric|min_length[6]'            
            },
            {
              name: 'confirm_new_password',
              display: 'confirm password',
              rules: 'required|alpha_numeric|matches[new_password]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              $('.password_errors').html(errors[0].message);
                errors[0].element.focus();
              return false;
            };

          });
        /*Check fields before sending password change*/
        function checkfields(event){

          confirm_new_password= $('#confirm_new_password').val();
          
          new_password=$('#new_password').val();
          if (!confirm_new_password) {          
            $('.password_errors').html('Confirm Password');
            return false;
          };

          if (!new_password) {

            $('.password_errors').html('Enter Password');
            return false;
          };

          if (confirm_new_password!= new_password) {
            $('.password_errors').html('Password Does not match');
            return false;
          };
        }

        /*Show password */
      function showPassword(in_field){
        var id = $(in_field).prevAll('input').attr("id");
        var type = $(in_field).prevAll('input').attr("type");
          if (type=='password') {
          document.getElementById(id).type="text"; 
          $(in_field).html('<i class="fa fa-eye-slash"></i>');
          //console.log(type+" = " +id );        
          }

          if (type=='text') {
          $(in_field).html('<i class="fa fa-eye"></i>');
          document.getElementById(id).type="password"; 
          //console.log(type);              
          }
      }

      /*Focus on bank_name to edit*/
        $('#edit_account').click(function(){
          $("#bank_name").focus();
          $('#save_bank_details').fadeIn('fast');
        });
        $("#bank_name").focus(function(){
          $('#save_bank_details').fadeIn('fast');
        });
        $("#account_no").focus(function(){
          $('#save_bank_details').fadeIn('fast');
        });
        /*Validate Earnings form*/
        
        $("#account_form").submit(function(event){ 
            //$('.account_errors').html(' ').fadeIn('slow');
            /*Get bank Name*/
            var bank_name= $('#bank_name').val();
            var bank_patt= new RegExp("^((?!(0))[0-9])$");
            var b_na = bank_patt.test(bank_name);
            /*Get account number*/
            var account_no=$('#account_no').val();
            var valid_account_no=$.isNumeric(account_no);
            /*Regular expression for account number*/           
            var account_patt = new RegExp("^((?!(0))[0-9]{10})$");
            var a_no = account_patt.test(account_no);

            if (b_na) {          
              $('.account_errors').html('Enter Valid Bank Name').css('color','red');
              //alert('invalid bank name');
              return false;
            event.preventDefault(); 
            };
            
            if (!a_no) {
              $('.account_errors').html('Enter correct Account Number').css('color','red');
              //alert('invalid accnt no');
              return false;
            event.preventDefault(); 
            };
        }); 
        
        /*Profile*/
        $('#change_description').click(changetype);
        $('#description_holder').click(changetype);
        function changetype(){
          $('#description_holder').css('border','none');
          var desc=$('#description').html();
          //$('#change_description').attr('id','NaN');
          if ($('#description_holder').find('#send_description').length) {
            return false;
          };
          $('#description').replaceWith('<textarea class="mdl-textfield__input" type="text" id="send_description" ></textarea>'+
            '<button id="save_desc" onclick="sendDesc()" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">Save</button> <p class="line" id="save_result"></p>');
          $('#send_description').val(desc);
          //console.log(desc);
        }
        
        /*Return false form submit*/
        $("#password_change").submit(function(event){ 

            $('.password_errors').html(' ').fadeIn('slow');
            old_password= $('#old_password').val();
            new_password=$('#new_password').val();
            confirm_new_password= $('#confirm_new_password').val();
            
            if (!old_password) {          
              $('.password_errors').html('Enter Current Password').fadeIn('slow');
              return false;
            event.preventDefault(); 
            };
            
            if (!new_password) {
              $('.password_errors').html('Enter New Password').fadeIn('slow');
              return false;
            event.preventDefault(); 
            };

            if (!confirm_new_password) {          
              $('.password_errors').html('Confirm Password').fadeIn('slow');
              return false;
            event.preventDefault(); 
            };           

            if (confirm_new_password!= new_password) {
              $('.password_errors').html('Password Does not match').fadeIn('slow');
              return false;
            event.preventDefault(); 
            };
        }); 
        
        function sendDesc(){
          var desc_input= $('#send_description').val();
          var post_desc = new XMLHttpRequest();
            post_desc.onreadystatechange = function() {
              if (post_desc.readyState == 4 && post_desc.status == 200) {
                //console.log(post_desc.response);
                $('#save_result').html('<p class="mdl-color-text--green bold line">Description saved </p>');
              }
              if(post_desc.readyState!=4){
                $('#save_result').html('<p class="mdl-color-text--green bold line">Saving Description</p>');
              }
              if (post_desc.status == 500 ) {

                $('#save_result').html('<p class="mdl-color-text--red bold line">Description was not saved </p>');
              };
            };
            post_desc.open("POST",profile_desc_url , true);
            post_desc.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            post_desc.send("description="+desc_input);
          }    
      /*Form validations*/