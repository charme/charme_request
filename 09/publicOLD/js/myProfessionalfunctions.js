      $('input:checkbox').simpleCheckbox();
      $('#send_description').elastic();
              
      /*Start Service*/
      function start(appointment){

        //console.log(parent);
        var appointment_id= $(appointment).attr('id');
        var div=$('img#'+appointment_id);
        console.log(div);

        var time= $(appointment).attr('data-time');
        if (time=='') {
        var time=moment();          
        };

        var year=moment(time).year();
        var mon=moment(time).month();
        var day=moment(time).date();
        var hr=moment(time).hour();
        var min=moment(time).minute();
        var sec=moment(time).second();
        var start=new Date(year, mon, day,hr,min,sec);
        var timer_start=' ';
        var timer_start=moment().toISOString();
        $(div).attr('data-time',' '+timer_start+' ');
        console.log(timer_start);
        /*During service*/
          var timer ="<div style='display:block;'>"+'<div id="selector"></div>'+
          '<a href="'+app_completed_url+'" id="feedback" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent mdl-color--white mdl-color-text--pink">End Service</a>'+
          "<div class='sa-button-container' style='display:block;'>";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
            console.log(xhttp.responseText);
            swal({  title: "Your service has begun",   
                  text: timer,   
                  type: null,   
                  html: true,
                  imageUrl: timer_img,
                  customClass: 'no-background',
                  showCancelButton: false,
                  allowOutsideClick: true,
                  confirmButtonColor: "#e91e63",
                  showConfirmButton: true });   
                  $('#selector').countdown({since: new Date(year, mon, day, hr, min, sec), format: 'HMS', compact: true});
          }
          if (xhttp.status == 500) {
            console.log(xhttp.responseText);
          }
        };
        xhttp.open("POST",app_start_url , true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("id="+appointment_id);
                    
      }
      
        /*During service*/
        $('#timer > img').click(function(){
          var time='';
          var app_id= '';
          //console.log(this);
          var time= $(this).attr('data-time');
          var app_id= $(this).attr('data-id');
          //console.log(app_id);
          //console.log('appointment_session '+time);

          if (time=='null') {
            var time= $(this).attr('data-start_at');   
            var time =moment(time).format("dddd, MMM Do, h:mm a");                  
            console.log('new time'+time);
                var timer ="<div style='display:block;'>"+
                '<h2 class="bold mdl-color-text--white">'+time+'</h2>'+
                "<div class='sa-button-container' style='display:block;'>";
                swal({  title: "Your appointment is on",   
                        text: timer,   
                        type: null,   
                        html: true,
                        customClass: 'no-background',
                        showCancelButton: false,
                        allowOutsideClick: true,
                        confirmButtonColor: "#e91e63",
                        showConfirmButton: true });
                return false;
              };
              if (time!='null') {             
                var year=moment(time).year();
                ////console.log('year: '+year);
                var mon=moment(time).month();
                ////console.log('month: '+mon);
                var day=moment(time).date();
                ////console.log('day: '+day);
                var hr=moment(time).hour();
                ////console.log('hour: '+hr);
                var min=moment(time).minute();
                ////console.log('minute: '+min);
                var sec=moment(time).second();
                ////console.log('second: '+sec);

                var start=new Date(year, mon, day,hr,min,sec);
                var timer ="<div style='display:block;'>"+'<div id="progress"></div>'+
                '<a href="'+app_completed_url+app_id+'" id="feedback" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent mdl-color--white mdl-color-text--pink">End Service</a>'+
                "<div class='sa-button-container' style='display:block;'>";
                swal({  title: "Service in progress",   
                        text: timer,   
                        type: null,   
                        html: true,
                        imageUrl: timer_img,
                        customClass: 'no-background',
                        showCancelButton: false,
                        allowOutsideClick: true,
                        confirmButtonColor: "#e91e63",
                        showConfirmButton: true });
                  $('#progress').countdown({since: new Date(year, mon, day, hr, min, sec), format: 'HMS', compact: true});
              };
            });          
        
        /*Cancel Appointment*/
         /*Cancel service*/
        $('.central > #cancel_appointment').click(function(){
            var id=$(this).find('img').attr('id');
            console.log(id);
            var cancel ="<div class='sa-button-container'>"+
            '<span class="border-top block central">No money will be credited to you.</span>'+
            "<div style='display:block;'>"+
            '<input name="appointment_location" type="text" hidden>'+
            "<button class='mdl-color--white mdl-color-text--pink border-pink'>No</button>"+
            "<a href='"+app_cancel_url+id+"'><button class='mdl-color--pink mdl-color-text--black'>Yes</button></a>"+
            "</div>"+
            "</form></div>";
            swal({  title: "Cancel this appointment?",   
                    text: cancel,   
                    type: "input",   
                    html: true,
                    showCancelButton: false,
                    allowOutsideClick: true,
                    showConfirmButton: false });
        });

          /*function cancel_appointment(appointment_id){
            var cancel ="<div class='sa-button-container'>"+
              '<span class="border-top block central">(% of the price will be deducted from your account.)</span>'+
              "<div style='display:block;'>"+
              '<input name="appointment_location" type="text" hidden>'+
              "<button class='mdl-color--white mdl-color-text--pink border-pink'>No</button>"+
              "<a href='"+appointment_cancel_url+appointment_id+"'><button class='mdl-color--pink mdl-color-text--black'>Yes</button></a>"+
              "</div>"+
              "</div>";
              swal({  title: "Are you sure you want to cancel this appointment?",   
                      text: cancel,   
                      type: "input",   
                      html: true,
                      showCancelButton: false,
                      allowOutsideClick: true,
                      showConfirmButton: false });
          }*/
        /*Toggle available for*/
      $(document).ready(function(){
          $('#toggleAvailable').click(function(){
            var available_status;
            var status =$('#available_text').text();
            //console.log('Current '+status);
            if (status=='Available') {
              available_status=0;
            }
            else{available_status=1;};
            //console.log('New '+available_status);

            var change_available = new XMLHttpRequest();
            change_available.onreadystatechange = function() {
              if (change_available.readyState == 4 && change_available.status == 200) {
                //console.log(change_available.responseText);    
                $('#available_text').text(function(i, text){
                  return text === "Available" ? "Unavailable" : "Available";});
                $('#available_color').toggleClass("mdl-color-text--green");
                $('#available_toggle').toggleClass('on');            
              }
              if (change_available.status == 500) {
                //console.log(change_available.responseText);
              }
            };
            change_available.open("POST",available_url , true);
            change_available.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            change_available.send("available="+available_status);            
          });  

      });      