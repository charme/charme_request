        var data_time=$('#appointment_session').data('start_at');
        var time_duration=3000;
        /*Set Appointment Time*/
        $('.service_style > .pink_hover').click(function(){
            var service_id = $(this).attr('id');
            var appointment_time ="<div class='sa-button-container'>"+
            '<h4><i class="fa fa-clock-o fa-2x mdl-color-text--pink" aria-hidden="true"></i> Set Your Appointment Time</h4>'+
            "<form method='get' action='"+services_professionals_url+"'>"+
            "<input id='datetimepicker' class='mdl-color--white' type='text' value='Set Date & Time' placeholder='Select Time' name='appointment_time'>"+
            "<input type='text' hidden name='service_id' value="+service_id+" >"+
            "<div id='ad'></div>"+
            "<div style='display:block;'>"+
            "<input type='submit' value='Continue' id='continue' class='mdl-color--pink mdl-color-text--white'/>"+
            "</div>"+
            "</form></div>";
            $('#datetimepicker').onkeydown = function(e){
                e.preventDefault();
            }
            swal({  title: null,   
                    text: appointment_time,   
                    type: null,   
                    html: true,
                    showCancelButton: false,
                    allowOutsideClick: true,
                    showConfirmButton: false });
                $('#continue').click(function(){
                  var appointment_time= $('#datetimepicker').val();
                  if (appointment_time=='') {
                  document.getElementById("ad").innerHTML ="<b class='mdl-color-text--red block'>Kindly Select Appointment Time</b>";
                  return false;   
                  };
                });
                $('#datetimepicker').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm', shortTime : true, minDate : new Date() });                
                
        });

        /*Filter by Time*/
        $('#time_filter').click(function(){
            var service_id = filter_service_id;
            var filter_time ="<div class='sa-button-container'>"+
            "<form method='get' action='"+services_professionals_url+"'>"+
            "<input id='datetimepicker' class='mdl-color--white' type='text' value='Set Date & Time' placeholder='Select Time' name='appointment_time'>"+            
            "<input type='text' hidden name='service_id' value="+service_id+" >"+
            "<div style='display:block;'>"+
            "<input type='submit' value='Find' class='mdl-color--pink mdl-color-text--white'/>"+
            "</div>"+
            "</form></div>";
            swal({  title: "Find Professional Available On",   
                    text: filter_time,   
                    type: null,   
                    html: true,
                    showCancelButton: false,
                    allowOutsideClick: true,
                    showConfirmButton: false });
            $('#datetimepicker').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm', shortTime : true, minDate : new Date() }).delay(7000);                
               
                /*$('#datetimepicker').datetimepicker({
                  minDate:0,
                  inline:true
                });*/
        });              
          
        /*Cancel service*/
        $('.central > #cancel').click(function(){
            var cancel ="<div class='sa-button-container'>"+
            '<span class="border-top block central">(% of the price will be deducted from your account.)</span>'+
            "<div style='display:block;'>"+
            '<input name="appointment_location" type="text" hidden>'+
            "<button class='mdl-color--white mdl-color-text--pink border-pink'>No</button>"+
            "<a href=''><button class='mdl-color--pink mdl-color-text--black'>Yes</button></a>"+
            "</div>"+
            "</form></div>";
            swal({  title: "Are you sure you want to cancel this appointment?",   
                    text: cancel,   
                    type: "input",   
                    html: true,
                    showCancelButton: false,
                    allowOutsideClick: true,
                    showConfirmButton: false });
        });
        /*During service*/
        $('#timer > img').click(function(){
            var time=' ';
            var app_id= ' ';
            //console.log(this);
            var time= $(this).attr('data-time');
            var app_id= $(this).attr('data-id');
            var appointment_id= $(this).attr('id');
            console.log('id is '+appointment_id);
            console.log(app_id);
            console.log('appointment_session '+time);

            if (time=='null') {
              var time= $(this).attr('data-start_at');   
              var time =moment(time).format("dddd, MMM Do, h:mm a");                  
              //console.log('new time'+time);
                  var timer ="<div style='display:block;'>"+
                  '<h2 class="bold mdl-color-text--white">'+time+'</h2>'+
                  "<div class='sa-button-container' style='display:block;'>";
                  swal({  title: "Your appointment is on",   
                          text: timer,   
                          type: null,   
                          html: true,
                          customClass: 'no-background',
                          showCancelButton: false,
                          allowOutsideClick: true,
                          confirmButtonColor: "#e91e63",
                          showConfirmButton: true });
                  return false;
                };
                if (time!='null') {             
                  var year=moment(time).year();
                  ////console.log('year: '+year);
                  var mon=moment(time).month();
                  ////console.log('month: '+mon);
                  var day=moment(time).date();
                  ////console.log('day: '+day);
                  var hr=moment(time).hour();
                  ////console.log('hour: '+hr);
                  var min=moment(time).minute();
                  ////console.log('minute: '+min);
                  var sec=moment(time).second();
                  ////console.log('second: '+sec);

                  var start=new Date(year, mon, day,hr,min,sec);
                  var timer ="<div style='display:block;'>"+'<div id="progress"></div></div>'+
                  '<a href="'+feedback_url+'/feedback for appointment: '+appointment_id+'" id="feedback" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent mdl-color--white mdl-color-text--pink">Any Feedback?</a>'+
                  "<div class='sa-button-container' style='display:block;'>";
                  swal({  title: "Service in progress",   
                          text: timer,   
                          type: null,   
                          html: true,
                          imageUrl: time_img,
                          customClass: 'no-background',
                          showCancelButton: false,
                          allowOutsideClick: true,
                          confirmButtonColor: "#e91e63",
                          showConfirmButton: true });
                    $('#progress').countdown({since: new Date(year, mon, day, hr, min, sec), format: 'HMS', compact: true});
                };
              });          
          
        /*Redeem service Date-time Picker*/
          $('#redeem_gift').click(function(){
            var gift_code = $('#gift_code').val();          
            if (gift_code=='') {
              document.getElementById("results").innerHTML = "<p class='mdl-color-text--red'>Enter Gift Card</p>";
              return false; 
            };
            //console.log('Send'+gift_code+' to server');
            send_gift = new XMLHttpRequest();
            send_gift.onreadystatechange = function(){
                    if (send_gift.readyState==4 && send_gift.status==200) {
                      var gift_response = JSON.parse(send_gift.responseText);
                      //console.log(gift_response);
                      if (gift_response.status=='ok') {
                         document.getElementById("results").innerHTML = "<p class='mdl-color-text--green'>Great, Your Gift Is Ready</p>";
                          var service_id= gift_response.data.Gift.service_id;
                          var appointment_time ="<div class='sa-button-container'>"+
                          '<h4><i class="fa fa-clock-o fa-2x mdl-color-text--pink" aria-hidden="true"></i> <span class="mdl-color-text--green line">Valid Gift</span>, Set Appointment Time</h4>'+
                          "<form method='get' action='"+gift_professional_url+"'>"+
                          "<input id='datetimepicker' class='mdl-color--white' type='text' value='Set Date & Time' placeholder='Select Time' name='appointment_time'>"+
            
                          "<input type='text' hidden name='service_id' value="+service_id+" >"+
                          "<input type='text' hidden name='gift_code' value="+gift_code+" >"+
                          "<div id='ad'></div>"+
                          "<div style='display:block;'>"+
                          "<input type='submit' value='Continue' id='continue' class='mdl-color--pink mdl-color-text--white'/>"+
                          "</div>"+
                          "</form></div>";
                          swal({  title: null,   
                                  text: appointment_time,   
                                  type: null,   
                                  html: true,
                                  showCancelButton: false,
                                  allowOutsideClick: true,
                                  showConfirmButton: false });
                              $('#continue').click(function(){
                                var appointment_time= $('#datetimepicker').val();
                                if (appointment_time=='') {
                                document.getElementById("ad").innerHTML ="<b class='mdl-color-text--red block'>Kindly Select Appointment Time</b>";
                                return false;                  
                                //alert();
                                };
                              });
                              $('#datetimepicker').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm', shortTime : true, minDate : new Date() }).delay(7000);                
                              /*$('#datetimepicker').datetimepicker({
                                minDate:0,
                                inline:true
                              });*/
                      
                      };

                      if (gift_response.status !=='ok') {
                         document.getElementById("results").innerHTML = "<p class='mdl-color-text--red'>Invalid Gift Card,"+gift_response.error.msg+"</p>";
                      };
                    }
                    if (send_gift.readyState!=4){
                      /*Validating Gift*/
                       document.getElementById("results").innerHTML = '<p class="mdl-color-text--green">'+
                       '<i class="fa fa-spinner fa-spin mdl-color-text--green"></i>Validating Gift</p>';
                    }
                    if(send_gift.status==500){
                      /*Gift verification failed*/
                      document.getElementById("results").innerHTML = '<p class="mdl-color-text--pink">'+
                       '<i class="fa fa-exclamation-circle mdl-color-text--black"></i> Unable to validate Gift (Network Error)</p>';
                    }
                  };
                  send_gift.open("POST",redeem_gift_url, true);
                  send_gift.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                  send_gift.send("code="+gift_code);
          });
        /*End of Redeem service*/
        /*Reschedule appointment*/            
          function reschedule(appointment_id){
            var appmnt_id=$(appointment_id).attr('id');     
            //alert('hi');
            var reschedule ="<div class='sa-button-container'>"+
              "<form method='post' action='"+appointment_reschedule_url+"'>"+
              "<input id='datetimepicker' class='mdl-color--white' type='text' value='Set Date & Time' placeholder='Select Time' name='appointment_time'>"+            
              "<input hidden value='"+appointment_id+"' type='text' name='id'>"+
              "<div id='ad'></div>"+
              "<div style='display:block;'>"+
              "<input type='submit' id='continue' value='Reschedule' class='mdl-color--pink mdl-color-text--white'/>"+
              "</div>"+ 
              "</form></div>";
              swal({  title: "Reschedule your Appointment",   
                      text: reschedule,   
                      type: null,   
                      html: true,
                      showCancelButton: false,
                      allowOutsideClick: true,
                      showConfirmButton: false });
                  $('#continue').click(function(){
                                var appointment_time= $('#datetimepicker').val();
                                //alert(appointment_time);
                                if (appointment_time=='') {
                                document.getElementById("ad").innerHTML ="<b class='mdl-color-text--red block'>Kindly Select Appointment Time</b>";
                                return false;  
                                };
                              });
                  $('#datetimepicker').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm', shortTime : true, minDate : new Date() });                
                  /*$('#datetimepicker').datetimepicker({
                    minDate:0,
                    inline:true
                  });*/

          }
        /*Cancel Appointment*/
          function cancel_appointment(appointment_id){
            //alert(appointment_id);
            var cancel ="<div class='sa-button-container'>"+
              '<span class="border-top block central">(% of the price will be deducted from your account.)</span>'+
              "<div style='display:block;'>"+
              '<input name="appointment_location" type="text" hidden>'+
              "<button class='mdl-color--white mdl-color-text--pink border-pink'>No</button>"+
              "<a href='"+appointment_cancel_url+appointment_id+"'><button class='mdl-color--pink mdl-color-text--black'>Yes</button></a>"+
              "</div>"+
              "</div>";
              swal({  title: "Are you sure you want to cancel this appointment?",   
                      text: cancel,   
                      type: "input",   
                      html: true,
                      showCancelButton: false,
                      allowOutsideClick: true,
                      showConfirmButton: false });
          }
        /*Show coupon*/
          function showCoupon(){
                    if($('#coupon_question').prop("checked")){
                      //alert('show');
                        $(".answer").show(700);
                      } else {
                        //alert('hide');
                          $(".answer").hide(1000);
                      }
                  }
        /*Verify Coupon*/
          function verifyCoupon(){
                    var coupon_code=$('#coupon_field').val();
                    if (coupon_code=='') {return false;};
                    //alert('Send'+coupon_code+' to server');
                    send_coupon = new XMLHttpRequest();
                    send_coupon.onreadystatechange = function(){
                      if (send_coupon.readyState==4 && send_coupon.status==200) {
                        var coupon_response = JSON.parse(send_coupon.responseText);
                        //console.log(coupon_response);
                        if (coupon_response.status=='ok') {
                          
                          /*fetch service price*/
                          var service_amount= $('.price').attr('id');
                          
                          /*Fetch coupon price*/
                          var coupon_amount = coupon_response.data.Coupon.amount/100;
                          
                          /*Calculate total price*/
                          var total_amount = service_amount - coupon_amount;
                          
                          /*Show breakdown to customer*/
                          document.getElementById("coupon_amount").innerHTML = '- ₦'+coupon_amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                          document.getElementById("total_amount").innerHTML = '₦'+total_amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                          document.getElementById("results").innerHTML ='<p><i class="fa fa-check-circle mdl-color-text--green"></i>'
                          +' Valid Coupon!</p>';
                          $("#coupon_value").fadeIn('slow');
                          $("#total_value").fadeIn('slow');
                        };

                        if (coupon_response.status !=='ok') {
                           document.getElementById("results").innerHTML = "<p class='mdl-color-text--red'>Invalid Coupon"+coupon_response.error.msg+"</p>";
                        };
                        //document.getElementById("test").innerHTML = send_coupon.responseText;
                      }
                      if (send_coupon.readyState!=4){
                        /*Validating coupon*/
                         document.getElementById("results").innerHTML = '<p class="mdl-color-text--green">'+
                         '<i class="fa fa-spinner fa-spin mdl-color-text--green"></i>Validating Coupon</p>';
                      }
                      if(send_coupon.status==500){
                        /*Coupon verification failed*/
                        document.getElementById("results").innerHTML = '<p class="mdl-color-text--pink">'+
                         '<i class="fa fa-exclamation-circle mdl-color-text--black"></i> Unable to validate Coupon (Network Error)</p>';
                      }
                    };
                    send_coupon.open("POST", coupon_url, true);
                    send_coupon.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    send_coupon.send("coupon_code="+coupon_code);                
                  }
        /*Make request to get transaction details and info for payment gateway*/
          function saveRequest(){
            /*So what I did was save all other details in a php session, so to make/create a request all I have to do is post coupon code if it's valid*/
            var coupon_code=$('#coupon_field').val();
            var makeRequest = new XMLHttpRequest();

            makeRequest.onreadystatechange= function(){
              if (makeRequest.readyState==4 && makeRequest.status==200) {
                document.getElementById('save_details').innerHTML='Transaction Created <i class="fa fa-arrow-right"></i>';
                //console.log(makeRequest.responseText);
                var json= JSON.parse(makeRequest.responseText);
                if (json.status=='ok') {
                  var form = json.data;
                    if (form.Payment.status=="pending") {

                        var interswitch='<form name="form1" action="'+form.PaymentGatewayMeta.payment_gateway_url+'" method="post">'+
                          '<input name="product_id" type="text" hidden value="'+form.PaymentGatewayMeta.product_id+'" >'+
                          '<input name="pay_item_id" type="text" hidden value="'+form.PaymentGatewayMeta.pay_item_id+'" >'+
                          '<input name="amount" type="text" hidden value="'+form.PaymentGatewayMeta.amount+'" >'+
                          '<input name="currency" type="text" hidden value="'+form.PaymentGatewayMeta.currency+'" >'+
                          '<input name="site_redirect_url" type="text" hidden value="'+form.PaymentGatewayMeta.site_redirect_url+'">'+
                          '<input name="txn_ref" type="text" hidden value="'+form.PaymentGatewayMeta.txn_ref+'" >'+
                          //'<input name="cust_id" type="text" hidden value="'+form.Request.customer_id+'" >'+
                          '<input name="hash" type="text" hidden value="'+form.PaymentGatewayMeta.hash+ '" >';

                        var payment_options ="<div class='sa-button-container' style='display:block;'>"+
                          interswitch+
                          '<button type="submit" id="make_payment" class="padding--0px mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">'+
                          'Pay With Debit/Credit Card</button></form>'+
                          '<img src="'+interswitch_img+'">'+
                          "</div>";
                          ////console.log(form.PaymentGatewayMeta.hash);
                          ////console.log(payment_options);
                          $("#save_details").css('background-color','green');
                          document.getElementById("save_details").innerHTML = 'Request Created';

                        swal({  title: "Make Payment",   
                          text: payment_options,   
                          html: true,
                          type: null,
                          showCancelButton: false,
                          allowOutsideClick: false,
                          showConfirmButton: false });
                    };
                    if (form.Payment.status=='paid') {
                      var title = "Payment Confirmed";
                      var post_payment ="<div class='sa-button-container' style='display:block;'>"+
                      '<span class="mdl-color-text--green bold">Enjoy the service.</span> '+
                      '<a href="'+chat_url+form.Appointment.id+'" id="'+form.Request.supplier_id+'" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Chat With Professional <i class="fa fa-arrow-right"></i></a>'+
                      '<a href="'+appointment_url+'" id="'+form.Request.id+'" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> View Appointments <i class="fa fa-arrow-right"></i></a>'+
                      "</div>";

                      swal({  title: title,   
                      text: post_payment,   
                      type: "input",   
                      html: true,
                      imageUrl: payment_success_img,
                      imageSize: '120x120',
                      showCancelButton: false,
                      allowOutsideClick: false,
                      showConfirmButton: false });
                    };
                };
                /*This is where I receive response to load a swal for interswtich payment or View Appointments and chat*/
              if(json.status=='error') {
                document.getElementById('save_details').innerHTML='Unable to process request <i class="fa fa-ban"></i>';
                document.getElementById('request_response').innerHTML='Error Creating transaction, '+json.error.msg;};
              }

              if (makeRequest.status==500) {
                document.getElementById('save_details').innerHTML='Proceed To Payment <i class="fa fa-arrow-right"></i>';
                document.getElementById('request_response').innerHTML=makeRequest.responseText;
                /*show error processing request, Network most of the time*/
              }
              if (makeRequest.readyState !=4) {
                /*Show Processing Request*/
                document.getElementById('save_details').innerHTML='Creating transaction <i class="fa fa-spinner fa-spin mdl-color-text--black"></i>';
              }
            }; 
            makeRequest.open("POST", services_request_url, true);
            makeRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            makeRequest.send("coupon="+coupon_code);    
          }
        /*Create Gift*/
          function createGift(service_id){
            //alert(service_id);
            var makeGift = new XMLHttpRequest();
            makeGift.onreadystatechange= function(){
              if (makeGift.readyState==4 && makeGift.status==200) {
                var json= JSON.parse(makeGift.responseText);   
                if (json.status=='ok') {
                document.getElementById('gift_results').innerHTML='<span class="mdl-color-text--green bold">Gift Created</span>';
                  var form = json.data;
                    if (form.Gift.payment_status=="pending") {

                        var interswitch='<form name="form1" action="'+form.PaymentGatewayMeta.payment_gateway_url+'" method="post">'+
                          '<input name="product_id" type="text" hidden value="'+form.PaymentGatewayMeta.product_id+'" >'+
                          '<input name="pay_item_id" type="text" hidden value="'+form.PaymentGatewayMeta.pay_item_id+'" >'+
                          '<input name="amount" type="text" hidden value="'+form.PaymentGatewayMeta.amount+'" >'+
                          '<input name="currency" type="text" hidden value="'+form.PaymentGatewayMeta.currency+'" >'+
                          '<input name="site_redirect_url" type="text" hidden value="'+form.PaymentGatewayMeta.site_redirect_url+'">'+
                          '<input name="txn_ref" type="text" hidden value="'+form.PaymentGatewayMeta.txn_ref+'" >'+
                          //'<input name="cust_id" type="text" hidden value="'+form.Request.customer_id+'" >'+
                          '<input name="hash" type="text" hidden value="'+form.PaymentGatewayMeta.hash+ '" >';

                        var payment_options ="<div class='sa-button-container' style='display:block;'>"+
                          interswitch+
                          '<button type="submit" id="make_payment" class="padding--0px mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">'+
                          'Pay With Debit/Credit Card</button></form>'+
                          '<img src="'+interswitch_img+'">'+
                          "</div>";
                          $("#save_details").css('background-color','green');
                        swal({  title: "Purchase Gift",   
                          text: payment_options,   
                          html: true,
                          type: null,
                          showCancelButton: false,
                          allowOutsideClick: true,
                          showConfirmButton: false });
                    };
                };

                if (json.status=='error') {
                document.getElementById('gift_results').innerHTML='<span class="mdl-color-text--red bold">Error Creating Gift, please try again.</span>';
                  //alert(json.error.msg);
                };
              }

              if (makeGift.status==500) {
                document.getElementById('gift_results').innerHTML='<span class="mdl-color-text--red bold">Error Creating Gift, please try again.</span>';                
              }
              if (makeGift.readyState !=4) {
                /*Show Processing Request*/
                document.getElementById('gift_results').innerHTML='<span class="mdl-color-text--green bold">Creating Gift <i class="fa fa-spinner fa-spin mdl-color-text--green"></i></span>';
              }
            }; 
            makeGift.open("POST", gift_payment_url, true);
            makeGift.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            makeGift.send("service_id="+service_id); 

          }
        /*Save Location and details for making request later*/
          $('#save_details').click(function(){
            var address=$('#address').val();
            var location= $('#location').val();
            var address_details=$('#address_details').val();
            var instructions=$('#instructions').val();
            if (address=='' || location=='') {
              document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
                    
              return false;
            }
            else{
              var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (xhttp.readyState == 4 && xhttp.status == 200) {
                      var text = xhttp.responseText;
                      ////console.log(text);
                      
                      document.getElementById("holder").innerHTML = text;
                    }
                  if (xhttp.readyState !=4) {
                      document.getElementById("holder").innerHTML = '<i class="width--100 fa fa-spinner fa-5x fa-spin mdl-color-text--pink bold central"></i><br><h4 class="mdl-color-text--pink margin--auto text_center bold">Creating Appointment Request</h4>';
                  }
                  if (xhttp.status ==500) {
                    ////console.log(xhttp.responseText);
                    document.getElementById("holder").innerHTML = '<span onclick="location.reload();" class="fa-stack fa-5x block cancel width--100"><i class="fa fa-circle fa-stack-2x mdl-color-text--pink"></i><i class="fa fa-repeat fa-rotate-270 fa-stack-1x mdl-color-text--black"></i></span><br><h2 class="width--100"> Error Loading, Reload Page</h2>';
                  }
                };
                xhttp.open("POST",services_details_url, true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send("address="+address+"&location="+location+"&address_details="+address_details+"&instructions="+instructions);         
              }
          });   
        /*Save gift details and book Appointment*/
          $('#save_gift').click(function(){
            var address=$('#address').val();
            var location= $('#location').val();
            var address_details=$('#address_details').val();
            var instructions=$('#instructions').val();
            if (address=='' || location=='') {
              document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
                    
              return false;
            }
              else{
                var xhttp = new XMLHttpRequest();
                  xhttp.onreadystatechange = function() {
                    if (xhttp.readyState == 4 && xhttp.status == 200) {
                        var text = xhttp.responseText;
                        //console.log(text);
                        var json = JSON.parse(text);
                        if (json.status=='ok') {

                          var title = "Appointment Booked";
                          var post_payment ="<div class='sa-button-container' style='display:block;'>"+
                          '<span class="mdl-color-text--green bold"> Enjoy the service.</span> '+
                          '<a href="'+chat_url+json.data.Appointment.id+'" id="'+json.data.Appointment.id+'" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Chat With Professional <i class="fa fa-arrow-right"></i></a>'+
                          '<a href="'+appointment_url+'" id="'+json.data.Appointment.id+'" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> View Appointments <i class="fa fa-arrow-right"></i></a>'+
                          "</div>";

                          swal({  title: title,   
                          text: post_payment,   
                          type: "input",   
                          html: true,
                          imageUrl: gift_service_img,
                          imageSize: '120x120',
                          showCancelButton: false,
                          allowOutsideClick: false,
                          showConfirmButton: false });
                        };
                        if (json.status!='ok') {
                          var title = "Appointment not created";
                          var post_payment ="<div class='sa-button-container' style='display:block;'>"+
                          '<div class="mdl-color-text--red bold">'+json.error.msg+'</div>'+
                          '<a href="'+gift_location_url+'" id="make_payment" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Try Again <i class="fa fa-arrow-left"></i></a>'+
                          "</div>";
                          swal({  title: title,   
                          text: post_payment,   
                          type: "input", 
                          imageUrl: payment_unsuccessful_img,
                          imageSize: '120x120',  
                          html: true,
                          showCancelButton: false,
                          allowOutsideClick: false,
                          showConfirmButton: false });
                        };
                      }
                    if (xhttp.readyState !=4) {
                        document.getElementById("save_gift").innerHTML = ' Booking Appointment <i class="fa fa-spinner fa-spin mdl-color-text--white"></i>';
                    }
                    if (xhttp.status ==500) {
                      //console.log(xhttp.responseText);
                      document.getElementById("holder").innerHTML = '<span onclick="location.reload();" class="fa-stack fa-5x block cancel width--100"><i class="fa fa-circle fa-stack-2x mdl-color-text--pink"></i><i class="fa fa-repeat fa-rotate-270 fa-stack-1x mdl-color-text--black"></i></span><br><h2 class="width--100"> Error Loading, Reload Page</h2>';
                    }
                  };
                  xhttp.open("POST", gift_book_url, true);
                  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                  xhttp.send("address="+address+"&location="+location+"&address_details="+address_details+"&instructions="+instructions);         
                }
            });      
        $(document).ready(function(){ 
            $('#terms').nivoLightbox(); 
            $('#privacy').nivoLightbox(); 
            $('#cancellation').nivoLightbox(); 
        }); 