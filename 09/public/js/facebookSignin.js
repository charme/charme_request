  function fb_login() {
    FB.login( function(response) {console.log(response);}, { scope: 'email,public_profile',return_scopes: true } );
  }
// This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
        FB.login( function(response) {console.log(response);testAPI();}, { scope: 'email,public_profile',return_scopes: true } );
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
        FB.login( function(response) {console.log(response);testAPI();}, { scope: 'email,public_profile',return_scopes: true } );
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '508448699363738',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.5' // use graph api version 2.5
    });
  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me',{fields:['last_name','first_name','name','email','gender','picture']}, function(response) {
        //console.log(response);
        var pic_url=response.picture.data.url;
        FB.api(response.id+'/picture?type=large&redirect=false',function (pic_fb){
          if (response && !response.error) {
            console.log('pic is '+JSON.stringify(pic_fb));
            var pic_data=JSON.stringify(pic_fb);
            var pic_obj= JSON.parse(pic_data);
            var picture_fb=pic_obj.data.url;
            console.log('response object pic is '+picture_fb);
            //return picture_fb;
            };
          });
      //console.log(response.id);
       var checkFacebookId= new XMLHttpRequest();
                  checkFacebookId.onreadystatechange = function(){
                    if (checkFacebookId.readyState==4 && checkFacebookId.status==200) {
                      var result=JSON.parse(checkFacebookId.responseText);
                      console.log('Request '+checkFacebookId.responseText);
                       if (result.status=='ok') {
                        window.location.replace(services_url);                        
                      };
                      if (result.status!='ok') {
                        $('#social_login').fadeOut('fast');
                        $('#social_login_error').text('Account details not found, Kindly register').fadeIn('fast');
                      };
                    }
                  }
                  checkFacebookId.open("POST",facebook_url, true);
                  checkFacebookId.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                  checkFacebookId.send("fb_id="+response.id+"&pic_url="+pic_url);
      $('#social_login').text('Hi '+response.name+', welcome to Charmé').fadeIn('fast');
    });
  }