<?php

use Illuminate\Http\Request;

use App\Http\Requests;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Customer Routes. Routes to the customers usage on the app are defined here


*/
Route::group(['middleware' => ['web']], function () {

    Route::get('password-recovery/{token?}','PasswordController@password_recovery');
    Route::get('professional/password-recovery/{token?}','PasswordController@professional_password_recovery');
    Route::get('/','CustomerRegistrationController@welcome');

    Route::get('/signin', 'CustomerSigninController@welcome');

    Route::match(['get','post'],'/logout' ,function () {
        Session::flush();
        return redirect('/');
    });

    /*Route::get('register/facebook', 'CustomerRegistrationController@redirectToProvider');
    Route::get('register/facebook/callback', 'CustomerRegistrationController@handleProviderCallback');*/
    /*forgot password details*/
    Route::get('/forgot_password', 'CustomerSigninController@forgot_password');
    Route::get('professional/forgot_password', 'ProfessionalSigninController@forgot_password');
    
    /*reset password*/
    Route::post('/remember_password', 'CustomerSigninController@remember_password');
    Route::post('professional/remember_password', 'ProfessionalSigninController@remember_password');
    Route::post('/reset_password', 'CustomerSigninController@reset_password');
    Route::match(['post','get'],'professional/reset_password', 'ProfessionalSigninController@reset_password');

    /*Login into your account*/
    Route::post('/signin_as_customer', 'CustomerSigninController@login_request');
    Route::post('/signin/facebook', 'CustomerSigninController@facebook_login');
    Route::post('/signin/google', 'CustomerSigninController@google_login');

    Route::get('/register/customer/verify-phone' ,'CustomerRegistrationController@show_verify_phone');
    
    Route::get('/register/customer/{reference?}', 'CustomerRegistrationController@register_customer');

    Route::post('/register/customer', 'CustomerRegistrationController@send_registration_data');

    Route::post('/register/customer/verify-phone', 'CustomerRegistrationController@verify_phone');
    Route::get('/resend-sms', 'CustomerSigninController@resend_sms');

    /*Supplier registration*/
    Route::get('/register/professional','ProfessionalRegistrationController@register_professional');
    Route::post('/register/professional', 'ProfessionalRegistrationController@send_registration_data');

    Route::get('/signin/professional','ProfessionalSigninController@welcome');
    Route::post('/professional_signin', 'ProfessionalSigninController@login_request');


    Route::post('/register/professional/verify-phone', 'ProfessionalRegistrationController@verify_phone');
    Route::get('/register/professional/verify-phone' ,function () {
        return view('professionals_login.verify_phone');
    });
    
    Route::get('professional/resend-sms', 'ProfessionalSigninController@resend_sms');


    Route::get('/register/professional/more','ProfessionalRegistrationController@more_details');   
    Route::get('/register/add_new_services/{name?}' ,'ProfessionalRegistrationController@reg_services');
    Route::get('/register/save_services/{service_id?}' ,'ProfessionalRegistrationController@save_services');
    
    Route::get('/register/delete_services/{service_id?}' ,'ProfessionalRegistrationController@delete_services');
    
    Route::post('/register/professional/more','ProfessionalRegistrationController@update_more_details');     


    Route::get('/register/professional/success', function () {
        return view('Notifications.professional_notify');
    });


    Route::get('/register/success', function () {
        return view('Notifications.notify');
    });

    /*Payment notification*/
    Route::get('/services/payment','ServicesController@post_payment');
    Route::get('gifts/payment','GiftServiceController@postpayment');

    Route::get('/sessions',function(request $request){
            return $request->session()->all();
        });
});




Route::group(['middleware' => ['customer']], function () {
        /*Profile*/
        Route::get('/profile', 'CustomerProfileController@view_profile');

        Route::get('/profile/edit', 'CustomerProfileController@edit_profile');
        Route::get('/profile/location', 'CustomerProfileController@location');
        Route::post('/profile/location/save', 'CustomerProfileController@save_location');        

        Route::post('/profile/edit', 'CustomerProfileController@update_profile');

        Route::post('/profile/edit/dp', 'CustomerProfileController@update_dp');
        /*-----/Profile-----------*/

        /*Services*/

        Route::get('/services','ServicesController@get_categories');

        Route::get("/services/style/{id?}", 'ServicesController@get_services');       

        Route::match(['get','post'],'/services/professionals','ServicesController@get_professionals');
        Route::get('/services/professionals/profile/{id?}','ServicesController@get_professional_details');
       
        Route::get('/services/professionals/pro', function () {
            return view('modules.service_professionals_pro');
        });

        

        Route::POST('/services/instructions','ServicesController@save_instructions');

        Route::get('/services/location', 'ServicesController@get_location');

        Route::post('/services/request', 'ServicesController@save_request');
        Route::match(['get','post'],'/services/details', 'ServicesController@save_details');
        Route::post('/getcoupon','ServicesController@check_coupon');
        /*----------/Services-------------*/

        



        /*APPOINTMENT*/
        Route::get('/appointments','AppointmentController@show_appointments');
        /*Chat*/
        Route::get('/appointments/chat/{id}','AppointmentController@show_chat');
        Route::post('/appointments/chat/send','AppointmentController@send_chat');
        /*location*/
        Route::get('/appointments/location/{id}','AppointmentController@show_location');

        Route::get('/appointments/feedback', function () {
            return view('modules.feedback');
        });

        Route::post('/appointments/reschedule', 'AppointmentController@reschedule_appointment');
        Route::get('/appointments/cancel/{id}', 'AppointmentController@cancel_appointment');
        Route::post('appointments/end','AppointmentController@send_end');
        Route::post('/appointments/feedback', function () {
            return view('modules.feedback_submit');
        });

        Route::get('/appointments/completed/{id}','AppointmentController@confirm_service');
        Route::get('/appointments/deny', function () {
            return view('modules.feedback_deny');
        });

        /*Inbox*/
        Route::get('/inbox', 'InboxController@get_inbox');
        /*Invite A friend*/
        Route::get('/invite', 'InviteFriendController@view_invite');
        /*Send friend Invite*/
        Route::post('/invite-friend', 'InviteFriendController@send_invite');

        /*Gift A Service*/
        Route::get('/gift','GiftServiceController@view_gift' );
        Route::post('/gift/save_receiver','GiftServiceController@save_receiver' );
        Route::get('/gift/category','GiftServiceController@select_gift_category' );
        Route::get('/gift/service/{id}','GiftServiceController@select_gift_service' );
        Route::post('gift/makepayment','GiftServiceController@pay_for_gift');

        /*Redeem A Service*/
        Route::get('/redeem','RedeemServiceController@view_redeem' );   
        Route::post('/redeem/gift','RedeemServiceController@verify_gift');     
        Route::get('gift/professionals','RedeemServiceController@get_professionals');
        Route::get('gift/selected-pro/profile/{id?}','RedeemServiceController@get_professional_details');

        Route::POST('/gift/instructions','RedeemServiceController@save_instructions');

        Route::get('/gift/location', 'RedeemServiceController@get_location');
        Route::POST('/gift/book', 'RedeemServiceController@book_appointment');
        

        /*Customer Service*/
        Route::get('/customer_service', 'CustomerServiceController@view_customer_service');
        /*Open send message view*/
        Route::get('customer_service/message/{title?}', 'CustomerServiceController@show_message');
        Route::post('customer_service/send_message', 'CustomerServiceController@send_message');
        /*Settings--- THis includes: 
        1. CHange Password.
        2. Sign Out.
        3. Rate us on Google Play.
        4. Follow CHarme on Instagram.
        5. Follow Charme on Twitter.
        6. Terms & Condition.
        7. Privacy Policy.
        8. Cancellation Policy*/

        Route::get('/settings', function () {
            return view('modules.settings');
        });

        Route::get('/settings/password','SettingsController@change_password');

        Route::post('/settings/change-password','SettingsController@send_change_password');

        /*END of CUstomer ROutes*/
});




/*Professionals Routes*/
Route::group(['middleware' => ['supplier']], function () {
   /*Profile details*/
   

    Route::get('professional/profile', 'ProfessionalProfileController@view_profile');
    Route::get('professional/services','ProfessionalServiceController@view_services');
    Route::post('professional/services/save','ProfessionalServiceController@save_services');
    Route::post('professional/available','ProfessionalServiceController@toggle_available');



    Route::get('professional/profile/location', 'ProfessionalProfileController@location');
    Route::post('professional/profile/location/save', 'ProfessionalProfileController@save_location'); 

    Route::get('professional/profile/edit', 'ProfessionalProfileController@edit_profile');
    Route::post('professional/profile/edit', 'ProfessionalProfileController@update_profile');
    Route::post('professional/profile/edit/dp', 'ProfessionalProfileController@update_dp');
    Route::post('professional/profile/description', 'ProfessionalProfileController@update_description');


    


    /*Professional Inbox*/
    Route::get('professional/inbox','ProfessionalInboxController@get_inbox');
    /*Professional Appointments*/
    Route::get('professional/appointments','ProfessionalAppointmentController@show_appointments');
    Route::match(['get','post'],'professional/appointments/start','ProfessionalAppointmentController@start_appointment');

    Route::get('professional/appointments/details/{id}','ProfessionalAppointmentController@get_details');
    /*chat*/
    Route::get('professional/appointments/chat/{id}','ProfessionalAppointmentController@show_chat');
    Route::post('professional/appointments/chat/send','ProfessionalAppointmentController@send_chat');
    /*Location*/
    Route::get('professional/appointments/location/{id}','ProfessionalAppointmentController@show_location');

    Route::get('professional/appointments/cancel/{id}', 'ProfessionalAppointmentController@cancel_appointment');
    Route::post('professional/appointments/end','ProfessionalAppointmentController@send_end');

    Route::get('professional/appointments/completed/{id}','ProfessionalAppointmentController@end_service');

            /*Route::get('professional/client-info', function () {
                return view('professional_modules.client_info');
            });

            Route::get('professional/client-info/request', function () {
                return view('professional_modules.client_info_request');
            });
            Route::get('professional/client-info/reschedule', function () {
                return view('professional_modules.client_info_reschedule');
            });*/

            /*Route::get('professional/client-location', function () {
                return view('professional_modules.client_location');
            });*/
    /*Appointment*/

    /*Customer Service*/
        Route::get('professional/customer_service', 'ProfessionalCustomerServiceController@view_customer_service');
        /*Open send message view*/
        Route::get('professional/customer_service/message', 'ProfessionalCustomerServiceController@show_message');
        Route::post('professional/customer_service/send_message', 'ProfessionalCustomerServiceController@send_message');
    /*/Customer service*/

    /*Earnings*/
    Route::get('professional/earnings','EarningsController@get_earnings');

    Route::get('professional/earnings/breakdown/{month?}', 'EarningsController@month_breakdown');
    Route::post('professional/earnings/account', 'EarningsController@update_account');

     /*Settings--- THis includes: 
        1. CHange Password.
        2. Sign Out.
        3. Rate us on Google Play.
        4. Follow CHarme on Instagram.
        5. Follow Charme on Twitter.
        6. Terms & Condition.
        7. Privacy Policy.
        8. Cancellation Policy*/

        Route::get('professional/settings','ProfessionalSettingsController@show_settings');
       

        Route::get('professional/settings/password','ProfessionalSettingsController@change_password');

        Route::post('professional/settings/change-password','ProfessionalSettingsController@send_change_password');
});
/*End of professional Routes*/

Route::get('/api', 'ApiController@api');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

