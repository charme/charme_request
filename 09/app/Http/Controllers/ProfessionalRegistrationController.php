<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class ProfessionalRegistrationController extends Controller
{	/*To keep the user-id and token all through every method*/
	

    public function register_professional(){
    	$data = array();
    	return view('professionals_login.register')->with('data',$data);
    }

    public function send_registration_data(Request $request){
    	$data = array();

    	/*Send Professionals Details*/
    	$input = $request->except(['confirm_password']);
        //return $input;
    	$response = $this->charmeapi()->request('POST', 'suppliers', ['form_params' => $input])->getBody();
    	$responseBody = $this->ArrayResponse($response);
        //return $responseBody;
    	$data= array_add($data,'response',$responseBody);
    	
    	/*Send verification code to phone number on successful registration*/
    	if($responseBody['status']==='ok'){
        	$id=$responseBody['data']['Supplier']['id'];
        	/*Save ID in session*/
        	Session::put('signup_id',$id);
        	$token=$responseBody['data']['Supplier']['token'];
        	/*Save token in session*/
        	Session::put('signup_token',$token);

        	/*Send verification SMS to supplier phone*/
        	$verify_phone = $this->charmeapi()->request('GET', 'suppliers/'.$id.'/verify-phone?token='.$token)->getBody();
        	$verify_phoneBody =  $this->ArrayResponse($response);
        	$data= array_add($data,'verify-phone',$verify_phoneBody);
            //return $data;
        		/*Check if verification code was successfully sent*/
    	    	if($data['verify-phone']['status']==='ok' && !empty($data['verify-phone']['status'])){
    	    		return redirect('register/professional/verify-phone')->with('status','Registration successfully');
    	    	}
    	    	else
    	    	$sms_error = array('error' => 'SMS verification sending failed.', 'id'=>$id, 'token'=>$token );
    	    	 return redirect('register/professional/verify-phone')->with('error', 'SMS verification sending failed.');
    	}

    	/*Failed Send Customer Data*/
    	elseif ($responseBody['status']==='error') {
    	return redirect('register/professional')->with('error',$responseBody['error']['msg']);
    	}

    	else{
    		return redirect('register/professional');
    	}
    }

    /*To resend code incase of failure*/
    public function resend_code(Request $request){
    	$data = array('meta' => 'nothing here');
    	$id=Session::get('signup_id');
    	$token= Session::get('signup_token');
    	$verify_phone = $this->charmeapi()->request('GET', 'suppliers/'.$id.'/verify-phone?token='.$token)->getBody();
    	$verify_phoneBody = $this->ArrayResponse($verify_phone);
    	$data= array_add($data,'verify-phone',$verify_phoneBody);
    	return view('professionals_login.verify_phone')->with('data',$data);
    }

    public function verify_phone(Request $request){
    	$phone_code= $request->code;
    	$id=Session::get('signup_id');
    	//return $id;
    	$token= Session::get('signup_token');
    	//return $token;
    	$data = array();
    	$send_verification = $this->charmeapi()->request('POST',"suppliers/{$id}/verify-phone?token={$token}", ['form_params' => ['code'=>$phone_code]])->getBody();
    	/*Convert response to array*/
    	$send_verification = $this->ArrayResponse($send_verification);
    	//return $send_verification;
    	/*End of conversion*/
    	/*Redirect if phone number is verified*/
    	if ($send_verification['status']==='ok') {
             /*Saves id and token used in middleware for logged-in*/
                Session::put('supplier_id',$id);
                Session::put('supplier_token',$token);
            //return redirect('/register/professional/more');
    		$data= array_add($data,'registration_successful','Registration Successful, Enjoy the Charmé App');
                return redirect('/register/professional/more')->with('registration_successful','A few more details please'); 
    	}
    	elseif ($send_verification['status']==='error') {
    		$data= array_add($data,'send_verification',$send_verification);
    		return view('professionals_login.verify_phone')->with('data',$data);
    	}
    	else return view('professionals_login.verify_phone');
    	
    }

    public function more_details(){

        $response = $this->charmeapi()->request('GET', "categories")->getBody();
        $responseBody = $this->ArrayResponse($response);         
        $categories = $responseBody['data']['Categories'];
        //return $categories;
        return view('professionals_login.professional_register_more',['categories'=> $categories]);
    }

    public function reg_services($name = null) {
        $id = $name;
        $s_id = session('signup_id');
        $ss = $this->charmeapi()->request('GET', "suppliers/{$s_id}/services?raw=yes")->getBody();
            $get_services= $this->ArrayResponse($ss);
            //return $get_services;
            $supplier_services=$get_services['data']['SupplierServices'];
        //return $supplier_services;
        $response = $this->charmeapi()->request('GET', "categories/{$id}/services")->getBody();
        $responseBody = $this->ArrayResponse($response);         
        $services = $responseBody['data']['Services'];
        $data= array();
        $data=array_add($data,'services',$services);
        $data=array_add($data,'supplier_services',$supplier_services);
        //return $data;

        return view('sub_view.services',['data'=>$data]);
    }

    public function save_services($service_id){
        $id=Session::get('signup_id');
        //return $id;
        $save_id=$service_id;
        //return $save_id;
        $token= Session::get('signup_token');
                    //return 'I"ll register the service';
                     $send_services = $this->charmeapi()->request('POST',"suppliers/{$id}/services", ['form_params' => [
                        'service_id'=>$save_id,
                        'id'=>$id,
                        'token'=>$token,
                        'available_from' => '00:00',
                        'available_to' => '23:59',
                        ]])->getBody();
                    /*Convert response to array*/
                    $send_services = $this->ArrayResponse($send_services);
                    return $send_services;         

    }
    public function delete_services($service_id){
        $id=Session::get('signup_id');
        //return $id;
        $delete_id=$service_id;
        //return $sdelete_id;
        $token= Session::get('signup_token');
        $send_services = $this->charmeapi()->request('DELETE',"suppliers/{$id}/services/{$delete_id}")->getBody();
                    /*Convert response to array*/
                    $send_services = $this->ArrayResponse($send_services);
                    return $send_services;
    }
    public function update_more_details(request $request){
        $extra= $request->all();     
        //return $extra;   
        $supplier_id = session('signup_id');
        $supplier_token= Session::get('signup_token');
        $extra = array_except($extra,'service_category');
        //return $supplier_id;
        Session::put('supplier_id',$supplier_id);
        Session::put('supplier_token',$supplier_token);
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$extra])->getBody();
        $response = $this->ArrayResponse($response);
        //return $response;
        return redirect('/professional/profile')->with('status','Registration Successful, Enjoy the Charmé App');
    }
}
