<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class RedeemServiceController extends Controller
{
    public function view_redeem(){
    	return view('modules.redeem_a_service');
    }
    public function verify_gift(request $request){
    	//return $request->all();
    	$code= $request->code;
    	$response = $this->charmeapi()->request('GET', "gifts?code={$code}")->getBody();
    	$response= $this->ArrayResponse($response);
    	return $response;
    }

    public function get_professionals(request $request) {
        //return $request->all();
        $service_id= $request->service_id;
        if ($service_id!='') {
            Session::put('gift_service_id',$service_id);            
        }
        else {
            $service_id=Session::get('gift_service_id');
        }
        
        $appointment_time=$request->appointment_time;
        if ($appointment_time!='') {
            Session::put('gift_appointment_time',$appointment_time);            
        }
        else {
            $appointment_time=Session::get('gift_appointment_time');
        }
        //Session::put('gift_appointment_time',$appointment_time);        
        $gift=$request->gift_code;
        if ($gift!='') {
            Session::put('gift_code',$gift);            
        }
        else {
            $gift=Session::get('gift_code');
        }
        //Session::put('gift_code',$gift);   
        $appointment_time= date_format(date_create($appointment_time),"Y-m-d H:i");
        $user_details = $this->UserDetails();
        $response = $this->charmeapi()->request('GET', "services/{$service_id}/suppliers")->getBody();
        $available_professionals= $this->ArrayResponse($response);
            return view('modules.gift_professionals',[
                'available_professionals'=>$available_professionals['data']['Suppliers'],
                'user_details'=>$user_details,]);
    }

    public function get_professional_details($id=null){
        if ($id!=null) {
            $professional_id=$id;
            Session::put('gift_pro_id',$id);
        } else {
            $professional_id=Session::get('gift_pro_id');
        }
        
        //return $professional_id;
        $service_id=Session::get('gift_service_id');
        //return $service_id;

        /*Get Service Details*/
        $appointment_time=Session::get('appointment_time');
        /*Convert Date time string*/
        $date=date_create($appointment_time);
        $appointment_time =date_format($date,"h:i A");        
        //return $appointment_time;

        $service_details = $this->charmeapi()->request('GET', "services/{$service_id}")->getBody();
        $service_details= $this->ArrayResponse($service_details);
        $service_details=$service_details['data']['Service'];
        $service_details=array_add($service_details,'appointment_time',$appointment_time);
        //return $service_details;

        /*Get Category Details*/
        $category_id=$service_details['category_id'];
        $category_details = $this->charmeapi()->request('GET', "categories/{$category_id}")->getBody();
        $category_details = $this->ArrayResponse($category_details);
        $category_details = $category_details['data']['Category'];
        //return $category_details;

        /*Get Professional's details*/
        $response = $this->charmeapi()->request('GET', "suppliers/{$professional_id}")->getBody();
        $professional= $this->ArrayResponse($response);
        $professional_details=$professional['data']['Supplier'];
        session::put('gift_pro_id',$professional_id);
        //return $professional_details;

        /*Add Service Details, category details and professional's details together*/
        $data = array();
        $data=array_add($data,'service_details',$service_details);
        $data=array_add($data,'category_details',$category_details);
        $data=array_add($data,'professional_details',$professional_details);

        return view('modules.gift_professional_profile',['data'=>$data]);
    }

    /*Returns view to add location and extra details*/
    public function get_location(request $request){
        //return $request->session()->all();
        $professional_id=session('gift_pro_id');
        $response = $this->charmeapi()->request('GET', "suppliers/{$professional_id}")->getBody();
        $professional= $this->ArrayResponse($response);
        $professional_details=$professional['data']['Supplier'];
        //return $professional_details;
        $data = array('Supplier' => $professional_details);
        //return $data;
        return view('modules.gift_location',['data'=>$data]);

        //return view('modules.gift_location');
    }

    /*Book Appointment for gift*/
    public function book_appointment(request $request){
        /*Get previous service details and store in an array to post*/
        $instructions = $request->instructions;
        /*save latlong*/
        $latlong = $request->location;
        if (!empty($request->address_details)) {
        $address_details=$request->address.'('.$request->address_details.')';
        }
        else $address_details=$request->address;          
        $service_id=session('service_id');

         $service_request=array(
            'customer_id'=>session('customer_id'),
            'supplier_id'=>(int) session('gift_pro_id'),
            'service_id'=>(int) session('gift_service_id'),
            'start_at'=>session('gift_appointment_time'),
            'additional_message'=>$instructions,
            /*if you notice, I'm adding the meetup_address afterwards*/
            'gift'=>session('gift_code'),
            'token'=>session('customer_token'),
            );
         
        /*Add address details with address*/
        //$address_details=session('address_details');
        /*Get LatLong*/
        //$latlong = session('latlong');
        
       
        $latlong = trim($latlong,'(');
        $latlong = trim($latlong,')');
        $location_split= explode(",", $latlong);
        $lat=(float) trim($location_split[0]);
        $long=(float) $location_split[1];

        $meetup_address = array('latitude' => $lat,'longitude'=>$long, 'address_details'=> $address_details);
        /*Here, I add meet-up address details to the request to send*/
        $service_request=array_add($service_request,'meetup_address',json_encode($meetup_address));
        //return $service_request;
        $response = $this->charmeapi()->request('POST', "requests",['form_params' => $service_request])->getBody();
        $response = $this->ArrayResponse($response);
        return $response;
    }

}
