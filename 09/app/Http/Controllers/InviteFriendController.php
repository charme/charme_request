<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class InviteFriendController extends Controller
{
    public function view_invite(){
            return view('modules.invite_a_friend');
    }
    public function send_invite(request $request){

    	$customer_id=Session::get('customer_id');    	
        $customer_token=Session::get('customer_token'); 
    	//return $request->all();      
    	$invite = array('id' => $customer_id,'email'=>$request->email);
 		$response = $this->charmeapi()->request('POST', "customers/{$customer_id}/invite?token={$customer_token}",["form_params"=>$invite])->getBody();
 		$response = $this->ArrayResponse($response);
    	//return $response;
    	if ($response['status']=='ok') {
    		return redirect('/invite')->with('status','Invitation Sent');
    	}

    	if ($response['status']=='error') {
    		return redirect('/invite')->with('error',$response['error']['msg']);
    	}
    	else return redirect('/invite');
    }
}
