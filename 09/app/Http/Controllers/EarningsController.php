<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class EarningsController extends Controller
{
    public function get_earnings() {
    	$supplier_id=Session::get('supplier_id');
    	$token=session('supplier_token');
    	$response = $this->charmeapi()->request('GET', "suppliers/{$supplier_id}/earnings?token={$token}")->getBody();
        $response = $this->ArrayResponse($response);
        //return $response;
        return view('professional_modules.earnings',['earnings'=>$response]);
    }
    public function month_breakdown($month= null) {
    	$supplier_id=Session::get('supplier_id');
    	$token=session('supplier_token');
    	$response = $this->charmeapi()->request('GET', "suppliers/{$supplier_id}/earnings?token={$token}&date={$month}")->getBody();
        $response = $this->ArrayResponse($response);
    	//return $response;
    	if ($response['status']=='ok') {
    		$total=0;    
    		$breakdown=$response['data']['EarningsBreakdown'];
    		foreach ($breakdown as $value) {
    			$total+=$value['total_amount'];
    		}
    	}
    	if ($response['status']=='error') {
    		return redirect('professional/earnings');
    	}
        return view('professional_modules.earnings_breakdown',['month_earnings'=>$response,'month'=>$month,'total'=>$total]);
    }

    public function update_account(request $request){
    	//return $request->all();
    	$supplier_id=Session::get('supplier_id');
        $supplier_token=Session::get('supplier_token');
        $update_account = array('bank_name' => $request->bank_name,'account_no'=>$request->account_no);
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$update_account])->getBody();
        $response = $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {
        	return redirect('professional/earnings')->with('status','Bank Details Updated Successfully');
        }
        if ($response['status']=='error') {
        	return redirect('professional/earnings')->with('error',"Failed to Update, {$response['error']['msg']}");
        }

        else return redirect('professional/earnings');
    }
}
