<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Socialite;

class CustomerRegistrationController extends Controller
{	/*To keep the user-id and token all through every method*/
	
    public function welcome(){
	   return view('login.index');
    }

    public function register_customer($reference=''){
    	return view('login.register',['reference'=>$reference]);
    }

    public function redirectToProvider(){
        return Socialite::driver('facebook')->scopes(['public_profile', 'email'])->redirect();
    }

    public function handleProviderCallback(request $request){
        $user = Socialite::driver('facebook')->user();
        return $user;
    }

    public function send_registration_data(Request $request){
    	$data = array();
        /*Send Customer Details*/
        //$input = $request->except(['confirm_password']);
        $input = array(
            'first_name' => $request->first_name,
            'last_name'=> $request->last_name,
            'gender'=> $request->gender,
            'email'=>$request->email,
            'phone_no'=>$request->phone_no,            
         );
        if ($request->fb_id!='') {
            $input=array_add($input,'facebook_id',$request->fb_id);
            $input=array_add($input,'signup_source','facebook');
        }
        if ($request->g_token!='') {
            $g_token=$request->g_token;
            $response = \Httpful\Request::post("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=$g_token")->expectsJson()->send();
            $response= json_decode((string) $response,true);
            $g_id= $response['sub'];            
            $input=array_add($input,'google_id',$g_id);
            $input=array_add($input,'signup_source','google');
        }
        if ($request->fb_id=='' && $request->g_token=='') {
            $input=array_add($input,'password',$request->password);

        }
        if ($request->reference_code!='') {
            $input=array_add($input,'reference_code',$request->reference_code);
        }
        if ($request->pic_url!='') {
            session::put('social_pic',$request->pic_url);
        }
        //return $input;
        $response = $this->charmeapi()->request('POST', 'customers', ['form_params' => $input])->getBody();
        $responseBody = (string) $response;
        $responseBody = json_decode($responseBody, true);
        $data= array_add($data,'response',$responseBody);
        print_r($data);
        
        /*Send verification code to phone number*/
        if($responseBody['status']==='ok'){
            $id=$responseBody['data']['Customer']['id'];
            /*Save ID in session*/
            Session::put('signup_id',$id);
            $token=$responseBody['data']['Customer']['token'];
            /*Save token in session*/
            Session::put('signup_token',$token);

            /*Send verification SMS to user phone*/
            $verify_phone = $this->charmeapi()->request('GET', 'customers/'.$id.'/verify-phone?token='.$token)->getBody();
            $verify_phoneBody = (string) $verify_phone;
            $verify_phoneBody = json_decode($verify_phoneBody, true);
            $data= array_add($data,'verify-phone',$verify_phoneBody);
            /*Check if verification code was successfully sent*/
                if($data['verify-phone']['status']==='ok'){
                    return redirect('register/customer/verify-phone')->with('status','Registration Successful');
                }
                if ($data['verify-phone']['status']==='error') {                    
                    return redirect('register/customer/verify-phone')->with('error', 'Error Sending SMS');
                }
                else return redirect('register/customer/verify-phone');
        }

        /*Failed Send Customer Data*/
        elseif ($responseBody['status']==='error') {
        return redirect('register/customer')->with('error',$responseBody['error']['msg']);
        }
        /*I dont know the error so redirect to registration page*/
    	else{
    		return redirect('register/customer');
    	}
    }

    /*To resend code incase of failure*/
    public function resend_code(Request $request){
    	$data = array();
    	$id=Session::get('signup_id');
    	$token= Session::get('signup_token');
    	$verify_phone = $this->charmeapi()->request('GET', 'customers/'.$id.'/verify-phone?token='.$token)->getBody();
    	$verify_phoneBody = (string) $verify_phone;
    	$verify_phoneBody = json_decode($verify_phoneBody, true);
    	$data= array_add($data,'verify-phone',$verify_phoneBody);
    	return redirect('customer/verify-phone');
    }

    public function verify_phone(Request $request){
    	$phone_code= $request->code;

        if ($request->session()->has('customer_id')) {            
        $request->session()->forget('customer_id');
        }
        if ($request->session()->has('customer_token')) {            
        $request->session()->forget('customer_token');
        }
    	$id=Session::get('signup_id');
    	$token= Session::get('signup_token');
    	$data = array();
    	$send_verification = $this->charmeapi()->request('POST',"customers/{$id}/verify-phone?token={$token}", ['form_params' => ['code'=>$phone_code]])->getBody();
    	/*Convert response to array*/
    	$send_verification = (string) $send_verification;
    	$send_verification = json_decode($send_verification, true);
    	
    	/*End of conversion*/
    	/*Redirect if phone number is verified*/
    	if ($send_verification['status']==='ok') {

            /*Saves id and token used in middleware for logged-in*/
                Session::put('customer_id',$id);
                Session::put('customer_token',$token);
                Session::put('verified',true);
    		$data= array_add($data,'registration_successful','Registration Successful, Enjoy the Charmé App');
    		return redirect('/services')->with('status','Registration Successful, Enjoy the Charmé App');
    	}
        /*SMS verification failed*/
    	elseif ($send_verification['status']==='error') {
    		$data= array_add($data,'send_verification',$send_verification);
    		return redirect('register/customer/verify-phone')->with('error',$send_verification['error']['msg']);
    	}
        /*Return by default*/
    	else return redirect('register/customer/verify-phone');
    	
    }

    public function show_verify_phone(){
        return view('login.verify_phone');
    }
}
