<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
class ApiController extends Controller
{
	public function __construct()
	{

	
	}

	public function api(){
    $response = $this->charmeapi()->request('GET', '/customers');
    return $response->getBody();
	}
}
