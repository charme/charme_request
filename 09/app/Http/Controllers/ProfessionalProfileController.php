<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use File;
use charmeapi;
class ProfessionalProfileController extends Controller
{
    public function view_profile(){
    	$data = array();
    	$supplier_id=Session::get('supplier_id');
    	$response = $this->charmeapi()->request('GET', "suppliers/{$supplier_id}")->getBody();
    	$response = $this->ArrayResponse($response);
        //return $response;
        /*$location = array_get($response, 'data.Supplier.location');
        $location= $this->ArrayResponse($location);
        array_set($response,'data.Supplier.location',$location['address_details']);*/
        //array_forget($response,'data.Supplier.location');
        $data = array_add($data,'response',$response);
        //return $data;
    	return view('professional_modules.profile')->with('data', $data);
    }

    public function edit_profile(){
    	/*Get existing User Data and prepopulate the form*/
    	
    	$data = array();
    	$supplier_id=Session::get('supplier_id');
    	$response = $this->charmeapi()->request('GET', "suppliers/{$supplier_id}")->getBody();
    	$response = (string) $response;
    	$response = json_decode($response,true);
    	$data = array_add($data,'response',$response);

    	return view('professional_modules.edit_profile')->with('data', $data);
    }

 	public function update_profile(request $request){ 		
 		$updated_details=$request->all();
 		//return $updated_details;
 		$data = array();
    	$supplier_id=Session::get('supplier_id');
        $supplier_token=Session::get('supplier_token');

 		$response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$updated_details])->getBody();
 		$response = $this->ArrayResponse($response);
    	$data = array_add($data,'response',$response);
 		//return $data;
 		if ($data['response']['status']==='ok') {
 			
    	return redirect('professional/profile')->with('profile_updated','Profile Successfully Updated');
 		}
        else return redirect('professional/profile/edit')->with('Update_failed','Profile Update Failed, Try Again');
    }   

    public function update_description(request $request){
        //$updated_details=$request->description;
        $supplier_id=Session::get('supplier_id');
        $supplier_token=Session::get('supplier_token');
        $update_description = array('description' => $request->description, );
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$update_description])->getBody();
        $response = $this->ArrayResponse($response);
        return $response;
    }

    public function location(request $request){
        return view('professional_modules.profile_location');
    }

    public function save_location(request $request){
        /*save latlong*/
        $latlong = $request->location;
        if (!empty($request->address_details)) {
        $address_details=$request->address.'('.$request->address_details.')';
        }
        else $address_details=$request->address;
        $latlong = trim($latlong,'(');
        $latlong = trim($latlong,')');
        $location_split= explode(",", $latlong);
        $lat=(float) trim($location_split[0]);
        $long=(float) $location_split[1];

        $save_address = array('latitude' => $lat,'longitude'=>$long, 'address_details'=> $address_details);
        $save_address=json_encode($save_address);
        //return $save_address;
        $supplier_id=Session::get('supplier_id');
        $supplier_token=Session::get('supplier_token');
        $location = array('location' => $save_address, );
        $response = $this->charmeapi()->request('POST', "suppliers/{$supplier_id}?token={$supplier_token}",["form_params"=>$location])->getBody();
        $response = $this->ArrayResponse($response);
        if ($response['status']=='ok') {
            return redirect('professional/profile')->with('status','Location saved');
        }
        if ($response['status']=='error') {
            return redirect('professional/profile')->with('error',"Location not saved, {$response['error']['msg']}");
        }
        else return redirect('professional/profile');
    }
    public function update_dpNEW(request $request){
        $token=session('supplier_token');
        $id=session('supplier_id');
        $contents = File::get($request->file('pics'));
        //return $contents;
         $data = array('pics' => $request->file('pics'));
         $pics = array(
            'pics' => [
                'name'=>$request->file('pics')->getClientOriginalName(),
                'type'=>$request->file('pics')->getMimetype(),
                'tmp_name'=>File::get($request->file('pics')),
                'error'=>$request->file('pics')->getError(),
                'size'=>$request->file('pics')->getSize(),
            ]);
        $uri=getenv("CHARME_API");
        $pics = json_encode($pics);
        $tmp_name=$request->file('pics')->getRealPath();
        $fields = array("pics" => $request->file('pics')->getClientOriginalName());
        $fields["file"] = fopen($tmp_name, 'rb');
        $pic_uplod = new charmeapi(array(
            'request.options' => array(
            'headers' => array('Content-Type' => 'multipart/form-data'),
            )
        ));
        $request = $pic_uplod->request("suppliers/$id/profile-pics?token=$token", array(), array(
            'name' => 'pics',
            'file_field'   => $tmp_name
        ));
        //$response = $request->send();
        $data = $request->json();
        return $data;
        /*$response = $this->ArrayResponse($request);
        return $response;*/

        $response = $this->charmeapi()->request('POST', $url."/suppliers/{$id}/profile-pics?token={$token}",
            array('Content-Type => multipart/form-data'),
            ['form_params'=>$pics],
            array(),
            $fields)->getBody();
        $response = $this->ArrayResponse($response);
        return $response;
        if ($response['status']=='ok') {
            return redirect('professional/profile')->with('status','Profile picture changed');
        }
        if ($response['status']=='error') {
            return redirect('professional/profile')->with('error',"Profile picture not changed, {$response['errror']['msg']}");
        }
        else return redirect('professional/profile');
    }


    public function update_dp(request $request){
        $token=session('supplier_token');
        $id=session('supplier_id');
         $pics = array('pics' => $request->file('pics'));
        $uri=getenv("CHARME_API");
         $response = \Httpful\Request::post($uri."suppliers/$id/profile-pics?token=$token")->expectsJson()
         ->addHeader('Accept', "application/json")
         ->addHeader('Content-Type', "multipart/form-data");
         $response=$response->attach($pics);
         $response=$response->send();
        $response= json_decode((string) $response,true);
        if ($response['status']=='ok') {
            return redirect('professional/profile')->with('status','Profile picture changed');
        }
        if ($response['status']=='error') {
            return redirect('professional/profile')->with('error',"Profile picture not changed, {$response['errror']['msg']}");
        }
        else return redirect('professional/profile');
    }
}
