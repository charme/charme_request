<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class CustomerServiceController extends Controller
{
    public function view_customer_service(){    	
        return view('modules.customer_service');
    }
    public function send_message(request $request){
    	$customer_id=Session::get('customer_id');    	
        $customer_token=Session::get('customer_token'); 
    	//return $request->all();      
    	$message = array('subject'=>$request->title, 'message'=>$request->message);
 		$response = $this->charmeapi()->request('POST', "customers/{$customer_id}/inbox?token={$customer_token}",["form_params"=>$message])->getBody();
 		$response = $this->ArrayResponse($response);
 		//return $response;

 		if ($response['status']=='ok') {
 			return redirect('customer_service/message')->with('status','Your message has been recieved, we will contact you shortly');
 		}
 		if ($response['status']=='error') {
 			return redirect('customer_service/message')->with('error','Error Sending Message'+$response['error']['msg']);
 		}

 		else return redirect('customer_service/message');
    }

    public function show_message(request $request,$title=null){
        if ($title==null) {
            $title='';
        }
        return view('modules/contact_form',['title'=>$title]);
    	//return view('modules/contact_form');
    }
}
