<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class GiftServiceController extends Controller
{
    public function view_gift() {
            return view('modules.gift_a_service');
        }
    public function save_receiver(request $request){
    	session::put('gift_receiver',$request->all());
    	//return $request->all();
    	return redirect('gift/category');
    }

    public function select_gift_category(){
    	$response = $this->charmeapi()->request('GET', 'categories')->getBody();
    	$responseBody = $this->ObjectResponse($response);
    	return view('modules.gift_category', ['response'=>$responseBody]);
    }

    public function select_gift_service($id){
    	$response = $this->charmeapi()->request('GET', "categories/".$id)->getBody();
    	$category_details= $this->ArrayResponse($response);
    	$category_name= $category_details['data']['Category']['name'];
        
    	$response = $this->charmeapi()->request('GET', "categories/".$id."/services")->getBody();
		$responseBody = $this->ArrayResponse($response); 
		$responseBody = array_add($responseBody,'category_name',$category_name);
    	return view('modules.gift_service', ['response'=>$responseBody]);
    }

    public function pay_for_gift(request $request){
    	$id=session('customer_id');
    	$token=session('customer_token');
    	$gift = array('id' => session('customer_id'),
    		'service_id'=>$request->service_id,
    		'name'=>session('gift_receiver')['name'],
    		'phone_no'=>session('gift_receiver')['phone'],
    		'email'=>session('gift_receiver')['email']
    	 );
    	$response = $this->charmeapi()->request('POST', "customers/{$id}/gifts?token={$token}", ['form_params' => $gift])->getBody();    	
		$responseBody = $this->ArrayResponse($response); 
    	return $responseBody;
    }

    public function postpayment(request $request){
    	//return $request->response;
    	$gifts =$request->response;
    	//return $gifts;
    	$gifts=$this->ArrayResponse($gifts);
    	if ($gifts['status']=='ok') {    		
    		return view('modules.gift_payment')->with('gift',$gifts);
    	}

    	elseif ($gifts['status']=='error') {
    		return view('modules.gift_payment')->with('gift',$gifts);
    	}

    	else return 'Wrong Move';
    }
}
