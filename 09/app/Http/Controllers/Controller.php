<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use charmeapi;
use Session;
use Carbon\Carbon;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    


    static  function charmeapi(){
    	return new charmeapi(['base_uri' => 'http://charmeapp.herokuapp.com/api/v1/']);
    }
    static function ObjectResponse($response){    	
    	$responseC= $response->getContents();
    	$string = (string) $responseC;
		$responseBody=json_decode($string);
		return $responseBody;
    }
    static function ArrayResponse($response){
    	$response = (string) $response;
    	$responseBody = json_decode($response,true);
    	return $responseBody;
    }

    static function MySessions(){
        $sessions = Session::all();
        return $sessions;
    }

    static function UserDetails(){
        if (Session::has('customer_id')) {
            $user_id=Session::get('customer_id');            
            $user_details = Controller::charmeapi()->request('GET', 'customers/'.$user_id)->getBody();
            $user_details = Controller::ArrayResponse($user_details);
            return $user_details['data']['Customer'];
            //Session::put('user_details',);
        }
    }

    static function ProfessionalDetails(){
        if (Session::has('supplier_id')) {
            $user_id=Session::get('supplier_id');            
            $user_details = Controller::charmeapi()->request('GET', 'suppliers/'.$supplier_id)->getBody();
            $user_details = Controller::ArrayResponse($user_details);
            return $user_details['data']['Supplier'];
            //Session::put('user_details',);
        }
    }

    static function date_app($start_at){
        $app_date=date_create($start_at);
        $today=date_create(date("Y-m-d h:i:s"));
        if ($today==$app_date) {
          return 'Today';
        }
        if ($today>$app_date) {
          return " ";
        }

        if ($today<$app_date) {
          $diff=$today->diff($app_date,false);
          return $diff->format("%a days left");
        }
        else return " ";
    }
}
