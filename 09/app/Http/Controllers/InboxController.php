<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class InboxController extends Controller
{
    public function get_inbox(){
    	$customer_id=Session::get('customer_id');    	
        $customer_token=Session::get('customer_token'); 
 		$response = $this->charmeapi()->request('GET', "customers/{$customer_id}/inbox?token={$customer_token}")->getBody();
 		$response = $this->ArrayResponse($response);
 		//
 		if ($response['status']=='ok') {
 			//print_r($response);
            return view('modules.inbox',['inbox'=> $response['data']['Inboxes']]); 			
 		}
 		 else return view('modules.inbox');

    }
}
