       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Invite a friend -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <i class="fa fa-envelope fa-5x mdl-color-text--pink"></i>
                    <h4>Send Us A Message</h4>  
                    <div class="bold">
                      <?php if(session('error')): ?>
                        <div class="mdl-color--red mdl-color-text--white middle border-radius--5px padding--5px">
                          <?php echo e(session('error')); ?>

                        </div>
                      <?php endif; ?>

                      <?php if(session('status')): ?>
                        <div class="mdl-color--green mdl-color-text--white  middle border-radius--5px padding--5px">
                          <?php echo e(session('status')); ?>

                        </div>
                      <?php endif; ?>
                      <form action="<?php echo e(url('customer_service/send_message')); ?>" name="contact_customer_care" id="contact_customer_care" method="post">

                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                            <input class="mdl-textfield__input" type="text" id="title" name="title">
                            <label class="mdl-textfield__label" for="title">Title</label>
                          </div>

                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                            <textarea class="mdl-textfield__input" type="text" name="message" rows= "4" id="message" ></textarea>
                            <label class="mdl-textfield__label" for="message">Message...</label>
                          </div>

                          <span class="contact_customer_care_errors mdl-color-text--red"></span>

                          <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                            Send Message
                          </button> 
                      </form>
                    </div>
                  </div>
                </div>
                  <!-- /Invite a friend -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>