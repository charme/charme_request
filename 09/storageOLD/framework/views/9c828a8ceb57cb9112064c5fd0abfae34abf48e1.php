       
       <?php $__env->startSection('content'); ?>
    <!-- Page Content -->
        
          <main class="contact-about white_bg">
            <div class="mdl-color-white central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
              <form action="<?php echo e(url('/signin_as_customer')); ?>" name="customerSignIn" method="POST">
                <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('common.social_login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <input class="mdl-textfield__input" type="text" id="username" name="username">
                  <label class="mdl-textfield__label bold mdl-color-pink central" for="username">Email address or Phone number</label>
                </div> 
                         
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <input class="mdl-textfield__input " type="password" id="password" name="password">
                  <label class="mdl-textfield__label bold mdl-color-pink central" for="password">password</label>
                </div>
                <?php echo $__env->make('common.form_errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <button id="signin" type="submit" class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Sign in</button>      
              </form>
              <div class="top-margin--1em bottom-margin--1em"><a href="<?php echo url('/forgot_password'); ?>" class="mdl-color-text--black bold">Forgot password?</a></div>
              <div class="bottom-margin--1em">
                <span class="top-margin--1em border_circle bold">OR</span>
              </div>

             <div id="logins">
                <button id="social" onclick="checkLoginState()" scope="public_profile,email"  class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-button-color--blue white">
                <i class="fa fa-facebook-official"></i>
                  Sign in with Facebook</button>  
                <button id="google" class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-button-color--red white">
                <i class="fa fa-google-plus-square"></i>
                  Sign in with Google</button>  
              </div> 


              <a href="<?php echo e(url('signin/professional')); ?>" id="signin" class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone wide_link mdl-button-color--white border-pink">
                Service professional? <span class="bold">Sign in here</span>
              </a>  
            </div>
          </main>
        <!-- /Page Content -->

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.login_register', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>