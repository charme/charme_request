       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content settings mdl-card padding-left--5px">
         
        <h5>ACCOUNT</h5>
          <ul class="bold central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
            <li>
              <img class="thumbnails left" src="<?php echo e(URL::asset('icon/lock-48.png')); ?>" style="padding-right: 5px">
              <span><a href="<?php echo e(url('/settings/password')); ?>"class="mdl-color-text--black">Change Password</a></span>
              <div class="clr"></div>
            </li> 

            <li>
              <img class="thumbnails left" src="<?php echo e(URL::asset('icon/signout-48.png')); ?>"  style="padding-right: 5px">
              <span><a href="<?php echo e(url('/logout')); ?>" class="mdl-color-text--black">Sign out</a></span>
              <div class="clr"></div>
            </li> 
          </ul>

        <h5>SOCIAL</h5>
          <ul class="bold central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
            <li>
              <img class="thumbnails left" src="<?php echo e(URL::asset('icon/playstore-48.png')); ?>" style="padding-right: 5px">
              <span> <a href="http://play.google.com" class="mdl-color-text--black">Rate us on Google Play</a></span>
              <div class="clr"></div>
            </li> 

            <li>
              <i class="fa fa-instagram fa-2x left" style="margin-right:5px; "></i>
              <span><a href="http://instagram.com" class="mdl-color-text--black">Follow Charme on Instagram</a></span>
              <div class="clr"></div>
            </li> 
            <li>
              <img class="thumbnails left" src="<?php echo e(URL::asset('icon/twitter-48.png')); ?>"  style="padding-right: 5px">
              <span><a href="http://twitter.com" class="mdl-color-text--black">Follow Charme on Twitter</a></span>
              <div class="clr"></div>
            </li> 
          </ul>

        <h5>SETTINGS</h5>
          <ul class="bold central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
            <li>
              <img class="thumbnails left" src="<?php echo e(URL::asset('icon/no-icon-48.png')); ?>" style="padding-right: 5px">
              <span><a id="terms" href="<?php echo e(URL::asset('Terms_conditions.html')); ?>" class="mdl-color-text--black">Terms & Condition</a></span>
              <div class="clr"></div>
            </li> 

            <li>
              <img class="thumbnails left" src="<?php echo e(URL::asset('icon/no-icon-48.png')); ?>" style="padding-right: 5px">
              <span><a id="privacy" href="<?php echo e(URL::asset('PrivacyPolicy.html')); ?>" class="mdl-color-text--black">Privacy Policy</a></span>
              <div class="clr"></div>
            </li> 
          </ul>

        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>