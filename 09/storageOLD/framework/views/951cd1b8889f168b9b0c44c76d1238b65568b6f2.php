       
       <?php $__env->startSection('content'); ?>
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <form class="width--100" name="my_services" action="<?php echo e(url('professional/services/save')); ?>" method="post">
                <div class="mdl-grid">
                  <!-- Instructions -->
                    <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php if(isset($services)): ?>
                      <?php if(!empty($services['SupplierServices'])): ?>
                        <?php foreach($services['SupplierServices'] as $key => $category): ?>
                          <div class="margin--5px mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                            <div class="faq">
                              <ul>
                                <li class="mdl-card mdl-shadow--2dp text_left">
                                  <a href="#<?php echo e($key); ?>" class="mdl-color-text--black bold">
                                    <i class="fa fa-angle-down fa-2x mdl-color-text--pink cancel"></i>
                                    <?php echo e($key); ?>

                                  </a>   
                                  <div id="<?php echo e($key); ?>" class="bg-grey padding--7px bold">
                                    <?php foreach($category as $service): ?>
                                      <span class="padding--7px border_bottom service_details">
                                        <?php echo e($service['name']); ?>   
                                        <input type="hidden" id="hidden_<?php echo e($service['id']); ?>" name="<?php echo e($service['id']); ?>" value="0" />
                                        <input type="checkbox" value="1" class="right" checked="<?php echo e($service['available']? 'checked': 'off'); ?>" id="<?php echo e($service['id']); ?>" name="<?php echo e($service['id']); ?>"/>
                                      </span>               
                                    <?php endforeach; ?>
                                  </div> 
                                </li>
                              </ul>                             
                            </div>
                          </div>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    <?php endif; ?>
                    <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> 
                      Save Services
                    </button>

                  <!-- /Instructions -->
                </div>
              </form>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.professional_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>