

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="<?php echo e(URL::asset('favicon.ico')); ?>" type="image/x-icon"/>
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/sweetalert.css')); ?>">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/swipebox.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/style.css')); ?>">

    
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/jquery.datetimepicker.css')); ?>"/>


    <script src="<?php echo e(URL::asset('js/jquery-2.1.4.min.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="<?php echo e(URL::asset('js/sweetalert.min.js')); ?>"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="<?php echo e(URL::asset('js/jquery.swipebox.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('js/function.js')); ?>"></script>
    <style type="text/css">
    .sweet-overlay{
          background-color: rgba(0, 0, 0, 0.95) !important;
    }
    </style>
    <!-- New plugins -->
    <script src="<?php echo e(URL::asset('js/jquery.datetimepicker.min.js')); ?>"></script>
    <?php if(url()->current()==url('/gifts/payment')): ?>
    <script type="text/javascript">
       /*Post-gift Options*/      
      $(document).ready(function(){
        $('#post_gift').ready(function(){
         

          <?php if($gift['status']=='ok'): ?>
            var title = "<?php echo e($gift['data']['PaymentResponse']['msg']); ?>";
            var post_gift ="<div class='sa-button-container' style='display:block;'>"+
            '<span class="mdl-color-text--green bold">Gift to <?php echo e($gift['data']['Gift']['giftee_name']); ?> successfully sent</span> '+
            '<a href="<?php echo e(url('/services')); ?>" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> View Services <i class="fa fa-arrow-right"></i></a>'+
            "</div>";
            swal({  title: title,   
                  text: post_gift,   
                  type: "input",
                  imageUrl: '<?php echo e(asset("img/charme_icons/payment-success.png")); ?>',
                  imageSize: '88x88',
                  html: true,
                  showCancelButton: false,
                  allowOutsideClick: false,
                  showConfirmButton: false });  
          <?php endif; ?>

          <?php if($gift['status']=='error'): ?>
            var title = "<?php echo e($gift['error']['msg']); ?>";
            var post_gift ="<div class='sa-button-container' style='display:block;'>"+
            '<div class="mdl-color-text--red bold">Gift Not Sent</div>'+
            '<a href="<?php echo e(url('/gift')); ?>" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Try Again <i class="fa fa-arrow-left"></i></a>'+
            "</div>";

          swal({  title: title,   
                  text: post_gift,   
                  type: "input",
                  imageUrl: '<?php echo e(asset("img/charme_icons/payment-unsuccessful.png")); ?>',
                  imageSize: '88x88',
                  html: true,
                  showCancelButton: false,
                  allowOutsideClick: false,
                  showConfirmButton: false });         
          <?php endif; ?>
        });
      });     
    </script>
    <?php endif; ?>