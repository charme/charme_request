       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content settings mdl-card">
         

          <ul class="bold central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
            <li>
              <img class="thumbnails left" src="<?php echo e(URL::asset('icon/forward_message-48.png')); ?>" style="padding-right: 5px">
              <span><a href="<?php echo e(url('customer_service/message')); ?>" class="mdl-color-text--black"> Send Us a Message</a></span>
              <div class="clr"></div>
            </li> 

            <li>
              <a href="tel://+2349050505047" class="mdl-color-text--black">
                <img class="thumbnails left" src="<?php echo e(URL::asset('icon/end_call-48.png')); ?>"  style="padding-right: 5px">
                <span>Call Us</span>
              </a>
              <div class="clr"></div>
            </li> 
          </ul>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>