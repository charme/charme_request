          					  <?php if(session('error')): ?>
                        <div class="bold mdl-color--red mdl-color-text--white text_center middle border-radius--5px padding--10px">
                         ERROR: <?php echo e(session('error')); ?>

                        </div>
                      <?php endif; ?>

                      <?php if(session('status')): ?>
                        <div class="bold mdl-color--green mdl-color-text--white text_center middle border-radius--5px padding--5px">
                          <?php echo e(session('status')); ?>

                        </div>
                      <?php endif; ?>