       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid service_style">
                <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php foreach($response['data']['Services'] as $service): ?>
                  <div id="<?php echo e($service['id']); ?>" class="pink_hover mdl-card__actions mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone border_round mdl-shadow--2dp">
                      <span class="left mdl-color-text--black"><?php echo e($service['name']); ?></span>
                      <span class="right bold line">‎₦<?php echo e(number_format($service['price']/100)); ?></span>
                </div>
                <?php endforeach; ?>
                
              </div>
            </div>
          </div>
        </main>
        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
    
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>