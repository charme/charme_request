       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <h4>Kindly enter your email address here.</h4>                     
                    <form action="<?php echo url('reset_password'); ?>" name="forgot_password" method="post">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input mdl-cell--12-col" name="email" type="text" id="email">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="email">email address</label>
                      </div>
                      <?php echo $__env->make('common.form_errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                      <input type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" value="Send"/>
                    </form>                     
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.login_register', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>