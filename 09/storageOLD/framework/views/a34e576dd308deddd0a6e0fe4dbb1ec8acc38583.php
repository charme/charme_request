       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content mdl-color--white">
        <section class=""> <!-- first tab -->
          <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-shadow--2dp central">
                      <span class="bold">Total Earnings</span>       
                      <div class="bold bigger top-bottom-margin--04em">₦<?php echo e(number_format($total/100)); ?></div>
                </div>
            <?php if(!empty($month_earnings['data']['EarningsBreakdown'])): ?>
              <?php foreach($month_earnings['data']['EarningsBreakdown'] as $income): ?>
                <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-shadow--2dp bold bg-grey bold"><br>
                  <span class=" mdl-color--pink padding--7px mdl-color-text--white"><?php echo e($income['service_name']); ?></span>
                  <h4 class="left-margin--1em">₦<?php echo e(number_format($income['total_amount']/100)); ?></h4>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>

          </div>
        </section>
      </main>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.professional_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>