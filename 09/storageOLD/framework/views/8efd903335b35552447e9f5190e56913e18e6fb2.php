       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid">
                <?php if(empty($available_professionals)): ?>
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--2-col-phone mdl-shadow--8dp">
                  <a href="#">
                    <img src="<?php echo e(URL::asset('img/user.jpg')); ?>" alt="" />
                    <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                      <span class="bold">No available Professionals</span>
                    </span>                  
                  </a>
                </div>

                <?php else: ?>
                  <?php foreach($available_professionals as $professionals): ?>
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--2-col-phone mdl-shadow--8dp">
                    <a href="<?php echo e(url('/gift/professionals/profile')); ?>/<?php echo e($professionals['id']); ?>">
                      <img src="<?php echo e($professionals['pic_url'] ? $professionals['pic_url'] : URL::asset('img/user.jpg')); ?>" alt="" />
                      <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                        <span class="bold"><?php echo e($professionals['first_name']); ?></span>
                      </span>                  
                    </a>
                  </div>
                  <?php endforeach; ?>
                <?php endif; ?>
              </div>
              

            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>