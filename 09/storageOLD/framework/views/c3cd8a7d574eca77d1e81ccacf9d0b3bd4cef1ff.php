       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">                   

                    <?php if(session('Update_failed')): ?>
                    <div class="mdl-color-text--white bg-red mdl-card">
                       <?php echo e(session('Update_failed')); ?> 
                    </div>
                    <?php endif; ?>
 
                    <h4>Edit Profile Details Below</h4>
                    <form action="<?php echo e(url('professional/profile/edit/dp')); ?>" name="changePic" method="post" enctype="multipart/form-data">
                      <label for="pics"> 
                        <div class="profile_pic">
                          <img src="<?php echo e($data['response']['data']['Supplier']['pic_url'] ? $data['response']['data']['Supplier']['pic_url'] : asset('img/user.jpg')); ?>" alt="" />
                        </div>
                        <input type="file" name="pics" id="pics" class="custom-file-input" />
                      </label>
                      <?php echo $__env->make('common.pic_errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
                      <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  
                        Change Picture
                      </button>
                    </form>
                    <form action="<?php echo url('professional/profile/edit'); ?>" name="editProfile" method="post">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                        <input class="mdl-textfield__input" type="text" id="email" disabled name="email" value="<?php echo e($data['response']['data']['Supplier']['email']); ?>">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="email">email</label>
                      </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <input class="mdl-textfield__input " type="text" id="phone_no" disabled name="phone_no" value="<?php echo e($data['response']['data']['Supplier']['phone_no']); ?>">
                      <label class="mdl-textfield__label bold mdl-color-pink" for="phone_no">Phone Number</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <input class="mdl-textfield__input " type="text" id="first_name" disabled name="first_name" value="<?php echo e($data['response']['data']['Supplier']['first_name']); ?>">
                      <label class="mdl-textfield__label bold mdl-color-pink" for="first_name">First Name</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <input class="mdl-textfield__input " type="text" id="last_name" name="last_name" value="<?php echo e($data['response']['data']['Supplier']['last_name']); ?>">
                      <label class="mdl-textfield__label bold mdl-color-pink" for="last_name">Last Name</label>
                    </div>
                    <?php echo $__env->make('common.form_errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  
                      Update
                    </button>      
                    </form>  
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.professional_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>