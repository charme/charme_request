					  <?php if(session('error')): ?>
                        <div class="mdl-color--red mdl-color-text--white middle border-radius--5px padding--10px">
                         ERROR: <?php echo e(session('error')); ?>

                        </div>
                      <?php endif; ?>

                      <?php if(session('status')): ?>
                        <div class="mdl-color--green mdl-color-text--white  middle border-radius--5px padding--5px">
                          <?php echo e(session('status')); ?>

                        </div>
                      <?php endif; ?>