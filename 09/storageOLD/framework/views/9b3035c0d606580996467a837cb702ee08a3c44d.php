       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid">                 
                <?php echo $__env->make('common.services_errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php foreach($response->data->Categories as $services): ?>
                  <div class="mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--2-col-phone mdl-shadow--2dp">
                    <a href="<?php echo e(url('/services/style')); ?>/<?php echo e($services->id); ?>">
                      <img src="<?php echo e($services->icon_url); ?>" alt="" />
                      <span class="mdl-card__actions mdl-color--pink mdl-color-text--white bold"><?php echo e($services->name); ?></span>                  
                    </a>
                  </div>
                <?php endforeach; ?>                  
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>