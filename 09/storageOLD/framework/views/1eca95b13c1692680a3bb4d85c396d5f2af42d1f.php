<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <title>Charmé</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#E91E63" />
    <link rel="icon" sizes="192x192" href="<?php echo e(URL::asset('charme-highres.png')); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="<?php echo e(URL::asset('favicon.ico')); ?>" type="image/x-icon"/>
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/sweetalert.css')); ?>">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/swipebox.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/style.css')); ?>">

    
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/jquery.datetimepicker.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/nivo_lightbox_themes/default/default.css')); ?>" rel="stylesheet" media="screen">   <!-- Lightbox Styles -->
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/jquery.countdown.css')); ?>" /><!-- Countdown css -->
  </head>
  
    <body>
    <div class="<?php if(!str_contains(url()->current(),'/appointments/chat/') || !str_contains(url()->current(),'/services/location/')): ?>
          animsition
    <?php endif; ?>">
      <div class="mdl-layout mdl-js-layout mdl-layout--overlay-drawer-button">
        <header class="mdl-layout__header mdl-shadow--2dp mdl-layout__header--waterfall mdl-color--white">
          <div class="mdl-layout__header-row">
          <?php if(url()->current()==url('/profile')): ?>
          <span class="mdl-color-text--black bold">Profile</span>
          <?php endif; ?>

          <?php if(url()->current()==url('/profile/edit')): ?>
          <span class="mdl-color-text--black bold">Edit Profile</span>
          <?php endif; ?>

          <?php if(url()->current()==url('/services')): ?>
          <span class="mdl-color-text--black bold">Services</span>
          <?php endif; ?>
          
          <?php if(str_contains(url()->current(),'/services/style/')): ?>
          <span class="mdl-color-text--black bold"><?php echo e($response['category_name']); ?></span>
          <?php endif; ?>

          <?php if(url()->current()==url('/services/professionals') || str_contains(url()->current(),'gift/professionals')): ?>
          <span class="mdl-color-text--black bold">Professionals</span>
          <?php endif; ?>

          <?php if(str_contains(url()->current(),'/services/professionals/profile/')): ?>
          <span class="mdl-color-text--black bold">Service Details</span>
          <?php endif; ?>
          <?php if(url()->current()==url('/inbox')): ?>
          <span class="mdl-color-text--black bold">Inbox</span>
            <?php endif; ?>


          <?php if(url()->current()==url('/invite')): ?>
          <span class="mdl-color-text--black bold">Invite a friend</span>
            <?php endif; ?>

          <?php if(url()->current()==url('/services/location') || url()->current()==url('/profile/location') || url()->current()==url('/gift/location')): ?>
          <span class="mdl-color-text--black bold">Location</span>
            <?php endif; ?>

          <?php if(url()->current()==url('/services/payment')): ?>
          <span class="mdl-color-text--black bold">Payment</span>
            <?php endif; ?>
          <?php if(url()->current()==url('/appointments')): ?>
            <span class="mdl-color-text--black bold">Appointments</span>
          <?php endif; ?>

          <?php if(str_contains(url()->current(),'/appointments/chat/')): ?>
            <span class="mdl-color-text--black bold">Chat with Professional</span>
          <?php endif; ?>
          <?php if(url()->current()==url('/appointments/feedback')): ?>
          <span class="mdl-color-text--black bold">Feedback</span>
            <?php endif; ?>

          <?php if(url()->current()==url('/gift/category')): ?>
            <span class="mdl-color-text--black bold">Select Category</span>
          <?php endif; ?>

          <?php if(url()->current()==url('/gift')): ?>
            <span class="mdl-color-text--black bold">Gift A Service</span>
          <?php endif; ?>

          <?php if(url()->current()==url('/redeem')): ?>
            <span class="mdl-color-text--black bold">Redeem A Gift</span>
          <?php endif; ?>

          <?php if(str_contains(url()->current(),'/gift/service/')): ?>
          <a href="" class="mdl-color-text--black bold"><?php echo e($response['category_name']); ?> Gift</a>
          <?php endif; ?>

          <?php if(url()->current()==url('/customer_service')): ?>
          <span class="mdl-color-text--black bold">Customer Service</span>
            <?php endif; ?>

          <?php if(url()->current()==url('/customer_service/message')): ?>
            <span class="mdl-color-text--black bold">Send Us A Message</span>
          <?php endif; ?>
            
          <?php if(url()->current()==url('/settings')): ?>
            <span class="mdl-color-text--black bold">Settings</span>
          <?php endif; ?>

          <?php if(url()->current()==url('/settings/password')): ?>
            <span class="mdl-color-text--black bold">Change Password</span>
          <?php endif; ?>
            <!-- Spacer -->
            <div class="mdl-layout-spacer"></div>

            <?php if(url()->current()==url('/profile')): ?>
            <a href="<?php echo url('profile/edit'); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdl-color--white mdl-color-text--pink bold">
              Edit Profile
            </a>
            <?php endif; ?>

            <?php if(url()->current()==url('/profile/edit')): ?>
            <a href="<?php echo url('profile'); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdl-color--white mdl-color-text--pink bold">
              View Profile
            </a>
            <?php endif; ?>

            <?php if(url()->current()==url('/services/professionals') || url()->current()==url('/services/professionals/pro')): ?>
            <button id="time_filter" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdl-color--white mdl-color-text--pink bold">
              Filter By Time
            </button>
            <?php endif; ?>
            
          </div>
          <?php if(url()->current()==url('/appointments')): ?>
          <div class="mdl-layout__tab-bar mdl-js-ripple-effect mdl-color-text--black">
            <a href="#scroll-tab-1" class="mdl-layout__tab mdl-color-text--black is-active bold">Upcoming</a>
            <a href="#scroll-tab-2" class="mdl-layout__tab mdl-color-text--black bold">Past</a>
          </div>
          <?php endif; ?>
        </header>
        <!-- Top-right Dropdown Menu -->
        <!-- /Top-right Dropdown Menu -->
        <!-- Sidebar -->
        <div class="mdl-layout__drawer mdl-color--pink">
          <!-- Top -->
          <div class="mdl-card mdl-shadow--2dp mdl-color--pink mdl-color-text--black drawer-profile">
            <div class="mdl-card__title user" id="current_user" data-id="<?php echo e(session('Customer')->id); ?>">
              <a href="<?php echo e(url('/profile')); ?>">

                     
                <?php if(session('Customer')): ?>
                    <img src="<?php echo e((session('Customer')->pic_url ? session('Customer')->pic_url : asset('img/user.jpg') )); ?>" alt="" />
                    <span class="user-name bold">
                      <?php echo e(session('Customer')->first_name); ?> <?php echo e(session('Customer')->last_name); ?>

                    </span>
                <?php endif; ?>
              </a>
            </div>
          </div>
          <!-- /Top -->

          <!-- Main Navigation -->
          <nav class="mdl-navigation bold">
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/services')); ?>">Services</a>
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/appointments')); ?>">Appointments</a>
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/inbox')); ?>">Inbox</a>   
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/invite')); ?>">Invite a friend</a>  
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/gift')); ?>">Gift a Service</a>  
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/redeem')); ?>">Redeem A Gift</a>  
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/customer_service')); ?>">Customer Service</a>  
            <a class="mdl-navigation__link animsition-link white" href="<?php echo e(url('/settings')); ?>">Settings</a>
          </nav>
          <!-- /Main Navigation -->
        </div>
         <?php echo $__env->yieldContent('content'); ?>
      </div>
    </div>
    <div class="live_updates">
      
    </div>
    
    <script src="<?php echo e(URL::asset('js/jquery-2.1.4.min.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="<?php echo e(URL::asset('js/sweetalert.min.js')); ?>"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="<?php echo e(URL::asset('js/jquery.swipebox.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('js/function.js')); ?>"></script>
    <!-- New plugins -->
    <script src="<?php echo e(URL::asset('js/jquery.datetimepicker.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('js/nivo-lightbox.min.js')); ?>"></script>       <!-- Lightbox/Modalbox -->
    <script type="text/javascript" src="<?php echo e(URL::asset('js/validate.min.js')); ?>"></script><!-- Form validator -->
    <script src="<?php echo e(URL::asset('js/moment.js')); ?>"></script><!-- moment.js -->
    <script src="<?php echo e(URL::asset('js/livestamp.min.js')); ?>"></script><!-- livestamp.min.js -->
    <script src="<?php echo e(URL::asset('js/jquery.plugin.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('js/jquery.countdown.min.js')); ?>"></script><!-- Flip Counter -->
    <script src="http://js.pusher.com/3.0/pusher.min.js"></script><!-- Pusher -->
    <script src="<?php echo e(URL::asset('js/pusherfunctions.js')); ?>"></script><!-- Pusher functions -->
    <script src="<?php echo e(URL::asset('js/chatfunctions.js')); ?>"></script><!-- chat functions -->
    <script src="<?php echo e(URL::asset('js/myfunctions.js')); ?>"></script><!-- my functions -->
    <script src="<?php echo e(URL::asset('js/formValidate.js')); ?>"></script><!-- Form Validation -->
    <script type="text/javascript">
      var img="<?php echo e(asset('img/push_image.png')); ?>";
      var chat_url="<?php echo e(url("appointments/chat")); ?>/";
      var chat_img="<?php echo e(asset('img/user.jpg')); ?>";
      var inbox_url="<?php echo e(url('inbox')); ?>";
      var appointment_url="<?php echo e(url("appointments")); ?>/";   
      var send_chat_url="<?php echo e(url('/appointments/chat/send')); ?>";
      var services_professionals_url="<?php echo e(url('/services/professionals')); ?>";
      var filter_service_id = "<?php echo e((Session::get('service_id') ? Session::get('service_id') : "1" )); ?>";
      var feedback_url = "<?php echo e(url('professional/appointments/feedback')); ?>/";
      var time_img="<?php echo e(URL::asset("icon/time-48.png")); ?>";
      var gift_professional_url= "<?php echo e(url('/gift/professionals')); ?>";
      var redeem_gift_url="<?php echo e(url('/redeem/gift')); ?>";
      var appointment_reschedule_url="<?php echo e(url('/appointments/reschedule')); ?>";
      var appointment_cancel_url="<?php echo e(url('appointments/cancel/')); ?>/";
      var coupon_url="<?php echo e(url('/getcoupon')); ?>";
      var interswitch_img="<?php echo e(URL::asset("img/interswitch.png")); ?>";
      var payment_success_img='<?php echo e(asset("img/charme_icons/payment-success.png")); ?>';
      var payment_unsuccessful_img='<?php echo e(asset("img/charme_icons/payment-unsuccessful.png")); ?>';
      var services_request_url="<?php echo e(url('services/request')); ?>";
      var gift_payment_url="<?php echo e(url('gift/makepayment')); ?>";
      var services_details_url="<?php echo e(url('services/details')); ?>";
      var gift_service_img='<?php echo e(asset("img/charme_icons/gift-a-service.png")); ?>';
      var gift_location_url="<?php echo e(url('/gift/location')); ?>";
      var gift_book_url="<?php echo e(url('gift/book')); ?>";
    </script>
    <!-- Map Functions -->
    <?php if(url()->current()==url('/services/location') || url()->current()==url('/gift/location') || url()->current()==url('/profile/location')): ?>
      <script type="text/javascript">
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBQdQFks6cQ7mW_A_A5tdummLJ-9fvVXRE&callback=initMap&region=NG";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
      </script>
      <script>    
          function initMap() {
            var myLatlng= {lat: 6.523276, lng: 3.540791};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 8,
              center: myLatlng
            });
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow;
            /*Geocode address to latlog*/
            document.getElementById('address').addEventListener('change', function() {
              $('#my_location').removeClass('active_location');
              $('#supplier_address').removeClass('active_location');
              geocodeAddress(geocoder, map);
            });
            /*Autocomplete address location*/
            
            /*Use customer's location*/
            $('#my_location').click(function(){
              if ($('#supplier_address').hasClass('active_location')) {
                $('#supplier_address').removeClass('active_location');                
              };
              $('#my_location').toggleClass('active_location');
              geocodeLatLng(geocoder, map, infowindow,this);
            });
            /*Use supplier's location*/
            $('#supplier_address').click(function(){
              if ($('#my_location').hasClass('active_location')) {
                $('#my_location').removeClass('active_location');                
              };
              $('#supplier_address').toggleClass('active_location');
              geocodeLatLng(geocoder, map, infowindow,this);
            });
            /*Click map to get latlog and show address and location*/
            /*google.maps.event.addListener(map,'click', function(event) {
              console.log(event.latlog);
              placeMarker(event.latLng,map);              
            });*/
          }
          /*function placeMarker(location,marker,map) {            
            if (marker == undefined){
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
            else{
                marker.setPosition(location);
            }
            map.setCenter(location);
          }*/

          function geocodeLatLng(geocoder, map, infowindow,nn) {
            var lat =$(nn).attr('data-lat');
            var log =$(nn).attr('data-log');
            var addr=$(nn).attr('data-address');
            $('#address').val(addr);
            $('#address').parent().addClass('is-focused');
            var latlog=lat+','+log;
            //console.log(latlog);
            $('#location').val('('+latlog+')');

            var input = latlog;
            var latlngStr = input.split(',', 2);
            var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
            geocoder.geocode({'location': latlng}, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                  map.setZoom(15);
                  $("#location_found").fadeIn('slow');
                    $("#location_found").css('background-color','green !important');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--green mdl-color-text--black padding--10px line border-radius--5px">'+
                    'location found on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--green border-radius--5px">map</a></p>';
                  var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                  });
                  infowindow.setContent(addr);
                  infowindow.open(map, marker);
                } else {
                  $("#location_found").fadeIn('slow');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
                }
              } else {
                $("#location_found").fadeIn('slow');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
              }
            });
          }

          function geocodeAddress(geocoder, resultsMap) {
                $("#location_found").fadeOut('slow');
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === google.maps.GeocoderStatus.OK) {
                    resultsMap.panTo(results[0].geometry.location);
                    resultsMap.setZoom(15);

                    document.getElementById("location").value = results[0].geometry.location;
                    $("#location_found").fadeIn('slow');
                    $("#location_found").css('background-color','green !important');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--green mdl-color-text--black padding--10px line border-radius--5px">'+
                    'location found on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--green border-radius--5px">map</a></p>';
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
          
                  } 

                  else {

                    $("#location_found").fadeIn('slow');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
                    //alert('We could not find your location on the map: ' + status);
                  }
                    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
                });
          }
      </script>
    <?php endif; ?> 
    <script type="text/javascript">
        
    </script>
  </body>
</html>
    


    
