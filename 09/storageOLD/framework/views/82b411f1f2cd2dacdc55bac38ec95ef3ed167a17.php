       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid">
                <!-- Go Pro! -->
                <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-shadow--8dp mdl-color--pink">
                  <a href="<?php echo url('/services/professionals/pro'); ?>">
                      <span class="bold big mdl-color-text--white">
                        <img class="inherit" src="<?php echo e(URL::asset('icon/charme-pro-48.png')); ?>" alt="" />
                          Go Charme Pro!
                      </span> 
                  </a>                  
                </div>
                <?php if(empty($available_professionals)): ?>
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--2-col-phone mdl-shadow--8dp">
                  <a href="#">
                    <img src="<?php echo e(URL::asset('img/user.jpg')); ?>" alt="" />
                    <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                      <small class="bold">No available Professionals</small>
                    </span>                  
                  </a>
                </div>

                <?php else: ?>
                  <?php foreach($available_professionals as $professionals): ?>
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--2-col-phone mdl-shadow--2dp">
                    <a href="<?php echo e(url('/services/professionals/profile')); ?>/<?php echo e($professionals['id']); ?>">
                      <img src="<?php echo e($professionals['pic_url'] ? $professionals['pic_url'] : URL::asset('img/user.jpg')); ?>" alt="" />
                      <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                        <small class="bold"><?php echo e($professionals['first_name']); ?></small>
                      </span>                  
                    </a>
                  </div>
                  <?php endforeach; ?>
                <?php endif; ?>
              </div>
              

            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>