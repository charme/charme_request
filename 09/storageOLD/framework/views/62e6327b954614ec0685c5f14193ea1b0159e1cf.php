       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="chat bold">
            <ul id="appointment_chat">
            <?php if(!empty($messages['ChatMessages'])): ?>
              <?php foreach($messages['ChatMessages'] as $chat): ?>
                <?php if($chat['sent_by']=='customer'): ?>
                  <li class="message-left" id="<?php echo e($chat['id']); ?>"> <!-- left message -->
                    <img src="<?php echo e(URL::asset('img/user.jpg')); ?>" alt="" class="minilogo mdl-shadow--2dp"> <!-- user -->
                    <div class="message mdl-shadow--2dp"> 
                      <p><?php echo e($chat['message']); ?></p> <!-- message text -->                      
                    </div>
                    <div id="msg_detail" class="text_right">
                      <small data-livestamp="<?php echo e(date_format(date_create($chat['created_at']),'U')); ?>">
                      </small>
                      <small id="sent_status" class="mdl-color-text--green">&#10003;&#10003;</small>
                    </div>
                  </li>
                <?php endif; ?>
                <?php if($chat['sent_by']=='supplier'): ?>
                  <li class="message-right" id="<?php echo e($chat['id']); ?>"> <!-- right message -->
                    <img src="<?php echo e(URL::asset('img/user.jpg')); ?>" alt="" class="minilogo mdl-shadow--2dp"> <!-- user -->
                    <div class="message mdl-shadow--2dp">
                      <p><?php echo e($chat['message']); ?></p> <!-- message text -->
                    </div>
                        <div id="msg_detail" class="text_left">
                          <small data-livestamp="<?php echo e(date_format(date_create($chat['created_at']),'U')); ?>">
                          </small>
                        </div>
                  </li>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
            </ul>
          </div>

          <!-- write message fixed bottom -->
          <div class="send_message">

              <div class="padding--0px mdl-textfield mdl-js-textfield textfield-demo bold">
                <textarea class="mdl-textfield__input" type="text" placeholder="Type message....." id="chat_message" ></textarea>
              </div>
            
            <button id="send_button" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored send">
              <i class="material-icons">send</i>
            </button>
          </div>
          <div id="scrollTo" data-id="<?php echo e($messages['Conversation']['appointment_id']); ?>"></div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>