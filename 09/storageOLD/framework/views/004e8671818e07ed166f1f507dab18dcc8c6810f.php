       
       <?php $__env->startSection('content'); ?>
       <?php 
       function date_app($start_at){
        $app_date=date_create($start_at);
        $today=date_create(date("Y-m-d h:i:s"));
        if ($today==$app_date) {
          echo 'Today';
        }
        if ($today>$app_date) {
          echo " ";
        }

        if ($today<$app_date) {
          $diff=$today->diff($app_date,false);
          $diff= $diff->format("%a");
          if ($diff==0) {
            echo "Today";
          }
          if ($diff !=0) {
            echo "{$diff} days left";
          }
        }
        else echo " ";
       }
       ?>
        <!-- Page Content -->
        <main class="mdl-layout__content mdl-color--white">
        <!-- Upcoming Appointments -->
        <section class="mdl-layout__tab-panel is-active" id="scroll-tab-1"> <!-- first tab -->
          <div class="mdl-grid" id="upcoming_appointments">
            <!-- Notification of ended appointment -->
            <?php if(!empty($data['past']['data']['Appointments'])): ?>
              <?php foreach($data['past']['data']['Appointments'] as $past): ?>
                <?php if($past['Appointment']['supplier_end_at'] && !$past['Appointment']['customer_rating'] && !$past['Appointment']['cancelled']): ?>
                  <a href="<?php echo e(url('/appointments/completed')); ?>/<?php echo e($past['Appointment']['id']); ?>" 
                  class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                    <i class="fa fa-star-o fa-2x mdl-color-text--yellow"></i> Confirm End Of Service
                  </a>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
              <?php echo $__env->make('common.appointment_errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
              <?php if(empty($data['data']['Appointments'])): ?>
                <i class="fa fa-user-times fa-5x"></i><br>
                <h4>You have no upcoming appointments yet.</h4>
              <?php endif; ?>
        <?php if(!empty($data['data']['Appointments'])): ?>
          <?php foreach($data['data']['Appointments'] as $appointment): ?>
            <?php if($appointment['Appointment']['cancelled']!=true): ?>
              <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                <div class="demo-card-event mdl-card mdl-shadow--2dp">
                  <div class="mdl-card__title mdl-card--expand">
                    <div class="bold">
                      <p class="line"><?php echo e($appointment['Service']['name']); ?></p><br>
                      <small class="line color--mid-grey">Professional: <?php echo e($appointment['Supplier']['first_name']); ?></small><br>                    
                      <small class="line mdl-color-text--grey">
                        <i class="fa fa-map-marker mdl-color-text--black"></i>
                        <?php echo e($appointment['MeetupAddress']['address_details']); ?></small>
                    </div>                  
                    <div class="mdl-layout-spacer"></div>
                    <div id="timer" class="cancel">
                      <img class="inherit" id="<?php echo e($appointment['Appointment']['id']); ?>"
                              data-id="<?php echo e($appointment['Appointment']['id']); ?>" 
                              data-start_at="<?php echo e($appointment['Appointment']['start_at']); ?>" 
                             data-time="<?php echo e($appointment['Appointment']['supplier_start_at'] ? $appointment['Appointment']['supplier_start_at'] : 'null'); ?>"
                              src="<?php echo e(URL::asset('icon/time-48.png')); ?>"><br>
                      <small class="bold" data-livestamp="<?php echo e(date_format(date_create($appointment['Appointment']['start_at']),'U')); ?>"></small>
                    </div>
                  </div>
                  <div class="mdl-card__actions mdl-card--border no_padding">
                    <div class="central">
                        <a href="<?php echo e(url('/appointments/chat')); ?>/<?php echo e($appointment['Appointment']['id']); ?>" class="left text_center border_round one-third">
                          <img class="thumbnails" src="<?php echo e(URL::asset('icon/chat-48.png')); ?>">
                        </a>
                        <span <?php if(!$appointment['Appointment']['rescheduled']): ?> onclick="reschedule(this.id)" <?php endif; ?> id="<?php echo e($appointment['Appointment']['id']); ?>" class="text_center one-third cancel">
                          <img class="thumbnails" id="<?php echo e($appointment['Appointment']['id']); ?>" src="<?php echo e(URL::asset('icon/calendar-48.png')); ?>">
                        </span>
                        <span onclick="cancel_appointment(this.id)" id="<?php echo e($appointment['Appointment']['id']); ?>" class="right text_center border_round one-third cancel">
                          <img  class="thumbnails" id="<?php echo e($appointment['Appointment']['id']); ?>" src="<?php echo e(URL::asset('icon/cancel-48.png')); ?>">
                        </span>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endif; ?>

          </div>

        </section>

        <section class="mdl-layout__tab-panel" id="scroll-tab-2"> <!-- tab 2 -->
          
          <div class="mdl-grid">
            
            <!-- Past Appointment -->
            <?php if(empty($data['past']['data']['Appointments'])): ?>
                <i class="fa fa-user-times fa-5x"></i><br>
                <h4>You have no previous appointments.</h4>
              <?php endif; ?>
            <?php if(!empty($data['past']['data']['Appointments'])): ?>
              <?php foreach($data['past']['data']['Appointments'] as $past): ?>
                <?php if($past['Appointment']['customer_id']==session('customer_id')): ?>
                  <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                    <div class="demo-card-event mdl-card mdl-shadow--2dp">
                      <div class="mdl-card__title mdl-card--expand">
                        <div class="bold">
                          <p class="line"><?php echo e($past['Service']['name']); ?></p><br>
                            <small class="line color--mid-grey">Professional: <?php echo e($past['Supplier']['first_name']); ?></small><br>                    
                            <small class="line mdl-color-text--grey">
                              <i class="fa fa-map-marker mdl-color-text--black"></i>
                              <?php echo e($past['MeetupAddress']['address_details']); ?></small>
                        </div>                  
                        <div class="mdl-layout-spacer"></div>
                        <div>
                          <img class="inherit" src="<?php echo e(URL::asset('icon/done.png')); ?>"><br>
                          <span class="bold"><?php echo e(date_format(date_create($past['Appointment']['start_at']),"dS-M-Y")); ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>

          </div>

        </section>
      </main>
      <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>