       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Basic Info & Pic -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <!-- Notification for profile updated successfully -->
                    <?php if(session('profile_updated')): ?>
                    <div class="mdl-color-text--white mdl-color--pink mdl-card">
                       <?php echo e(session('profile_updated')); ?> 
                    </div> 
                    <?php endif; ?>

                    <?php if(isset($data)): ?>
                    <div class="profile_pic">
                      <img src="<?php echo e($data['response']['data']['Customer']['pic_url'] ? $data['response']['data']['Customer']['pic_url'] : asset('img/user.jpg')); ?>" alt="" />
                    </div>                       

                    <h4><?php echo e($data['response']['data']['Customer']['first_name']); ?> <?php echo e($data['response']['data']['Customer']['last_name']); ?> </h4>
                    <h6><?php echo e($data['response']['data']['Customer']['phone_no']); ?></h6>
                    <h6><?php echo e($data['response']['data']['Customer']['email']); ?></h6>
                  
                    
                  </div>
                </div>
                  <!-- /Basic Info & Pic -->

                    <!-- Location Details -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <div class="left">
                            <h4 class="bold"><i class="material-icons mdl-color-text--pink">location_on</i> Locations <a href="<?php echo e(url('profile/location')); ?>"><i class="material-icons mdl-color-text--pink">edit</i></a></h4>
                            <div class="clr border_bottom"></div>                  
                            <ul>
                              <li>
                                <?php if(!empty($data['response']['data']['Customer']['location'])): ?>
                                  <?php echo e($data['response']['data']['Customer']['location']['address_details'] ? $data['response']['data']['Customer']['location']['address_details'] : ''); ?>         
                                <?php endif; ?>
                              </li>
                            </ul>     
                    </div>
                  </div>
                </div>
                  <!-- /Location Details -->
              </div>
            </div>
          </div>
        </main>
          <?php endif; ?>
        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>