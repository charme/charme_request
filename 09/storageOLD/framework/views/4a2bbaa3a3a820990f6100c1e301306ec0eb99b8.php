       
       <?php $__env->startSection('content'); ?>
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid">
                <!-- Go Pro! -->
                <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-shadow--8dp mdl-color--pink">
                  <a href="<?php echo e(url('/services/professionals/')); ?>">
                      <span class="bold big mdl-color-text--white">
                        <img class="inherit" src="<?php echo e(URL::asset('icon/regular-48.png')); ?>" alt="" />
                          Go Regular
                      </span> 
                  </a>                  
                </div>

                <div class="mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--2-col-phone mdl-shadow--8dp">
                    <img src="<?php echo e(URL::asset('img/user.jpg')); ?>" alt="" />
                    <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                      <span class="bold">No Pro's</span>
                    </span>                  
                </div>
                  <a href="<?php echo e(url('/services/professionals/profile')); ?>">
                  </a>

              </div>
              

            </div>
          </div>
        </main>

        <!-- /Page Content -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>