       @extends('layouts.professional_header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">                      
              <div id="holder" class="mdl-grid">
                  <!-- Extra info & address -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <form action="{{url('professional/profile/location/save')}}" name="my_location" method="POST"> 
                      <h4 class="">Address</h4>                     
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="text" id="address" name="address">
                          <input type="text" name="location" id="location" hidden>
                          <label class="mdl-textfield__label bold mdl-color-pink" for="address">(Enter Address Here)</label>
                        </div>
                        @include('common.form_errors')
                      <p id="location_found" class="padding--5px border-radius--5px"></p>
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                        <input class="mdl-textfield__input " type="text" id="address_details" name="address_details">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="address_details">(Please Provide Other Details Describing your Address)</label>
                      </div>            
                      <button type="submit" id="save_location"  class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                        Save Location
                      </button>      
                    </form>                   
                  </div>
                </div>
                  <!-- /Extra info & address -->
                  <!-- Location Details -->
                <div id="map_section" class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div id="map" style="height:380px;" class="mdl-card mdl-shadow--8dp">                    
                  </div>
                </div>
                  <!-- /Location Details -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection
