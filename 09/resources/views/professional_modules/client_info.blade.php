       @extends('layouts.sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    
                    <h4 class="mdl-color-text--pink">Name</h4>
                    <div class="center">Joke Idris</div>

                    <h4 class="mdl-color-text--pink">Location</h4>
                    <div class="center">Yaba, Lagos</div>

                    <h4 class="mdl-color-text--pink">Appointment date</h4>
                    <div class="center">Monday 30th March, 2016</div>

                    <h4 class="mdl-color-text--pink">Additional details</h4>
                    <div class="center">Yaba, Lagos</div>

                    <h4 class="mdl-color-text--pink">Location description</h4>
                    <div class="center">It is a cream coloured building, Ask the gateman for Skylar. It's on the second floor</div>
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection