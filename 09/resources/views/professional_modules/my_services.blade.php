       @extends('layouts.professional_header')
       @section('content')
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <form class="width--100" name="my_services" action="{{url('professional/services/save')}}" method="post">
                <div class="mdl-grid">
                  <!-- Instructions -->
                    @include('common.errors')
                    @if(isset($services))
                      @if(!empty($services['SupplierServices']))
                        @foreach($services['SupplierServices'] as $key => $category)
                          <div class="margin--5px mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                            <div class="faq">
                              <ul>
                                <li class="mdl-card mdl-shadow--2dp text_left">
                                  <a href="#{{$key}}" class="mdl-color-text--black bold">
                                    <i class="fa fa-angle-down fa-2x mdl-color-text--pink cancel"></i>
                                    {{$key}}
                                  </a>   
                                  <div id="{{$key}}" class="bg-grey padding--7px bold">
                                    @foreach($category as $service)
                                      <span class="padding--7px border_bottom service_details">
                                        <small>{{$service['name']}}</small>
                                        <input type="hidden" id="hidden_{{$service['id']}}" name='{{$service['id']}}' value="0" />
                                        <input type="checkbox" value="1" class="right" @if($service['available']=='true'){{$service['available']}} checked="checked" @endif id="{{$service['id']}}" name='{{$service['id']}}'/>
                                      </span>                
                                    @endforeach
                                  </div> 
                                </li>
                              </ul>                             
                            </div>
                          </div>
                        @endforeach
                      @endif
                    @endif
                    <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> 
                      Save Services
                    </button>

                  <!-- /Instructions -->
                </div>
              </form>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection