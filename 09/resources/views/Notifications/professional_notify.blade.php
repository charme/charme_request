       @extends('layouts.login_register')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Invite a friend -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <i class="fa fa-check fa-5x green-text"></i>
                    <h4>Thank you for choosing to use Charmé</h4>  
                    <h5>Your application will be reviewed and we will getback to you shortly.</h5>
                    <a href="<?php echo url('/professional/profile'); ?>" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Continue </a>      
                  </div>
                </div>
                  <!-- /Invite a friend -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection