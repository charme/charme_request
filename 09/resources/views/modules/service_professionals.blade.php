       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp">              
              <div class="mdl-grid">
                <!-- Go Pro! -->
                <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-shadow--2dp mdl-color--pink">
                  <a href="<?php echo url('/services/professionals/pro'); ?>">
                      <span class="bold big mdl-color-text--white">
                        <img class="inherit" src="{{URL::asset('icon/charme-pro-48.png')}}" alt="" />
                          Go Charme Pro!
                      </span> 
                  </a>                  
                </div>
                @if(empty($available_professionals))
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--2-col-phone mdl-shadow--8dp">
                  <a href="#">
                    <img src="{{URL::asset('img/user.jpg')}}" alt="" />
                    <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                      <small class="bold">No available Professionals</small>
                    </span>                  
                  </a>
                </div>

                @else
                  @foreach($available_professionals as $professionals)
                  <div class="mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--2-col-phone mdl-shadow--2dp">
                    <a href="{{url('/services/professionals/profile')}}/{{$professionals['id']}}">
                      <img src="{{$professionals['pic_url'] ? $professionals['pic_url'] : URL::asset('img/user.jpg')}}" alt="" />
                      <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                        <small class="bold">{{$professionals['first_name']}}</small>
                      </span>                  
                    </a>
                  </div>
                  @endforeach
                @endif
              </div>
              

            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection