       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Invite a friend -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                      @if (session('error'))
                        <div class="mdl-color--red mdl-color-text--white middle border-radius--5px padding--5px">
                          {{ session('error') }}
                        </div>
                      @endif

                      @if (session('status'))
                        <div class="mdl-color--green mdl-color-text--white  middle border-radius--5px padding--5px">
                          {{ session('status') }}
                        </div>
                      @endif
                    <div class="gift_service">
                      <img src="{{asset('img/charme_icons/invite-a-friend.png')}}" class="width--100px">
                    </div>
                    <h4>Earn Coupons</h4>  
                    <div class="bold">Send invitations to your friends.</div>
                    <div class="bold">
                      <form action="{{url('invite-friend')}}" id="invite_friend" method="post">

                          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                            <input class="mdl-textfield__input" type="email" id="email" name="email">
                            <label class="mdl-textfield__label" for="email">Friend's Email Address</label>
                          </div>

                          <span class="invite_errors mdl-color-text--red"></span>

                          <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                            Invite Friend
                          </button> 
                      </form>
                    </div>
                  </div>
                </div>
                  <!-- /Invite a friend -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection