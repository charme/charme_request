       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--8dp">
              <div class="mdl-grid">
                    @foreach($response->data->Categories as $services)
                      <div class="mdl-cell mdl-cell--2-col mdl-cell--4-col-tablet mdl-cell--2-col-phone mdl-shadow--2dp" style="display:inline-block;">
                        <a href="{{url('gift/service')}}/{{$services->id}}">
                          <img src="{{$services->icon_url}}" alt="" />
                          <span class="mdl-card__actions mdl-color--pink mdl-color-text--white bold">{{$services->name}}</span>                  
                        </a>
                      </div>
                    @endforeach                           
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection