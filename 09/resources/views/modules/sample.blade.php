       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content mdl-color--white">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <h4>About Me</h4>
              <p>We challenged ourselves to create a visual language for our users that synthesizes the classic principles of good design with the innovation and possibility of technology and science.</p>
              <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                Follow
              </button>
            </div>

            <div class="mdl-card mdl-shadow--2dp">
              <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--4-col">
                  <a class="no-accent-color" href="tel:1234567890"><i class="material-icons">call</i></a>
                  <span>Call</span>
                </div>
                <div class="mdl-cell mdl-cell--4-col">
                  <a class="no-accent-color" href="sms://1234567890"><i class="material-icons">message</i></a>
                  <span>Message</span>
                </div>
                <div class="mdl-cell mdl-cell--4-col">
                  <a><i class="material-icons">favorite</i></a>
                  <span>Like</span>
                </div>
              </div>
            </div>
            <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
              <thead>
                <tr>
                  <th class="mdl-data-table__cell--non-numeric">Things</th>
                  <th>Hour</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="mdl-data-table__cell--non-numeric">Work</td>
                  <td>8 AM</td>
                </tr>
                <tr>
                  <td class="mdl-data-table__cell--non-numeric">Shop</td>
                  <td>4 PM</td>
                </tr>
                <tr>
                  <td class="mdl-data-table__cell--non-numeric">Hobbies</td>
                  <td>6 PM</td>
                </tr>
                <tr>
                  <td class="mdl-data-table__cell--non-numeric">Dinner</td>
                  <td>8 PM</td>
                </tr>
                <tr>
                  <td class="mdl-data-table__cell--non-numeric">Movie</td>
                  <td>10 PM</td>
                </tr>
              </tbody>
            </table>

            <div class="mdl-card mdl-shadow--2dp movie">
                <img src="{{URL::asset('img/9.jpg')}}" alt="">
              <div id="p1" class="mdl-progress mdl-js-progress" style="width:100%;"></div>
              <div class="movie-controller">
                <button class="mdl-button mdl-js-button mdl-button--icon">
                  <i class="material-icons">play_arrow</i>
                </button>
                <span><strong>54:26</strong> / 1:15:30</span>
                <button class="mdl-button mdl-js-button mdl-button--icon">
                  <i class="material-icons">fullscreen</i>
                </button>
                <input class="mdl-slider mdl-js-slider" type="range" min="0" max="100" value="25" tabindex="0"/>
                <div class="clr"></div>
              </div>
            </div>

            <div class="mdl-card mdl-shadow--2dp weather">
              <span>11:57 AM</span>
              <h2>26</h2>
              <button class="mdl-button mdl-js-button mdl-button--icon left">
                  <i class="material-icons">chevron_left</i>
              </button>
              <button class="mdl-button mdl-js-button mdl-button--icon right">
                  <i class="material-icons">chevron_right</i>
              </button>
              <span>Sunny</span>
              <div class="weather-info">
                <h4>Mountain View</h4>
                <span>California</span>
              </div>
            </div>

            <span class="title"><strong>Latest Uploads</strong></span>
            <div class="mdl-card mdl-shadow--2dp latest-images gallery-container">
              <div class="mdl-grid gallery">
                <div class="mdl-cell mdl-cell--4-col">
                  <a href="{{URL::asset('img/5.jpg')}}" class="swipebox" title="Car"><img src="{{URL::asset('img/5.jpg')}}" alt=""></a>
                </div>
                <div class="mdl-cell mdl-cell--4-col">
                  <a href="{{URL::asset('img/2.jpg')}}" class="swipebox" title="Sea"><img src="{{URL::asset('img/2.jpg')}}" alt=""></a>
                </div>
                <div class="mdl-cell mdl-cell--4-col">
                  <a href="{{URL::asset('img/3.jpg')}}" class="swipebox" title="Coffee"><img src="{{URL::asset('img/3.jpg')}}" alt=""></a>
                </div>
              </div>
              <div class="mdl-grid gallery">
                <div class="mdl-cell mdl-cell--12-col">
                  <a href="{{URL::asset('img/6.jpg')}}" class="swipebox" title="Material Design"><img src="{{URL::asset('img/6.jpg')}}" alt=""></a>
                </div>
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection