       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--8dp">
              <div class="mdl-grid">
                {{--session('gift_receiver')['phone']--}}
                    @foreach($response['data']['Services'] as $service)
                  <div id="{{$service['id']}}" onclick="createGift(this.id)" class="pink_hover mdl-card__actions mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone border_round mdl-shadow--2dp">
                      <span class="left mdl-color-text--black">{{$service['name']}}</span>
                      <span class="right bold line">‎₦{{number_format($service['price']/100)}}</span>
                </div>
                @endforeach   
                <div id="gift_results"></div>                  
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection