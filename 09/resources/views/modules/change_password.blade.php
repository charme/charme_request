       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Change Password -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <i class="fa fa-unlock-alt fa-5x mdl-color-text--pink"></i>
                    <h4>Change Password</h4>  
                    <div class="bold">

                      @if (session('error'))
                        <div class="mdl-color--red mdl-color-text--white middle border-radius--5px padding--5px">
                          {{ session('error') }}
                        </div>
                      @endif

                      @if (session('status'))
                        <div class="mdl-color--green mdl-color-text--white  middle border-radius--5px padding--5px">
                          {{ session('status') }}
                        </div>
                      @endif

                      <form action="{{url('settings/change-password')}}" id="password_change" name="password_change" method="post">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="password" id="old_password" name="old_password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                          <label class="mdl-textfield__label" for="old_password">Current Password</label>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="password" id="new_password" name="new_password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                          <label class="mdl-textfield__label" for="new_password">New Password</label>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                          <input class="mdl-textfield__input" type="password" id="confirm_new_password" name="confirm_new_password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                          <label class="mdl-textfield__label" for="confirm_new_password">Confirm Password</label>
                        </div>
                          <span class="password_errors mdl-textfield__error"></span>
                        <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                          Change Password
                        </button>  
                      </form>
                    </div>
                         
                  </div>
                </div>
                  <!-- /Invite a friend -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection