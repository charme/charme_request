       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Basic Info & Pic -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    @include('common.errors')
                    <!-- Notification for profile updated successfully -->
                    @if (session('profile_updated'))
                    <div class="mdl-color-text--white mdl-color--pink mdl-card">
                       {{ session('profile_updated') }} 
                    </div> 
                    @endif

                    @if (isset($data))
                    <div class="profile_pic">
                      <img src="{{$data['response']['data']['Customer']['pic_url'] ? $data['response']['data']['Customer']['pic_url'] : asset('img/user.jpg')}}" alt="" />
                    </div>                       

                    <h4>{{$data['response']['data']['Customer']['first_name']}} {{$data['response']['data']['Customer']['last_name']}} </h4>
                    <h6>{{$data['response']['data']['Customer']['phone_no']}}</h6>
                    <h6>{{$data['response']['data']['Customer']['email']}}</h6>
                  
                    
                  </div>
                </div>
                  <!-- /Basic Info & Pic -->

                    <!-- Location Details -->
                <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <div class="left">
                            <h4 class="bold"><i class="material-icons mdl-color-text--pink">location_on</i> Locations <a href="{{url('profile/location')}}"><i class="material-icons mdl-color-text--pink">edit</i></a></h4>
                            <div class="clr border_bottom"></div>                  
                            <ul>
                              <li>
                                @if(!empty($data['response']['data']['Customer']['location']))
                                  {{$data['response']['data']['Customer']['location']['address_details'] ? $data['response']['data']['Customer']['location']['address_details'] : ''}}         
                                @endif
                              </li>
                            </ul>     
                    </div>
                  </div>
                </div>
                  <!-- /Location Details -->
              </div>
            </div>
          </div>
        </main>
          @endif
        <!-- /Page Content -->
    @endsection