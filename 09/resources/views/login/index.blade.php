       @extends('layouts.landing')
       @section('content')
       
        <!-- Page Content -->
      <div id="bg" style="min-height: 100vh;">
        <main class="mdl-layout__content launch_slider valign-wrapper"> <!-- vertical align container -->
          <div class="valign"> <!-- vertical align element -->
            <h2><img src="img/bigCharme.png" style="width:8em"></h2>
            <p class="mdl-color-text--white">beauty and wellness on demand.</p>
                <p class="top-margin--5em"><a href='<?php echo url('/signin'); ?>'><button class='mdl-button mdl-js-button mdl-button--raised mdl-color--white mdl-color-text--pink mdl-cell--4-col mdl-cell--3-col-tablet mdl-cell--2-col-phone border-pink'>Sign in</button></a>
                <button id="register" class='mdl-button mdl-js-button mdl-button--raised mdl-color--pink mdl-color-text--white mdl-cell--4-col mdl-cell--3-col-tablet mdl-cell--2-col-phone border-white'>New account</button></p> 
          </div>
        </main>
      </div>
        <!-- /Page Content -->
    @endsection