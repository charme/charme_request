
                <div class="mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone margin--auto">
                  <div class="mdl-card mdl-shadow--8dp">
                    <!--  -->
                        <h4 class="mdl-color-text--pink">Make Payment.</h4> 
                        <div class="mdl-grid margin--auto bold border-top border-left border-right border_bottom--pink">
                        	<table style="width:100%; text-align:left;">
							  <tr>
							    <td>Total Cost</td> 
							    <td class="price" id="{{$service_details['price']/100}}">₦<p id="service_amount" class="line">{{number_format($service_details['price']/100)}}</p></td>
							  </tr>
							  <tr id="coupon_value">
							    <td>Coupon Value</td> 
							    <td id="coupon_amount"></td>
							  </tr>
							  <tr>
							  	<td>
								  	<label id="use_coupon" class="question cancel" for="coupon_question">
	                                	<input type="checkbox" onchange="showCoupon()" id="coupon_question" class="">
	                                	<span class="text_left line">I have a Coupon/Gift Card</span>
	                            	</label>
                            	</td>
                            	<td></td>
							  </tr>
							  <tr class="answer">
							  	<td>
							  		<input class="bold mdl-color-pink" onchange="verifyCoupon()"  type="text" id="coupon_field" placeholder="(Enter Coupon Here)" name="coupon_field">
							  	</td>
							  	<td>
							  		<input id="verify" class="cancel line padding--10px border-radius--5px mdl-color-text--white" type="button" onclick="verifyCoupon()" value="Verify">							  		
							  	</td>
							  </tr>
							  <tr id="total_value">
							  	<td>Total:</td>
							  	<td id="total_amount"></td>
							  </tr>
							</table>    
                        	<div id="results"></div>
                        	<div id="test" class="hidden"></div>
                        	<div id="request_response" class="mdl-color-text--red"></div>
							 <button type="button" id="save_details" onclick="saveRequest()"  class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
							 Proceed To Payment <i class="fa fa-arrow-right"></i></button>                       
                        </div>
                  </div>
                </div>