<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <title>Charmé</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#E91E63" />
    <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vegas.min.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('css/colorbox.css')}}" />
    
  </head>
  
    <body>
        <div class="animsition white_bg" >
          <!-- Header -->
          <header class="mdl-layout__header mdl-shadow--2dp mdl-layout__header--waterfall mdl-color--white no-drawer">
            <div class="mdl-layout__header-row">
            @if(str_contains(url()->current(),'appointments/location') && !str_contains(url()->current(),'professional/appointments/location'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> {{$data['Supplier']['first_name']}}'s location
              </a>
            @endif
            </div>
          </header>
           <!-- header background -->        
              @yield('content')
         
    </div>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <!-- New plugins -->
    <script src="{{URL::asset('js/jquery.datetimepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/vegas.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/validate.min.js')}}"></script><!-- Form validator -->
    <script type="text/javascript">
    var lat="{{$data['MeetupAddress']['latitude']}}";
    var lng="{{$data['MeetupAddress']['longitude']}}";
    var address="{{$data['MeetupAddress']['address_details']}}";
    var supplier_name="{{$data['Supplier']['first_name']}}";
    </script>

    @if(str_contains(url()->current(),'appointments/location'))
      <script type="text/javascript">
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBQdQFks6cQ7mW_A_A5tdummLJ-9fvVXRE&callback=initMap";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
      </script>
      <script>
        function initMap() {
            var supplier_latlng;
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 16,
              center: new google.maps.LatLng(lat,lng),
            });
            directionsDisplay.setMap(map);
            supplier_latlng=new google.maps.LatLng(7.3785557,3.9870598);
              //calculateAndDisplayRoute(directionsService, directionsDisplay,supplier_latlng);
            
            /*Appointment Location*/
            var infowindow = new google.maps.InfoWindow;
            var appointment_location = new google.maps.Marker({
              position: new google.maps.LatLng(lat,lng),
              map: map,
              title: '"'+address+'"'
            });
            infowindow.setContent(address);
            infowindow.open(map, appointment_location);
            
            /*Supplier's location*/
            var supplier_info=new google.maps.InfoWindow;
            var supplier_location = new google.maps.Marker({
              position: supplier_latlng,
              map: map,
              animation: google.maps.Animation.BOUNCE,
            });
            supplier_info.setContent(supplier_name);
            supplier_info.open(map, supplier_location);
        }
      </script>
    @endif

  </body>
</html>
