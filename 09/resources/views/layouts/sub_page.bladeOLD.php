<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <title>Charmé</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#E91E63" />
    <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vegas.min.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('css/colorbox.css')}}" />
    
  </head>
  
    <body>
        <div class="animsition white_bg" >
          <!-- Header -->
          <header class="mdl-layout__header mdl-shadow--2dp mdl-layout__header--waterfall mdl-color--white no-drawer">
            <div class="mdl-layout__header-row">
            @if(url()->current()==url('professional/client-info'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> Client information
              </a>
            @endif

            @if(url()->current()==url('professional/client-info/reschedule'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> Appointment Reschedule
              </a>
            @endif

            @if(url()->current()==url('/register/professional/more'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> A few more details
              </a>
            @endif

            @if(str_contains(url()->current(),'professional/appointments/location'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> {{$data['Customer']['first_name']}}'s location
              </a>
            @endif
            </div>
          </header>
           <!-- header background -->        
              @yield('content')
         
    </div>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <!-- New plugins -->
    <script src="{{URL::asset('js/jquery.datetimepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/vegas.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/validate.min.js')}}"></script><!-- Form validator -->
    <script type="text/javascript">
    var lat="{{$data['MeetupAddress']['latitude']}}";
    var lng="{{$data['MeetupAddress']['longitude']}}";
    var address="{{$data['MeetupAddress']['address_details']}}";
    var supplier_name="{{$data['Supplier']['first_name']}}";
    </script>

    @if(str_contains(url()->current(),'appointments/location'))
      <script type="text/javascript">
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBQdQFks6cQ7mW_A_A5tdummLJ-9fvVXRE&callback=initMap";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
      </script>
      <script>
        function initMap() {
            var my_latlng;
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 16,
              center: new google.maps.LatLng(lat,lng),
            });
            var infowindow = new google.maps.InfoWindow;
            var appointment_location = new google.maps.Marker({
              position: new google.maps.LatLng(lat,lng),
              map: map,
              animation: google.maps.Animation.BOUNCE,
              title: '"'+address+'"'
            });
            infowindow.setContent(address);
            infowindow.open(map, appointment_location);
            my_latlng=new google.maps.LatLng(lat,lng);
            calculateAndDisplayRoute(directionsService, directionsDisplay,my_latlng);

              if(navigator.geolocation) {
                browserSupportFlag = true;
                navigator.geolocation.getCurrentPosition(function(position) {
                  current_latlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                  //calculateAndDisplayRoute(directionsService, directionsDisplay,current_latlng);
                }, function() {
                  handleNoGeolocation(browserSupportFlag);
                });
              }
              // Browser doesn't support Geolocation
              else {
                browserSupportFlag = false;
                handleNoGeolocation(browserSupportFlag);
              }
              function handleNoGeolocation(errorFlag) {
                if (errorFlag == true) {
                  alert("Geolocation service failed.");
                } else {
                  alert("Your browser doesn't support geolocation.");
                }
              }

            directionsDisplay.setMap(map);
        }

        /*function calculateAndDisplayRoute(directionsService, directionsDisplay,my_latlng) {
          directionsService.route({
            origin: my_latlng,
            destination: new google.maps.LatLng(lat,lng),
            travelMode: google.maps.TravelMode.DRIVING
          }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
              directionsDisplay.setDirections(response);
            } else {
              window.alert('Directions request failed due to ' + status);
            }
          });
        }*/
      </script>
    @endif

    @if(url()->current()==url('/register/professional/more'))
      <script>
      /*Phone verification form*/
        $('input:checkbox').simpleCheckbox();
        var validate_more_form= new FormValidator('moreDetails',
          [
            {
              name: 'description',
              display: 'About Me',
              rules: 'required|max_length[140]'
            }            
          ],
          function(errors,event){
            if (errors.length>0) {
              console.log(errors);
              $('.form_errors').html(errors[0].message);
              errors[0].element.focus();
              return false;
            };
            if (errors.length<=0) {
              $('.form_errors').fadeOut('slow');              
            };

          });
        validate_more_form.setMessage('required', 'Please tell us about yourself');
      $(document).ready(function(){     
        $("#cat > a").click(function(){
          var id= $(this).attr('id');
        });

        var new_cat= '<div class="dropdown mdl-cell--12-col text_left">'
                              +'<select id="select_category" onchange="loadService(this.value,$(this).text())" class="mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone select" name="service_category">'
                                +'<option id="null" value="null">Select service category</option>'
                                +'@foreach($categories as $category)'
                                 +'<option id="{{$category["id"]}}" value="{{$category["id"]}}">{{$category["name"]}}</option>'
                                +'@endforeach'
                              +'</select>';
        $('#new_service').click(function(){
          $("#cat_list").append(new_cat);
        });

      });
      function loadService(clicked_id,clicked_name){
          //var id = this;
          if (clicked_id !== 'null') {
            var url = '{{url("/")}}';
            url +='/register/add_new_services/'+clicked_id;
            parent.$("#services_select").fadeIn('slow');
            $('html, body').animate({scrollTop: $("#services_select").offset().top}, 'slow');
             var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
              if (xhttp.readyState == 4 && xhttp.status == 200) {
              document.getElementById("services_select").innerHTML = xhttp.responseText;
              }
              if (xhttp.readyState == 4 && xhttp.status == 500) {
              document.getElementById("services_select").innerHTML = '<i class="width--100 fa fa-times-circle fa-5x mdl-color-text--red bold central"></i><br><span class="mdl-color-text--black text_center bold">Service Loading Failed (Network Error)</span>';
              }
               if (xhttp.readyState !== 4) {
               document.getElementById("services_select").innerHTML = '<i class="width--100 fa fa-spinner fa-5x fa-spin mdl-color-text--pink bold central"></i><br><span class="mdl-color-text--pink text_center bold">Loading Service List</span>';
               }
              };
              xhttp.open("GET", url, true);
              xhttp.send();
            };
        }
      function SaveCat() {
                                  var val = [];
                                  var id = [];
                                  $(':checkbox:checked').each(function(i){
                                  var url = null;
                                  var url = '{{url("/register/save_services/")}}';
                                   url += '/'+$(this).attr('id');                                  
                                  $.get(url);

                                       var xhttp = new XMLHttpRequest();
                                      xhttp.onreadystatechange = function() {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                         document.getElementById("demo").innerHTML = '<span class="mdl-color--green mdl-color-text--white">Service(s) Successfully Added</span>';
                                        }

                                        if (xhttp.readyState == 4 && xhttp.status == 500) {
                                         document.getElementById("demo").innerHTML = 'Service(s) were not added';
                                        }

                                        if (xhttp.readyState !== 4) {
                                         document.getElementById("demo").innerHTML = '<i class="fa fa-spinner fa-2x fa-spin mdl-color-text--pink bold central"></i>Adding Service';
                                        }
                                      };
                                      xhttp.open("GET", url, true);
                                      xhttp.send();

                                  });
                                  var un_val =[];
                                  var un_id=[];
                                  $(":checkbox:not(:checked)").each(function(i){
                                    var url = null;
                                  var url = '{{url("/register/delete_services/")}}';
                                  url += '/'+$(this).attr('id');
                                    var xhttp = new XMLHttpRequest();
                                      xhttp.onreadystatechange = function() {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                         document.getElementById("demo").innerHTML = '<span class="mdl-color--green mdl-color-text--white">Service(s) Successfully Added</span>';
                                         //parent.$("#services_select").fadeIn('slow');
                                        }

                                        if (xhttp.readyState == 4 && xhttp.status == 500) {
                                         document.getElementById("demo").innerHTML = 'Service(s) were not added';
                                        }

                                        if (xhttp.readyState !== 4) {
                                         document.getElementById("demo").innerHTML = '<i class="fa fa-spinner fa-2x fa-spin mdl-color-text--pink bold central"></i>Adding Service';
                                        }
                                      };
                                      xhttp.open("GET", url, true);
                                      xhttp.send();
                                  });                                    
      }
      </script>
    @endif

  </body>
</html>
