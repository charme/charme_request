<!DOCTYPE html>
<html class="no-js">
    <head>
      <meta charset="utf-8">
      <title>Charmé</title>
      <meta name="description" content="">
      <meta name="HandheldFriendly" content="True">
      <meta name="MobileOptimized" content="320">
      <meta name="theme-color" content="#E91E63" />
      <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
      <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
      <!-- Stylesheets -->
      <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
      <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
      <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
      <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">

      
      <!-- /Extra CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="{{ URL::asset('css/nivo_lightbox_themes/default/default.css')}}" rel="stylesheet" media="screen">   <!-- Lightbox Styles -->
      <link rel="stylesheet" href="{{URL::asset('css/jquery.countdown.css')}}" /><!-- Countdown css -->
      <link rel="stylesheet" href="{{URL::asset('css/bootstrap-material-datetimepicker.css')}}" />
    </head>
  
    <body class="@if(str_contains(url()->current(),'appointments/chat/')) white_bg @endif">
        <div class="animsition white_bg" >
          <!-- Header -->
          <header class="mdl-layout__header mdl-shadow--2dp mdl-layout__header--waterfall mdl-color--white no-drawer">
            <div class="mdl-layout__header-row">

            <!-- Appointment Chat -->
            @if(str_contains(url()->current(),'/appointments/chat/'))
              <a href="{{url('appointments')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Chat with professional
              </a>
            @endif
            @if(str_contains(url()->current(),'appointments/location') && !str_contains(url()->current(),'professional/appointments/location'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> {{$data['Supplier']['first_name']}}'s location
              </a>
            @endif
            @if(str_contains(url()->current(),'/services/style'))
              <a href="{{url('services')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> {{$response['category_name']}}
              </a>
              <span class="mdl-color-text--black bold"></span>
            @endif 

            @if(url()->current()==url('/services/professionals'))
            <a href="{{url('/services/style')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Professionals
              </a>
            @endif

            @if(url()->current()==url('/services/professionals/pro'))
            <a href="{{url('services/professionals')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Pro Professionals
            </a>
            @endif

            @if(str_contains(url()->current(),'/services/professionals/profile'))
            <a href="{{url('services/professionals')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Service Details
            </a>
            @endif

            @if(url()->current()==url('/services/location') || url()->current()==url('/profile/location'))
              <a href="{{url('services/professionals/profile')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Location
              </a>
            @endif


            <!-- gift a service -->
            @if(url()->current()==url('/gift/category'))
              <a href="{{url('gift')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Select Gift Category
              </a>
            @endif

            @if(str_contains(url()->current(),'/gift/service/'))
              <a href="{{url('gift/category')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> {{$response['category_name']}} Gift
              </a>
            @endif
            <!-- redeem a gift -->
            @if(str_contains(url()->current(),'gift/professionals'))
            <a href="{{url('redeem')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Professionals
            </a>
            @endif
            @if(str_contains(url()->current(),'/gift/selected-pro/profile'))
              <a href="{{url('gift/professionals')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Service Details
              </a>
            @endif
            @if(url()->current()==url('/gift/location'))
              <a href="{{url('gift/selected-pro/profile')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Location
              </a>
            @endif
            <!-- Customer Service -->
            @if(str_contains(url()->current(),'/customer_service/message'))
              <a href="{{url('customer_service')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Send Us A Message
              </a>
            @endif
            <!-- Change Password -->
            @if(url()->current()==url('/settings/password'))
            <a href="{{url('settings')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Change Password
              </a>
            @endif
            <!-- Spacer -->
            <div class="mdl-layout-spacer"></div>
            @if(url()->current()==url('/services/professionals') || url()->current()==url('/services/professionals/pro'))
            <button id="time_filter" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdl-color--pink mdl-color-text--white bold">
              Filter By Time
            </button>
            @endif
            <!-- Chat -->
            @if(str_contains(url()->current(),'appointments/chat/'))
              <a href="{{url('appointments/location')}}/{{session('appointment_id')}}" class="mdl-color-text--pink mdl-color--white padding--5px border-radius--5px bold">                
                <i class="fa fa-map-marker fa-2x mdl-color-text--pink"></i>
              </a>              
            @endif
            </div>
          </header>
           <!-- header background -->        
              @yield('content')
         
    </div>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <script src="{{URL::asset('js/nivo-lightbox.min.js')}}"></script>       <!-- Lightbox/Modalbox -->
    <script type="text/javascript" src="{{URL::asset('js/validate.min.js')}}"></script><!-- Form validator -->
    <script src="{{URL::asset('js/moment.js')}}"></script><!-- moment.js -->
    <script src="{{URL::asset('js/livestamp.min.js')}}"></script><!-- livestamp.min.js -->
    <script src="{{URL::asset('js/jquery.plugin.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.countdown.min.js')}}"></script><!-- Flip Counter -->
    <script src="http://js.pusher.com/3.0/pusher.min.js"></script><!-- Pusher -->
    <script src="{{URL::asset('js/pusherfunctions.js')}}"></script><!-- Pusher functions -->
    <script src="{{URL::asset('js/chatfunctions.js')}}"></script><!-- chat functions -->
    <script src="{{URL::asset('js/myfunctions.js')}}"></script><!-- my functions -->
    <script src="{{URL::asset('js/formValidate.js')}}"></script><!-- Form Validation -->
    <script type="text/javascript" src="{{URL::asset('js/bootstrap-material-datetimepicker.js')}}"></script>
    <script type="text/javascript">
      var img="{{asset('img/push_image.png')}}";
      var chat_url="{{url("appointments/chat")}}/";
      var chat_img="{{asset('img/user.jpg')}}";
      var inbox_url="{{url('inbox')}}";
      var appointment_url="{{url("appointments")}}/";  
      var my_appointments_url="{{url("appointments")}}"; 
      var send_chat_url="{{url('/appointments/chat/send')}}";
      var services_professionals_url="{{url('/services/professionals')}}";
      var filter_service_id = "{{ (Session::get('service_id') ? Session::get('service_id') : "1" )}}";
      var feedback_url = "{{url('customer_service/message')}}";
      var time_img="{{URL::asset("icon/time-48.png")}}";
      var gift_professional_url= "{{url('/gift/professionals')}}";
      var redeem_gift_url="{{url('/redeem/gift')}}";
      var appointment_reschedule_url="{{url('/appointments/reschedule')}}";
      var appointment_cancel_url="{{url('appointments/cancel/')}}/";
      var appointment_complete_url="{{url('appointments/completed')}}/";
      var coupon_url="{{url('/getcoupon')}}";
      var interswitch_img="{{URL::asset("img/interswitch.png")}}";
      var payment_success_img='{{asset("img/charme_icons/payment-success.png")}}';
      var payment_unsuccessful_img='{{asset("img/charme_icons/payment-unsuccessful.png")}}';
      var services_request_url="{{url('services/request')}}";
      var gift_payment_url="{{url('gift/makepayment')}}";
      var services_details_url="{{url('services/details')}}";
      var gift_service_img='{{asset("img/charme_icons/gift-a-service.png")}}';
      var gift_location_url="{{url('/gift/location')}}";
      var gift_book_url="{{url('gift/book')}}";
    </script>
    <!-- Map Functions -->
    @if(url()->current()==url('/services/location') || url()->current()==url('/gift/location') || url()->current()==url('/profile/location'))
      <script type="text/javascript">
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBQdQFks6cQ7mW_A_A5tdummLJ-9fvVXRE&callback=initMap&region=NG";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
      </script>
      <script>    
          function initMap() {
            var myLatlng= {lat: 6.523276, lng: 3.540791};
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 8,
              center: myLatlng
            });
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow;
            /*Geocode address to latlog*/
            document.getElementById('address').addEventListener('change', function() {
              $('#my_location').removeClass('active_location');
              $('#supplier_address').removeClass('active_location');
              geocodeAddress(geocoder, map);
            });
            /*Autocomplete address location*/
            
            /*Use customer's location*/
            $('#my_location').click(function(){
              if ($('#supplier_address').hasClass('active_location')) {
                $('#supplier_address').removeClass('active_location');                
              };
              $('#my_location').toggleClass('active_location');
              geocodeLatLng(geocoder, map, infowindow,this);
            });
            /*Use supplier's location*/
            $('#supplier_address').click(function(){
              if ($('#my_location').hasClass('active_location')) {
                $('#my_location').removeClass('active_location');                
              };
              $('#supplier_address').toggleClass('active_location');
              geocodeLatLng(geocoder, map, infowindow,this);
            });
            /*Click map to get latlog and show address and location*/
            /*google.maps.event.addListener(map,'click', function(event) {
              console.log(event.latlog);
              placeMarker(event.latLng,map);              
            });*/
          }
          /*function placeMarker(location,marker,map) {            
            if (marker == undefined){
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
            else{
                marker.setPosition(location);
            }
            map.setCenter(location);
          }*/

          function geocodeLatLng(geocoder, map, infowindow,nn) {
            var lat =$(nn).attr('data-lat');
            var log =$(nn).attr('data-log');
            var addr=$(nn).attr('data-address');
            $('#address').val(addr);
            $('#address').parent().addClass('is-focused');
            var latlog=lat+','+log;
            //console.log(latlog);
            $('#location').val('('+latlog+')');

            var input = latlog;
            var latlngStr = input.split(',', 2);
            var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
            geocoder.geocode({'location': latlng}, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                  map.setZoom(15);
                  $("#location_found").fadeIn('slow');
                    $("#location_found").css('background-color','green !important');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--green mdl-color-text--black padding--10px line border-radius--5px">'+
                    'location found on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--green border-radius--5px">map</a></p>';
                  var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    animation: google.maps.Animation.BOUNCE,
                  });
                  infowindow.setContent(addr);
                  infowindow.open(map, marker);
                } else {
                  $("#location_found").fadeIn('slow');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
                }
              } else {
                $("#location_found").fadeIn('slow');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
              }
            });
          }

          function geocodeAddress(geocoder, resultsMap) {
                $("#location_found").fadeOut('slow');
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === google.maps.GeocoderStatus.OK) {
                    resultsMap.panTo(results[0].geometry.location);
                    resultsMap.setZoom(15);

                    document.getElementById("location").value = results[0].geometry.location;
                    $("#location_found").fadeIn('slow');
                    $("#location_found").css('background-color','green !important');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--green mdl-color-text--black padding--10px line border-radius--5px">'+
                    'location found on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--green border-radius--5px">map</a></p>';
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
          
                  } 

                  else {

                    $("#location_found").fadeIn('slow');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
                    //alert('We could not find your location on the map: ' + status);
                  }
                    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
                });
          }
      </script>
    @endif 

    @if(str_contains(url()->current(),'appointments/location'))
      <script type="text/javascript">
      var lat="{{$data['MeetupAddress']['latitude']}}";
      var lng="{{$data['MeetupAddress']['longitude']}}";
      var address="{{$data['MeetupAddress']['address_details']}}";
      var supplier_name="{{$data['Supplier']['first_name']}}";
      </script>
      <script type="text/javascript">
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyBQdQFks6cQ7mW_A_A5tdummLJ-9fvVXRE&callback=initMap";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
      </script>
      <script>
        function initMap() {
            var supplier_latlng;
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 16,
              center: new google.maps.LatLng(lat,lng),
            });
            directionsDisplay.setMap(map);
            supplier_latlng=new google.maps.LatLng(7.3785557,3.9870598);
              //calculateAndDisplayRoute(directionsService, directionsDisplay,supplier_latlng);
            
            /*Appointment Location*/
            var infowindow = new google.maps.InfoWindow;
            var appointment_location = new google.maps.Marker({
              position: new google.maps.LatLng(lat,lng),
              map: map,
              title: '"'+address+'"'
            });
            infowindow.setContent(address);
            infowindow.open(map, appointment_location);
            
            /*Supplier's location*/
            var supplier_info=new google.maps.InfoWindow;
            var supplier_location = new google.maps.Marker({
              position: supplier_latlng,
              map: map,
              animation: google.maps.Animation.BOUNCE,
            });
            supplier_info.setContent(supplier_name);
            supplier_info.open(map, supplier_location);
        }
      </script>
    @endif

  </body>
</html>
