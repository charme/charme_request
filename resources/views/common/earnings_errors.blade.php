          					  @if (session('error'))
                        <div class="mdl-color--red mdl-color-text--white middle border-radius--5px padding--10px bold text_center">
                         ERROR: {{ session('error') }}
                        </div>
                      @endif

                      @if (session('status'))
                        <div class="mdl-color--green mdl-color-text--white  middle border-radius--5px padding--5px bold text_center">
                          {{ session('status') }}
                        </div>
                      @endif