	
	
	@if (session('signin_error'))
	<div class="mdl-color-text--white bg-red mdl-card">
		ERROR: {{ session('signin_error') }} 
	</div>
	@endif

	@if(isset($data['response']['error']['msg']))
	<div class="mdl-color-text--white bg-red mdl-card">
		ERROR: {{$data['response']['error']['msg']}}
	</div>
	@endif

	@if(isset($data['response']['data']['Customer']['verified']))
	<div class="mdl-color-text--white bg-red mdl-card">
		Kindly verify your account.
	</div>
	@endif
	