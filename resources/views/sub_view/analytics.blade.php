<!-- Google Analytics -->
<script>
    window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
    ga('create', 'UA-77407392-1', 'auto', {autoLinker: true});
    ga('send', 'pageview');

    //load Linker plugin
    ga('require', 'linker');
    ga('linker:autoLink', ['www.charmeapp.com']);
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->