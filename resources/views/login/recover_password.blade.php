       @extends('layouts.login_register')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    @include('common.errors')
                    <h4>Recover Account</h4>                     
                    <form action="{{url('remember_password')}}" name="resetPassword" method="post">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label width--100">
                        <input class="mdl-textfield__input mdl-cell--12-col" name="password" type="password" id="password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                        <label class="mdl-textfield__label bold mdl-color-pink" for="password">Password</label>
                      </div>

                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label width--100">
                        <input class="mdl-textfield__input mdl-cell--12-col" name="confirm_password" type="password" id="confirm_password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                        <label class="mdl-textfield__label bold mdl-color-pink" for="confirm_password">Confirm Password</label>
                        <input type="text" hidden class="hidden" name="reset_token" value="{{$token}}">
                      </div>
                      @include('common.password_errors')
                      <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                        Submit
                      </button>
                    </form>                     
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection