       @extends('layouts.login_register')
       @section('content')
    <!-- Page Content -->
        
          <main class="contact-about white_bg">
            <div class="mdl-color-white mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
              <form action="{{url('register/customer')}}" name="registerCustomer" method="post">
                <div class="mdl-cell--8-col central mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  @include('common.register_errors')
                  @include('common.social_alerts')
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                    <input class="mdl-textfield__input" type="text" id="first_name" name="first_name">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="first_name">First Name</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                    <input class="mdl-textfield__input" type="text" id="last_name" name="last_name">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="last_name">Last Name</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone text_left">
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="male">
                      <input type="radio" id="male" class="mdl-radio__button" name="gender" value="male">
                      <span class="mdl-radio__label"><i class="fa fa-male fa-2x mdl-color-text--pink"></i></span>
                    </label>
                  </div>

                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone text_left">
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="female">
                      <input type="radio" id="female" class="mdl-radio__button" name="gender" value="female">
                      <span class="mdl-radio__label"><i class="fa fa-female fa-2x mdl-color-text--pink"></i></span>
                    </label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input" type="text" id="email" name="email">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="email">Email address</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input" type="text" id="phone_number" name="phone_no">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="phone_number">Phone Number</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input " type="password" id="password" name="password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                    <label class="mdl-textfield__label bold mdl-color-pink" for="password">Password</label>
                  </div>
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input " type="password" id="confirm_password" name="confirm_password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                    <label class="mdl-textfield__label bold mdl-color-pink" for="confirm_password">Confirm Password</label>
                  </div>
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input" type="text" id="reference_code" value="{{$reference}}" name="reference_code">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="reference_code">Reference Code</label>
                  </div> 
                </div>
                  <input type="text" name="fb_id" id="fb_id" hidden>
                  <input type="text" name="g_token" id="g_token" hidden>
                  <input type="text" name="pic_url" id="pic_url" hidden>
                  <ul class="bold central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-color-text--grey">
                      <a href="{{url("/signin")}}" class="mdl-color-text--grey">Have an account? Sign  in</a>                   
                    <div class="clr"></div>
                  </li> 
                </ul>
                @include('common.form_errors')
                <button type="submit" class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Create Your Account</button>      
              </form>
              <div class="bottom-margin--1em top-margin--1em ">
                <span class="top-margin--3em border_circle bold">OR</span>
              </div>
              
                <div id="status">
              </div>
              

              <div id="logins">
                <button id="social" onclick="checkLoginState()" scope="public_profile,email"  class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-button-color--blue white">
                <i class="fa fa-facebook-official"></i>
                  Sign up with Facebook</button>  
                <button id="google" class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-button-color--red white">
                <i class="fa fa-google-plus-square"></i>
                  Sign up with Google</button>  
              </div>
            </div>
          </main>
        <!-- /Page Content -->

    @endsection