       @extends('layouts.sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    <h4 class="mdl-color-text--pink">Name</h4>                                         
                    <div class="bold">
                      <p>{{$appointment['Customer']['first_name'] ? $appointment['Customer']['first_name'] : " "}} 
                      {{$appointment['Customer']['last_name'] ? $appointment['Customer']['last_name'] : " "}}</p>
                    </div> 
                    <!-- location -->                            
                    <h4 class="mdl-color-text--pink">Location</h4>                                         
                    <div class="bold">
                      <p>{{$appointment['MeetupAddress']['address_details']? $appointment['MeetupAddress']['address_details'] : " "}}</p>
                    </div>
                    <!-- Service Type -->
                    <h4 class="mdl-color-text--pink">Service Type</h4>                                         
                    <div class="bold">
                      <p>{{$appointment['Service']['name']  ? $appointment['Service']['name'] : " "}}</p>
                    </div>
                    <!-- Appointment date -->
                    <h4 class="mdl-color-text--pink">Appointment date</h4>                                         
                    <div class="bold">
                      <p>{{$appointment['Appointment']['start_at']  ? date_format(date_create($appointment['Appointment']['start_at']),"jS M, g:i A") : " "}}</p>
                    </div>
                    <!-- Addiyional details -->
                    <h4 class="mdl-color-text--pink">Additional details</h4>                                         
                    <div class="bold">
                      <p>{{$appointment['Appointment']['additional_message'] ? $appointment['Appointment']['additional_message'] : " "}}</p>
                    </div>

                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection