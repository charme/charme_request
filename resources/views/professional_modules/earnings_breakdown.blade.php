       @extends('layouts.sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content mdl-color--white" style="margin-top:4em;">
        <section class=""> <!-- first tab -->
          <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-shadow--2dp central">
                      <span class="bold">Total Earnings</span>       
                      <div class="bold bigger top-bottom-margin--04em">₦{{number_format($total/100)}}</div>
                </div>
            @if(!empty($month_earnings['data']['EarningsBreakdown']))
              @foreach($month_earnings['data']['EarningsBreakdown'] as $income)
                <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-shadow--2dp bold bg-grey bold"><br>
                  <span class=" mdl-color--pink padding--7px mdl-color-text--white">{{$income['service_name']}}</span>
                  <h4 class="left-margin--1em">₦{{number_format($income['total_amount']/100)}}</h4>
                </div>
              @endforeach
            @endif

          </div>
        </section>
      </main>
    @endsection