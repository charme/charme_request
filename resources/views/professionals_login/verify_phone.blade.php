       @extends('layouts.login_register')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    @include('common.errors')
                    <h4>
                      Verify Phone
                    </h4>                     
                    <form action="{{url('register/professional/verify-phone')}}" name="verifyPhone" method="post">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input mdl-cell--12-col" name="code" type="text" id="code">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="code">Please enter code here</label>
                      </div>
                      @include('common.form_errors')
                      <input type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" value="Send"/>
                    </form>                     
                      <a href="{{url('professional/resend-sms')}}" class="font--14px mdl-color--black mdl-color-text--white border-radius--5px padding--7px">Resend SMS</a>
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>
        <!-- /Page Content -->
    @endsection