       @extends('layouts.professionals_login_register')
       @section('content')
    <!-- Page Content -->
        
          <main class="contact-about white_bg">
            <div class="mdl-color-white mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
              <form action="{{url('register/professional')}}" name="registerProfessional" method="post">
                <div class="mdl-cell--8-col central mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  @include('common.register_errors')
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                    <input class="mdl-textfield__input" type="text" id="first_name" value="{{ old('first_name') }}" name="first_name">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="first_name">First Name</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                    <input class="mdl-textfield__input" type="text" id="last_name" value="{{ old('last_name') }}" name="last_name">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="last_name">Last Name</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone text_left">
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="male">
                      <input type="radio" id="male" class="mdl-radio__button" name="gender" value="male">
                      <span class="mdl-radio__label"><i class="fa fa-male fa-2x mdl-color-text--pink"></i></span>
                    </label>
                  </div>
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone text_left">
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="female">
                      <input type="radio" id="female" class="mdl-radio__button" name="gender" value="female">
                      <span class="mdl-radio__label"><i class="fa fa-female fa-2x mdl-color-text--pink"></i></span>
                    </label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input" type="text" id="email" value="{{ old('email') }}" name="email">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="email">Email address</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input" type="text" id="phone_number" value="{{ old('phone_no') }}" name="phone_no">
                    <label class="mdl-textfield__label bold mdl-color-pink" for="phone_number">Phone Number</label>
                  </div> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input " type="password" id="password" name="password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                    <label class="mdl-textfield__label bold mdl-color-pink" for="password">Password</label>
                  </div>
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <input class="mdl-textfield__input " type="password" id="confirm_password" name="confirm_password">
                          <div class="hideShowPassword-toggle cancel" onclick="showPassword(this)"><i class="fa fa-eye"></i></div>
                    <label class="mdl-textfield__label bold mdl-color-pink" for="confirm_password">Confirm Password</label>
                  </div>
                </div>
                <ul class="bold central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-color-text--grey">
                      By registering you agree to 
                      <a id="terms" href="{{URL::asset('serviceA.html')}}" class="mdl-color-text--black">Charme's terms & condition</a>                    
                    <div class="clr"></div>
                  </li> 
                </ul>                  
                @include('common.form_errors')
                <button type="submit" class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  Create Your Account</button>      
              </form>
              <p class="hidden" id="create"></p>
                            
            </div>
          </main>
        <!-- /Page Content -->

    @endsection