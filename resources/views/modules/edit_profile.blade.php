       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">                   

                    @if (session('Update_failed'))
                    <div class="mdl-color-text--white bg-red mdl-card">
                       {{ session('Update_failed') }} 
                    </div>
                    @endif
 
                    <h4>Edit Profile Details Below</h4>     
                    <form action="{{url('profile/edit/dp')}}" name="changePic" method="post" enctype="multipart/form-data">
                      <label for="pics"> 
                        <div class="profile_pic">
                          <img src="{{$data['response']['data']['Customer']['pic_url'] ? $data['response']['data']['Customer']['pic_url'] : asset('img/user.jpg')}}" alt="" />
                        </div>
                        <input type="file" name="pics" id="pics" class="custom-file-input" />
                      </label>
                      @include('common.pic_errors')  
                      <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  
                        Change Picture
                      </button>
                    </form>
                    <form action="{{url('profile/edit')}}" name="editProfile" method="post">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                        <input class="mdl-textfield__input" type="text" id="email" disabled name="email" value="{{$data['response']['data']['Customer']['email']}}">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="email">email</label>
                      </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <input class="mdl-textfield__input " type="text" id="phone_no" disabled name="phone_no" value="{{$data['response']['data']['Customer']['phone_no']}}">
                      <label class="mdl-textfield__label bold mdl-color-pink" for="phone_no">Phone Number</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <input class="mdl-textfield__input " type="text" id="first_name" name="first_name" value="{{$data['response']['data']['Customer']['first_name']}}">
                      <label class="mdl-textfield__label bold mdl-color-pink" for="first_name">First Name</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                      <input class="mdl-textfield__input " type="text" id="last_name" name="last_name" value="{{$data['response']['data']['Customer']['last_name']}}">
                      <label class="mdl-textfield__label bold mdl-color-pink" for="last_name">Last Name</label>
                    </div>
                    @include('common.form_errors')                    
                    <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  
                      Update
                    </button>      
                    </form>  
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection