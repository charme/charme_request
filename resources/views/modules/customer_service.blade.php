       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content settings mdl-card">
         

          <ul class="bold central mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
            <li>
              <img class="thumbnails left" src="{{URL::asset('icon/forward_message-48.png')}}" style="padding-right: 5px">
              <span><a href="{{url('customer_service/message')}}" class="mdl-color-text--black"> Send Us a Message</a></span>
              <div class="clr"></div>
            </li> 
            <li>
              <span><b>Or</b></span>
              <div class="clr"></div>
            </li>
            <li>
              <img class="thumbnails left" src="{{URL::asset('icon/end_call-48.png')}}"  style="padding-right: 5px">
              <span>Call Us</span>
              <div class="clr"></div>
            </li> 
            <li>
              <span>08131332111</span>
              <div class="clr"></div>
            </li>
            <li>
              <span>08133163634</span>
              <div class="clr"></div>
            </li>
            <li>
              <span>08167681746</span>
              <div class="clr"></div>
            </li>
            <li>
              <span>08160866386</span>
              <div class="clr"></div>
            </li>
            <li>
              <img class="thumbnails left" src="{{URL::asset('icon/whatsapp-64.png')}}"  style="padding-right: 5px">
              <span>Whatsapp: 08131332111</span>
              <div class="clr"></div>
            </li> 
          </ul>
        </main>

        <!-- /Page Content -->
    @endsection