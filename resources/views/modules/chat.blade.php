       @extends('layouts.customer_sub_page')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content" style="padding-top:4em;">  
          <div class="chat bold">
            <ul id="appointment_chat" class="white_bg">
            @if(!empty($messages['ChatMessages']))
              @foreach($messages['ChatMessages'] as $chat)
                @if($chat['sent_by']=='customer')
                  <li class="message-left" id="{{$chat['id']}}"> <!-- left message -->
                    <img src="{{URL::asset('img/user.jpg')}}" alt="" class="minilogo mdl-shadow--2dp"> <!-- user -->
                    <div class="message mdl-shadow--2dp"> 
                      <p>{{$chat['message']}}</p> <!-- message text -->                      
                    </div>
                    <div id="msg_detail" class="text_right">
                      <small data-livestamp="{{date_format(date_create($chat['created_at']),'U')}}">
                      </small>
                      <small id="sent_status" class="mdl-color-text--green">&#10003;</small>
                    </div>
                  </li>
                @endif
                @if($chat['sent_by']=='supplier')
                  <li class="message-right" id="{{$chat['id']}}"> <!-- right message -->
                    <img src="{{URL::asset('img/user.jpg')}}" alt="" class="minilogo mdl-shadow--2dp"> <!-- user -->
                    <div class="message mdl-shadow--2dp">
                      <p>{{$chat['message']}}</p> <!-- message text -->
                    </div>
                        <div id="msg_detail" class="text_left">
                          <small data-livestamp="{{date_format(date_create($chat['created_at']),'U')}}">
                          </small>
                        </div>
                  </li>
                @endif
              @endforeach
            @endif
            </ul>
          </div>

          <!-- write message fixed bottom -->
          <div class="send_message">

              <div class="padding--0px mdl-textfield mdl-js-textfield textfield-demo bold">
                <textarea class="mdl-textfield__input" type="text" placeholder="Type message....." id="chat_message" ></textarea>
              </div>
            
            <button id="send_button" class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored send">
              <i class="material-icons">send</i>
            </button>
          </div>
          <div id="scrollTo" data-id="{{$messages['Conversation']['appointment_id']}}"></div>
        </main>

        <!-- /Page Content -->
    @endsection