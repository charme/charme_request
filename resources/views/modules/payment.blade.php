

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">

    
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>


    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <style type="text/css">
    .sweet-overlay{
          background-color: rgba(0, 0, 0, 0.95) !important;
    }
    </style>
    <!-- New plugins -->
    <script src="{{URL::asset('js/jquery.datetimepicker.min.js')}}"></script>
    @if(url()->current()==url('/services/payment'))
    <script type="text/javascript">
       /*Post-payment Options*/      
      $(document).ready(function(){
        $('#post_payment').ready(function(){
         

          @if($payment['status']=='ok')
            var title = "Payment Confirmed";
            var post_payment ="<div class='sa-button-container' style='display:block;'>"+
            '<span class="mdl-color-text--green bold">{{$payment['data']['PaymentResponse']['msg']}}. Enjoy the service.</span> '+
            '<a href="{{url('/appointments/chat')}}/{{$payment['data']['Appointment']['id']}}" id="{{$payment['data']['Appointment']['id']}}" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Chat With Professional <i class="fa fa-arrow-right"></i></a>'+
            '<a href="{{url('/appointments')}}" id="{{$payment['data']['Appointment']['id']}}" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> View Appointments <i class="fa fa-arrow-right"></i></a>'+
            "</div>";
            swal({  title: title,   
                  text: post_payment,   
                  type: "input",   
                  html: true,
                  imageUrl: '{{asset("img/charme_icons/payment-success.png")}}',
                  imageSize: '120x120',
                  showCancelButton: false,
                  allowOutsideClick: false,
                  showConfirmButton: false });
          @endif

          @if($payment['status']=='error')
            var title = "Appointment not created";
            var post_payment ="<div class='sa-button-container' style='display:block;'>"+
            '<div class="mdl-color-text--red bold">{{$payment['error']['msg']}}</div>'+
            '<a href="{{url('/services/location')}}" id="make_payment" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Try Again<i class="fa fa-arrow-left"></i></a>'+
            "</div>";
          swal({  title: title,   
                  text: post_payment,   
                  type: "input",   
                  html: true,
                  imageUrl: '{{asset("img/charme_icons/payment-unsuccessful.png")}}',
                  imageSize: '120x120',  
                  showCancelButton: false,
                  allowOutsideClick: false,
                  showConfirmButton: false });
          @endif

        });
      });     
    </script>
    @endif