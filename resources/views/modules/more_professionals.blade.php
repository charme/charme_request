@if(empty($available_professionals))
@else
    @foreach($available_professionals as $professionals)
        <div class="line-block mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--2-col-phone mdl-shadow--2dp">
            <a href="{{url('/services/professionals/profile')}}/{{$professionals['id']}}">
                <img src="{{$professionals['pic_url'] ? $professionals['pic_url'] : URL::asset('img/user.jpg')}}" alt="" />
                      <span class="mdl-card__actions mdl-color--white mdl-color-text--black">
                        <small class="bold">{{$professionals['first_name']}}</small>
                      </span>
            </a>
        </div>
    @endforeach
    @if(!is_null($page['next_page_url']))
        <a href="{{url('/professionals/more')}}?page={{$page['next_page_url']}}" id="loadnext">home</a>
    @endif
@endif