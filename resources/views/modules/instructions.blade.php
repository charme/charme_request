       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                  <!-- Instructions -->
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">
                    
                    <h4>Please enter any other instructions you have for your professional.</h4>                     
                    <form action="{{url('services/instructions')}}" method="POST">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input mdl-cell--8-col" name="instructions" type="text" id="instructions">
                        <label class="mdl-textfield__label bold mdl-color-pink" for="instructions">(leave empty if none)</label>
                      </div>
                      <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                      Next <i class='fa fa-arrow-right'></i>
                      </button>                      
                    </form>  

                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection