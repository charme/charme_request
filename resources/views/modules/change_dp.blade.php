       @extends('layouts.header')
       @section('content')
       
        <!-- Page Content -->
        <main class="mdl-layout__content">  
          <div class="contact-about">
            <div class="mdl-card mdl-shadow--2dp about">
              <div class="mdl-grid">
                <div class="central mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-card mdl-shadow--8dp">                   

                    @if (session('Update_failed'))
                    <div class="mdl-color-text--white bg-red mdl-card">
                       {{ session('Update_failed') }} 
                    </div>
                    @endif

                    <h4>Edit Profile Details Below</h4>                     
                    <form action="<?php echo url('profile/edit'); ?>" method="post" enctype="multipart/form-data">
                      <input type="file" name="dp" />
                    <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">  
                      Update
                    </button>      
                    </form>  
                  </div>
                </div>
                  <!-- /Instructions -->
              </div>
            </div>
          </div>
        </main>

        <!-- /Page Content -->
    @endsection