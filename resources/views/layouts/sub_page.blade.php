<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <title>Charmé</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#E91E63" />
    <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
     <!-- iOS -->
        <link rel="apple-touch-icon" href="{{URL::asset('iOS/touch-icon-iphone.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('iOS/touch-icon-ipad.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('iOS/touch-icon-iphone-retina.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('iOS/touch-icon-ipad-retina.png')}}">
        <link rel="apple-touch-startup-image" href="{{URL::asset('iOS/home.jpg')}}">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <!-- /Extra CSS -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vegas.min.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('css/colorbox.css')}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('css/nivo_lightbox_themes/default/default.css')}}" rel="stylesheet" media="screen">   <!-- Lightbox Styles -->
    <link rel="stylesheet" href="{{URL::asset('css/jquery.countdown.css')}}" /><!-- Countdown css -->

    @include('sub_view/analytics')    
  </head>
  
    <body class="@if(str_contains(url()->current(),'professional/appointments/chat/')) white_bg @endif">
        <div class="@if(!str_contains(url()->current(),'/appointments/chat/')) animsition @endif white_bg" >
          <!-- Header -->
          <header class="mdl-layout__header mdl-shadow--2dp mdl-layout__header--waterfall mdl-color--white no-drawer">
            <div class="mdl-layout__header-row">
            <!-- Customer id index -->
            @if(session('Professional'))
              <span id="current_user" data-id="{{session('Professional')->id}}"></span>
            @endif

            @if(url()->current()==url('professional/client-info'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> Client information
              </a>
            @endif

            @if(url()->current()==url('professional/client-info/reschedule'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> Appointment Reschedule
              </a>
            @endif

            @if(url()->current()==url('/register/professional/more'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left"> </i> A few more details
              </a>
            @endif

            <!-- Appointment -->
            <!-- details -->
            @if(str_contains(url()->current(),'professional/appointments/details/'))
              <a href="{{url('professional/appointments')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Appointment Details
              </a>
            @endif
            <!-- Completed -->
            @if(str_contains(url()->current(),'professional/appointments/completed/'))
            <a href="{{url('professional/appointments')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Appointment Completed
            </a>
            @endif
            <!-- Location -->
            @if(str_contains(url()->current(),'professional/appointments/location'))
              <a href="{{url()->previous()}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> {{$data['Customer']['first_name']}}'s location
              </a>
            @endif
            <!-- Chat -->
            @if(str_contains(url()->current(),'professional/appointments/chat/'))
              <a href="{{url('professional/appointments')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Chat with Customer
              </a>
            @endif
            <!-- Customer Care -->
            @if(str_contains(url()->current(),'professional/customer_service/message'))
              <a href="{{url('professional/customer_service')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Send Us A Message
              </a>
            @endif
            <!-- Earnings breakdown -->
            @if(str_contains(url()->current(),'professional/earnings/breakdown/'))
              <a href="{{url('professional/earnings')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> {{ date_format(date_create($month),'F')}}
              </a>
            @endif
            <!-- Change password -->
            @if(url()->current()==url('professional/settings/password'))
            <a href="{{url('professional/settings')}}" class="bold mdl-color-text--black">
                <i class="fa fa-arrow-left mdl-color-text--pink"> </i> Change Password
              </a>
            @endif

            <!-- Spacer -->
            <div class="mdl-layout-spacer"></div>
            @if(str_contains(url()->current(),'professional/appointments/chat/'))
              <a href="{{url('professional/appointments/location')}}/{{session('appointment_id')}}   " class="mdl-color-text--pink mdl-color--white padding--5px border-radius--5px bold">                
                <i class="fa fa-map-marker fa-2x mdl-color-text--pink"></i>
              </a>              
            @endif
            </div>
          </header>
           <!-- header background -->        
              @yield('content')
         
    </div>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <!-- New plugins -->
    <script src="{{URL::asset('js/vegas.min.js')}}"></script>


    <script src="{{URL::asset('js/nivo-lightbox.min.js')}}"></script>       <!-- Lightbox/Modalbox -->
    <script type="text/javascript" src="{{URL::asset('js/validate.min.js')}}"></script><!-- Form validator -->
    <script src="{{URL::asset('js/moment.js')}}"></script><!-- moment.js -->
    <script src="{{URL::asset('js/livestamp.min.js')}}"></script><!-- livestamp.min.js -->
    <script src="{{URL::asset('js/jquery.plugin.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.countdown.min.js')}}"></script><!-- Flip Counter -->
    <script src="https://js.pusher.com/3.0/pusher.min.js"></script><!-- Pusher -->
    <script src="{{URL::asset('js/formProfessionalValidate.js')}}"></script><!-- Form Validate -->
    <script src="{{URL::asset('js/myProfessionalfunctions.js')}}"></script><!-- Professional functions -->
    <script src="{{URL::asset('js/pusherProfessionalfunctions.js')}}"></script><!-- Professional functions -->
    <script src="{{URL::asset('js/chatProfessionalfunctions.js')}}"></script><!-- Professional chat functions -->

    <script type="text/javascript">
      var img="{{asset('img/push_image.png')}}";
      var chat_url="{{url("professional/appointments/chat")}}/";
      var chat_img="{{asset('img/user.jpg')}}";
      var send_chat_url="{{url('professional/appointments/chat/send')}}";
      var profile_desc_url= "{{url('professional/profile/description')}}";
      var appointment_url="{{url("professional/appointments")}}";   
      var appointment_details_url= "{{url('professional/appointments/details')}}/";
      var app_start_url= "{{url('professional/appointments/start')}}";
      var app_completed_url="{{url('professional/appointments/completed')}}/";
      var app_cancel_url="{{url('professional/appointments/cancel')}}/";
      var timer_img= "{{URL::asset("icon/time-48.png")}}";
      var available_url="{{url('professional/available')}}";
      var inbox_url="{{url('professional/inbox')}}";
     
    </script>

    @if(str_contains(url()->current(),'appointments/location'))
      <script type="text/javascript">
          var lat="{{$data['MeetupAddress']['latitude']}}";
          var lng="{{$data['MeetupAddress']['longitude']}}";
          var address="{{$data['MeetupAddress']['address_details']}}";
          var supplier_name="{{$data['Supplier']['first_name']}}";
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBQdQFks6cQ7mW_A_A5tdummLJ-9fvVXRE&callback=initMap";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
      </script>
      <script>
        function initMap() {
            var my_latlng;
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 16,
              center: new google.maps.LatLng(lat,lng),
            });
            var infowindow = new google.maps.InfoWindow;
            var appointment_location = new google.maps.Marker({
              position: new google.maps.LatLng(lat,lng),
              map: map,
              animation: google.maps.Animation.BOUNCE,
              title: '"'+address+'"'
            });
            infowindow.setContent(address);
            infowindow.open(map, appointment_location);
            my_latlng=new google.maps.LatLng(lat,lng);
            calculateAndDisplayRoute(directionsService, directionsDisplay,my_latlng);

              if(navigator.geolocation) {
                browserSupportFlag = true;
                navigator.geolocation.getCurrentPosition(function(position) {
                  current_latlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                  //calculateAndDisplayRoute(directionsService, directionsDisplay,current_latlng);
                }, function() {
                  handleNoGeolocation(browserSupportFlag);
                });
              }
              // Browser doesn't support Geolocation
              else {
                browserSupportFlag = false;
                handleNoGeolocation(browserSupportFlag);
              }
              function handleNoGeolocation(errorFlag) {
                if (errorFlag == true) {
                  alert("Geolocation service failed.");
                } else {
                  alert("Your browser doesn't support geolocation.");
                }
              }

            directionsDisplay.setMap(map);
        }
      </script>
    @endif

    @if(url()->current()==url('/register/professional/more'))
      <script>
      /*Phone verification form*/
        $('input:checkbox').simpleCheckbox();
        var validate_more_form= new FormValidator('moreDetails',
          [
            {
              name: 'description',
              display: 'About Me',
              rules: 'required|max_length[140]'
            }            
          ],
          function(errors,event){
            if (errors.length>0) {
              console.log(errors);
              $('.form_errors').html(errors[0].message);
              errors[0].element.focus();
              return false;
            };
            if (errors.length<=0) {
              $('.form_errors').fadeOut('slow');              
            };

          });
        validate_more_form.setMessage('required', 'Please tell us about yourself');
      $(document).ready(function(){     
        $("#cat > a").click(function(){
          var id= $(this).attr('id');
        });

        var new_cat= '<div class="dropdown mdl-cell--12-col text_left">'
                              +'<select id="select_category" onchange="loadService(this.value,$(this).text())" class="mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone select" name="service_category">'
                                +'<option id="null" value="null">Select service category</option>'
                                +'@foreach($categories as $category)'
                                 +'<option id="{{$category["id"]}}" value="{{$category["id"]}}">{{$category["name"]}}</option>'
                                +'@endforeach'
                              +'</select>';
        $('#new_service').click(function(){
          $("#cat_list").append(new_cat);
        });

      });
      function loadService(clicked_id,clicked_name){
          //var id = this;
          if (clicked_id !== 'null') {
            var url = '{{url("/")}}';
            url +='/register/add_new_services/'+clicked_id;
            parent.$("#services_select").fadeIn('slow');
            $('html, body').animate({scrollTop: $("#services_select").offset().top}, 'slow');
             var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
              if (xhttp.readyState == 4 && xhttp.status == 200) {
              document.getElementById("services_select").innerHTML = xhttp.responseText;
              }
              if (xhttp.readyState == 4 && xhttp.status == 500) {
              document.getElementById("services_select").innerHTML = '<i class="width--100 fa fa-times-circle fa-5x mdl-color-text--red bold central"></i><br><span class="mdl-color-text--black text_center bold">Service Loading Failed (Network Error)</span>';
              }
               if (xhttp.readyState !== 4) {
               document.getElementById("services_select").innerHTML = '<i class="width--100 fa fa-spinner fa-5x fa-spin mdl-color-text--pink bold central"></i><br><span class="mdl-color-text--pink text_center bold">Loading Service List</span>';
               }
              };
              xhttp.open("GET", url, true);
              xhttp.send();
            };
        }
      function SaveCat() {
                                  var val = [];
                                  var id = [];
                                  $(':checkbox:checked').each(function(i){
                                  var url = null;
                                  var url = '{{url("/register/save_services/")}}';
                                   url += '/'+$(this).attr('id');                                  
                                  $.get(url);

                                       var xhttp = new XMLHttpRequest();
                                      xhttp.onreadystatechange = function() {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                         document.getElementById("demo").innerHTML = '<span class="mdl-color--green mdl-color-text--white">Service(s) Successfully Added</span>';
                                        }

                                        if (xhttp.readyState == 4 && xhttp.status == 500) {
                                         document.getElementById("demo").innerHTML = 'Service(s) were not added';
                                        }

                                        if (xhttp.readyState !== 4) {
                                         document.getElementById("demo").innerHTML = '<i class="fa fa-spinner fa-2x fa-spin mdl-color-text--pink bold central"></i>Adding Service';
                                        }
                                      };
                                      xhttp.open("GET", url, true);
                                      xhttp.send();

                                  });
                                  var un_val =[];
                                  var un_id=[];
                                  $(":checkbox:not(:checked)").each(function(i){
                                    var url = null;
                                  var url = '{{url("/register/delete_services/")}}';
                                  url += '/'+$(this).attr('id');
                                    var xhttp = new XMLHttpRequest();
                                      xhttp.onreadystatechange = function() {
                                        if (xhttp.readyState == 4 && xhttp.status == 200) {
                                         document.getElementById("demo").innerHTML = '<span class="mdl-color--green mdl-color-text--white">Service(s) Successfully Added</span>';
                                         //parent.$("#services_select").fadeIn('slow');
                                        }

                                        if (xhttp.readyState == 4 && xhttp.status == 500) {
                                         document.getElementById("demo").innerHTML = 'Service(s) were not added';
                                        }

                                        if (xhttp.readyState !== 4) {
                                         document.getElementById("demo").innerHTML = '<i class="fa fa-spinner fa-2x fa-spin mdl-color-text--pink bold central"></i>Adding Service';
                                        }
                                      };
                                      xhttp.open("GET", url, true);
                                      xhttp.send();
                                  });                                    
      }
      </script>
    @endif

  </body>
</html>
