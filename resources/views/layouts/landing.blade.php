﻿<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Charmé</title>
        <meta name="description" content="">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="theme-color" content="#E91E63" />
        <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
        <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
        <!-- iOS -->
        <link rel="apple-touch-icon" href="{{URL::asset('iOS/touch-icon-iphone.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('iOS/touch-icon-ipad.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('iOS/touch-icon-iphone-retina.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('iOS/touch-icon-ipad-retina.png')}}">
        <link rel="apple-touch-startup-image" href="{{URL::asset('iOS/home.jpg')}}">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <!-- Stylesheets -->
        <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
        <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
        <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
        <!-- /Extra CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vegas.min.css')}}"/>

        @include('sub_view/analytics')
    </head>
    <body>
    <div class="animsition">
      <div class="mdl-layout mdl-js-layout mdl-layout--overlay-drawer-button"> 
        
         @yield('content')
         <a id="bookmark-this" href="#" title="Bookmark This Page">
            <i class="fa fa-cloud-download mdl-color-text--pink" aria-hidden="true"></i>
         </a>
      </div>
    </div>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <!-- New plugins -->
    <script src="{{URL::asset('js/jquery.datetimepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/vegas.min.js')}}"></script>
    
    <script type="text/javascript">
        $(document).ready(function(){

            $('#bookmark-this').click(function(e) {
                var bookmarkURL = window.location.href;
                var bookmarkTitle = document.title;

                if ('addToHomescreen' in window && window.addToHomescreen.isCompatible) {
                  // Mobile browsers
                  addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
                } else if (window.sidebar && window.sidebar.addPanel) {
                  // Firefox version < 23
                  window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
                } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
                  // Firefox version >= 23 and Opera Hotlist
                  $(this).attr({
                    href: bookmarkURL,
                    title: bookmarkTitle,
                    rel: 'sidebar'
                  }).off(e);
                  return true;
                } else if (window.external && ('AddFavorite' in window.external)) {
                  // IE Favorite
                  window.external.AddFavorite(bookmarkURL, bookmarkTitle);
                } else {
                  // Other browsers (mainly WebKit - Chrome/Safari)
                  alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
                }

                return false;
              });


        $('#register').click(function(){
              var register ='<a href="<?php echo url("/register/customer");?>" id="make_payment" class="mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">I am looking for a professional</a>'+     

                    '<a href="<?php echo url("/register/professional");?>" id="make_payment" class="mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--4-col-phone mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">I want to offer my services</a>';
              swal({  title: "Choose Your Need",   
                      text: register,   
                      type: "input",   
                      html: true,
                      showCancelButton: false,
                      allowOutsideClick: true,
                      showConfirmButton: false });
                  $('#datetimepicker').datetimepicker({
                    minDate:0
                  });
            });
        $("#bg").vegas({
            delay: 5000,
            shuffle: false,
            cover: true,
            slides: [
                { src: "{{URL::asset('/img/slider/home.jpg')}}" },
                { src: "{{URL::asset('/img/slider/slide1.jpg')}}"},
                { src: "{{URL::asset('/img/slider/slide2.jpg')}}", valign: 'center'},
                { src: "{{URL::asset('/img/slider/slide3.jpg')}}", align: 'left'  }
                ]
            });
        });
    </script>
    <!-- Add to homescreen files -->
    
    <link rel="stylesheet" type="text/css" href="{{URL::asset('bookmark/addtohomescreen.css')}}">
    <script src="{{URL::asset('bookmark/addtohomescreen.min.js')}}"></script>
    <script>
    addToHomescreen();
    </script>
  </body>
</html>
