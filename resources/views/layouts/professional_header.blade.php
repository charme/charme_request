<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <title>Charmé</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#E91E63" />
    <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
     <!-- iOS -->
        <link rel="apple-touch-icon" href="{{URL::asset('iOS/touch-icon-iphone.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('iOS/touch-icon-ipad.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('iOS/touch-icon-iphone-retina.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('iOS/touch-icon-ipad-retina.png')}}">
        <link rel="apple-touch-startup-image" href="{{URL::asset('iOS/home.jpg')}}">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">

    
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('css/nivo_lightbox_themes/default/default.css')}}" rel="stylesheet" media="screen">   <!-- Lightbox Styles -->
    <link rel="stylesheet" href="{{URL::asset('css/jquery.countdown.css')}}" /><!-- Countdown css -->

    @include('sub_view/analytics')
  </head>
  
    <body>
    <div class="@if(!str_contains(url()->current(),'/appointments/chat/')) animsition @endif">
      <div class="mdl-layout mdl-js-layout mdl-layout--overlay-drawer-button">
        <header class="mdl-layout__header mdl-shadow--2dp mdl-layout__header--waterfall mdl-color--white">
          <div class="mdl-layout__header-row">
          @if(url()->current()==url('professional/services'))
          <a href="" class="mdl-color-text--black bold">My Services</a>
          @endif

          @if(url()->current()==url('professional/inbox'))
          <span class="mdl-color-text--black bold">Inbox</span>
            @endif

          @if(url()->current()==url('professional/appointments'))
          <span class="mdl-color-text--black bold">Appointments</span>
            @endif
          @if(str_contains(url()->current(),'professional/appointments/chat/'))
            <span class="mdl-color-text--black bold">Chat with Client</span>
          @endif

          @if(str_contains(url()->current(),'appointments/details/'))
            <span class="mdl-color-text--black bold">Appointment Details</span>
          @endif

            @if(url()->current()==url('professional/earnings'))
          <span class="mdl-color-text--black bold">Earnings</span>
            @endif 

            @if(url()->current()==url('professional/profile/location'))
          <span class="mdl-color-text--black bold">My Location</span>
            @endif

            @if(str_contains(url()->current(),'professional/appointments/completed/'))
          <span class="mdl-color-text--black bold">Appointment Completed</span>
            @endif

            @if(url()->current()==url('professional/profile/edit'))
          <span class="mdl-color-text--black bold">Edit Profile</span>
            @endif

            @if(url()->current()==url('professional/profile'))
          <span class="mdl-color-text--black bold">Profile</span>
            @endif

            @if(url()->current()==url('professional/customer_service'))
          <span class="mdl-color-text--black bold">Customer Care</span>
            @endif

            @if(url()->current()==url('professional/customer_service/message'))
          <span class="mdl-color-text--black bold">Send Us A Message</span>
            @endif

            @if(str_contains(url()->current(),'professional/earnings/breakdown/'))
          <span class="mdl-color-text--black bold">{{ date_format(date_create($month),'F')}}</span>
            @endif
            <!-- Settings -->
            @if(url()->current()==url('professional/settings'))
          <span class="mdl-color-text--black bold">Settings</span>
            @endif

            @if(url()->current()==url('professional/settings/password'))
          <span class="mdl-color-text--black bold">Change Password</span>
            @endif

            <!-- Spacer -->
            <div class="mdl-layout-spacer"></div>
            @if(url()->current()==url('professional/profile'))
            <a href="{{url('professional/profile/edit')}}" class="mdl-color-text--white mdl-color--pink padding--5px border-radius--5px bold">Edit Profile</a>
            @endif
            @if(url()->current()==url('professional/profile/edit'))
            <a href="{{url('professional/profile')}}" class="mdl-color-text--white mdl-color--pink padding--5px border-radius--5px bold">View Profile</a>
            @endif
            @if(url()->current()==url('professional/profile/location'))
            <a href="{{url('professional/profile')}}" class="mdl-color-text--white mdl-color--pink padding--5px border-radius--5px bold">View Profile</a>
            @endif

            @if(str_contains(url()->current(),'professional/appointments/chat/'))
              <a href="{{url('professional/appointments/location')}}/{{session('appointment_id')}}   " class="mdl-color-text--pink mdl-color--white padding--5px border-radius--5px bold">                
                <i class="fa fa-map-marker fa-2x mdl-color-text--pink"></i>
              </a>              
            @endif
          </div>
          @if(url()->current()==url('professional/appointments'))
          <div class="mdl-layout__tab-bar mdl-js-ripple-effect mdl-color-text--black">
            <a href="#scroll-tab-1" class="mdl-layout__tab mdl-color-text--black is-active bold">Upcoming</a>
            <a href="#scroll-tab-2" class="mdl-layout__tab mdl-color-text--black bold">Past</a>
          </div>
          @endif
        </header>
        <!-- Top-right Dropdown Menu -->
        <!-- /Top-right Dropdown Menu -->
        <!-- Sidebar -->
        <div class="mdl-layout__drawer mdl-color--pink">
          <!-- Top -->
          <div class="mdl-card mdl-shadow--2dp mdl-color--pink mdl-color-text--black drawer-profile">
            <div class="mdl-card__title user"  id="current_user" data-id="{{session('Professional')->id}}">
              <a href="{{url('professional/profile')}}">
                @if (session('Professional'))
                    <img src="{{(session('Professional')->pic_url ? session('Professional')->pic_url : asset('img/user.jpg') )}}" alt="" />
                    <span class="user-name bold">
                      {{session('Professional')->first_name}} {{session('Professional')->last_name}}
                    </span>
                @endif
              </a>
            </div>
          </div>
          <!-- /Top -->

          <!-- Main Navigation -->
          <nav class="mdl-navigation bold"><!-- mdl-navigation__link -->
            <div class="padding--7px float--none mdl-color--white" id="toggleAvailable">
                  <div class="line">
                    <i id="available_color" class=" {{session('Professional')->available ? 'mdl-color-text--green': ''}} right-margin--12px fa fa-circle"></i>                  
                  </div>
                  <div id="available_text" class="line mdl-color-text--black bold">{{session('Professional')->available ? 'Available': 'Unavailable'}}</div>                  
                  <div id="available_toggle" class="line tog {{session('Professional')->available ? 'on': ''}} right">
                  </div>
              <div class="clr"></div>              
            </div>
            <div id="notify" class="central mdl-color-text--white">
              <i class="fa fa-refresh fa-spin fa-2x fa-fw margin-bottom"></i>
                <span class="sr-only line mdl-color-text--white">Updating</span>
            </div>
            <a class="mdl-navigation__link animsition-link white" href="{{url('/professional/services')}}">My Services</a>
            <a class="mdl-navigation__link animsition-link white" href="{{url('professional/appointments')}}">Appointments</a>
            <a class="mdl-navigation__link animsition-link white" href="{{url('professional/inbox')}}">Inbox</a>
            <a class="mdl-navigation__link animsition-link white" href="{{url('professional/customer_service')}}">Customer Service</a>    
            <a class="mdl-navigation__link animsition-link white" href="{{url('/professional/earnings')}}">Earnings</a>  
            <a class="mdl-navigation__link animsition-link white" href="{{url('professional/settings')}}">Settings</a>
          </nav>
          <!-- /Main Navigation -->
        </div>
         @yield('content')
      </div>
    </div>
    <div class="live_updates">
      
    </div>
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <!-- New plugins -->
    <script src="{{URL::asset('js/jquery.datetimepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/nivo-lightbox.min.js')}}"></script>       <!-- Lightbox/Modalbox -->
    <script type="text/javascript" src="{{URL::asset('js/validate.min.js')}}"></script><!-- Form validator -->
    <script src="{{URL::asset('js/moment.js')}}"></script><!-- moment.js -->
    <script src="{{URL::asset('js/livestamp.min.js')}}"></script><!-- livestamp.min.js -->
    <script src="{{URL::asset('js/jquery.plugin.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.countdown.min.js')}}"></script><!-- Flip Counter -->
    <script src="https://js.pusher.com/3.0/pusher.min.js"></script><!-- Pusher -->
    <script src="{{URL::asset('js/formProfessionalValidate.js')}}"></script><!-- Form Validate -->
    <script src="{{URL::asset('js/myProfessionalfunctions.js')}}"></script><!-- Professional functions -->
    <script src="{{URL::asset('js/pusherProfessionalfunctions.js')}}"></script><!-- Professional functions -->
    <script src="{{URL::asset('js/chatProfessionalfunctions.js')}}"></script><!-- Professional chat functions -->
    <script type="text/javascript">      
      var img="{{asset('img/push_image.png')}}";
      var chat_url="{{url("professional/appointments/chat")}}/";
      var chat_img="{{asset('img/user.jpg')}}";
      var send_chat_url="{{url('professional/appointments/chat/send')}}";
      var profile_desc_url= "{{url('professional/profile/description')}}";
      var appointment_url="{{url("professional/appointments")}}";   
      var appointment_details_url= "{{url('professional/appointments/details')}}/";
      var app_start_url= "{{url('professional/appointments/start')}}";
      var app_completed_url="{{url('professional/appointments/completed')}}/";
      var app_cancel_url="{{url('professional/appointments/cancel')}}/";
      var timer_img= "{{URL::asset("icon/time-48.png")}}";
      var available_url="{{url('professional/available')}}";
      var inbox_url="{{url('professional/inbox')}}";
    </script>
    <!-- End of pusher connect -->
    <!-- Chat communications -->
    @if(url()->current()==url('professional/settings'))
      <script>       
        /*LightBox functions*/
        $(document).ready(function(){ 
            $('#terms').nivoLightbox(); 
            $('#privacy').nivoLightbox(); 
            $('#cancellation').nivoLightbox(); 
            $('#service').nivoLightbox(); 
        }); 
      </script>
    @endif    
    <!-- Map Functions -->
    @if(url()->current()==url('professional/profile/location'))
      <script type="text/javascript">
        function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBQdQFks6cQ7mW_A_A5tdummLJ-9fvVXRE&callback=initMap";
        document.body.appendChild(element);
        }
        if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
        else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
        else window.onload = downloadJSAtOnload;
      </script>
      <script>    
          function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 8,
              center: {lat: 6.523276, lng: 3.540791}
            });
            var geocoder = new google.maps.Geocoder();

            document.getElementById('address').addEventListener('change', function() {
              geocodeAddress(geocoder, map);
            });
          }

          function geocodeAddress(geocoder, resultsMap) {
                $("#location_found").fadeOut('slow');
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === google.maps.GeocoderStatus.OK) {
                    resultsMap.panTo(results[0].geometry.location);
                     resultsMap.setZoom(15);

                    document.getElementById("location").value = results[0].geometry.location;
                    $("#location_found").fadeIn('slow');
                    $("#location_found").css('background-color','green !important');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--green mdl-color-text--black padding--10px line border-radius--5px">'+
                    'location found on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--green border-radius--5px">map</a></p>';
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
          
                  } 

                  else {

                    $("#location_found").fadeIn('slow');
                    document.getElementById("location_found").innerHTML='<p class="mdl-color--red mdl-color-text--black padding--10px line border-radius--5px">'+
                    'We could not find location on <a href="#map" class="padding--5px mdl-color--white mdl-color-text--red border-radius--5px">map</a></p>';
                    //alert('We could not find your location on the map: ' + status);
                  }
                    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
                });
          }
      </script>
    @endif
  </body>
</html>
