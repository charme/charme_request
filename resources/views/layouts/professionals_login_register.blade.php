<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <title>Charmé</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#E91E63" />
    <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
     <!-- iOS -->
        <link rel="apple-touch-icon" href="{{URL::asset('iOS/touch-icon-iphone.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('iOS/touch-icon-ipad.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('iOS/touch-icon-iphone-retina.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('iOS/touch-icon-ipad-retina.png')}}">
        <link rel="apple-touch-startup-image" href="{{URL::asset('iOS/home.jpg')}}">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="{{URL::asset('css/swipebox.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('css/nivo_lightbox_themes/default/default.css')}}" rel="stylesheet" media="screen">   <!-- Lightbox Styles -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vegas.min.css')}}"/>

    @include('sub_view/analytics')
  </head>

    <body>
        <div class="animsition white_bg" >
          <!-- Header -->
          <header class="mdl-layout__header mdl-layout__header--waterfall mdl-shadow--2dp transparent-header no-drawer">
            <div class="mdl-layout__header-row">
            @if(url()->current()==url('/signin/professional'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Professional Sign in
              </a>
            @endif

            @if(str_contains(url()->current(),'professional/password-recovery/'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Recover Password
              </a>
            @endif

            @if(url()->current()==url('/register/customer') || url()->current()==url('/register/professional'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Create your account
              </a>
            @endif

            @if(url()->current()==url('/professional/forgot_password'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Forgot Password
              </a>
            @endif
            </div>
          </header>

          <!-- Page Content -->
          <main class="mdl-layout__content login_bg valign-wrapper"> <!-- vertical align container -->
            <div class="valign mdl-color-text--white bold"> <!-- vertical align element -->
              <h2 class="italics"><a href="{{url('/')}}" class="mdl-color-text--white">Charmé</a></h2>
              <span class="mdl-color-text--white bold">beauty and wellness on demand.</span>
            </div>
          </main>
           <!-- header background -->        
              @yield('content')
         
    </div>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/jquery.swipebox.min.js')}}"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <!-- New plugins -->
    <script src="{{URL::asset('js/jquery.datetimepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/vegas.min.js')}}"></script>
    <script src="{{URL::asset('js/nivo-lightbox.min.js')}}"></script>       <!-- Lightbox/Modalbox -->
    <script type="text/javascript" src="{{URL::asset('js/validate.min.js')}}"></script><!-- Form validator -->

    <script>       
        /*LightBox functions*/
        $(document).ready(function(){ 
            $('#terms').nivoLightbox(); 
            $('#privacy').nivoLightbox(); 
            $('#cancellation').nivoLightbox(); 
            $('#service').nivoLightbox(); 
        }); 
    </script>


    <script type="text/javascript">
        /*Show password field*/
          function showPassword(in_field){
            var id = $(in_field).prevAll('input').attr("id");
            var type = $(in_field).prevAll('input').attr("type");
              if (type=='password') {
              document.getElementById(id).type="text"; 
              $(in_field).html('<i class="fa fa-eye-slash"></i>');
              //console.log(type+" = " +id );        
              }

              if (type=='text') {
              $(in_field).html('<i class="fa fa-eye"></i>');
              document.getElementById(id).type="password"; 
              //console.log(type);              
              }
          }

          /*Validate forgot password Form*/
        var validate_forgot_password_form= new FormValidator('forgot_password',
            [
              {
                name: 'email',
                rules: 'required|email'
              }
            ],
            function(errors,event){
              if (errors.length>0) {
                //console.log(errors);
                $('.form_errors').html(errors[0].message);
                errors[0].element.focus();
                return false;
              };

            });

        /*Recover Account*/
         /*Validation for password change*/
        var validate_resetPassword= new FormValidator('resetPassword',
          [
            {
              name: 'password',
              display: 'Enter new password',
              rules: 'required|alpha_numeric|min_length[6]'            
            },
            {
              name: 'confirm_password',
              display: ' Enter confirmed password',
              rules: 'required|alpha_numeric|matches[password]'
            }
          ],
          function(errors,event){
            if (errors.length>0) {
              $('.password_errors').html(errors[0].message);
                errors[0].element.focus();
              return false;
            };

          });
        validate_resetPassword.setMessage('required', 'Please %s.');
        validate_resetPassword.setMessage('matches[password]', 'Password does not match.');
        
        var validate_login_form= new FormValidator('professionalSignIn',
            [
              {
                name: 'username',
                display: 'Email or Phone Number',
                rules: 'required'
              },
              {
                name: 'password',
                rules: 'required|min_length[6]'
              }
            ],
            function(errors,event){
              if (errors.length>0) {
                //console.log(errors);
                $('.form_errors').html(errors[0].message);
                errors[0].element.focus();
                return false;
              };

            });
          validate_login_form.setMessage('required', 'Kindly enter %s.');

           /*Validate Registration form*/

        var validate_register_form= new FormValidator('registerProfessional',
            [
              {
                name: 'first_name',
                display: 'First Name',
                rules: 'required|alpha'
              },
              {  
                name: 'last_name',
                display: 'Last Name',
                rules: 'required|alpha'
              },
              {
                name: 'gender',
                display: 'Gender',
                rules: 'required'
              },
              {
                name: 'email',
                display: 'email',
                rules: 'required|valid_email'
              },
              {
                name: 'phone_no',
                display: 'Phone Number',
                rules: 'required|numeric'
              },
              {
                name: 'password',
                display: 'Password',
                rules: 'required|min_length[6]'
              },
              {
                name: 'confirm_password',
                display: 'Confirm Password',
                rules: 'required|min_length[6]|matches[password]'
              }    
            ],
            function(errors,event){
              if (errors.length>0) {
                console.log(errors);
                $('.form_errors').html(errors[0].message);
                if (errors[0].display!='Gender') {
                  errors[0].element.focus();                
                };
                return false;
                event.preventDefault();
              };
              if (errors.length<=0) {
                $('.form_errors').fadeOut('slow');              
              };

            });
          validate_register_form.setMessage('required', '%s is required.');
          validate_register_form.setMessage('valid_email', 'Please use a valid %s.');
          validate_register_form.setMessage('matches[password]', 'Password does not match.');

      
        var validate_verify_form= new FormValidator('verifyPhone',
          [
            {
              name: 'code',
              display: 'verification Code',
              rules: 'required|alpha_numeric'
            }            
          ],
          function(errors,event){
            if (errors.length>0) {
              //console.log(errors);
              $('.form_errors').html(errors[0].message);
              errors[0].element.focus();
              return false;
            };
            if (errors.length<=0) {
              $('.form_errors').fadeOut('slow');              
            };

          });
        validate_verify_form.setMessage('required', 'verification code is required.');

        $(document).ready(function(){
         

          $('#logins > #social').click(function(){
            var enter_phone_number ="<div class='sa-button-container'>"+
            "<div style='display:block;'>"+
            "<form method='get' action=''>"+
            '<input name="phone_number" type="text" class="mdl-textfield__input">'+
            "<button type='button' class='mdl-color--white mdl-color-text--pink border-pink'>Cancel</button>"+
            "<button type='submit' class='mdl-color--pink mdl-color-text--white'>OK</button>"+
            "</div>"+
            "</form></div>";
            swal({  title: "Please Enter Your Phone Number",   
                    text: enter_phone_number,   
                    type: "input",   
                    html: true,
                    showCancelButton: false,
                    allowOutsideClick: true,
                    showConfirmButton: false });
          });

        });
    </script>

  </body>
</html>
