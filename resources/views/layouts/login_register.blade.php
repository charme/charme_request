<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <title>Charmé</title>
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#E91E63" />
    <meta name="google-signin-client_id" content="609910267560-4ql3id0qnpsirjhps21ch55qoggrmogg.apps.googleusercontent.com">
    <link rel="icon" sizes="192x192" href="{{URL::asset('charme-highres.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon"/>
     <!-- iOS -->
        <link rel="apple-touch-icon" href="{{URL::asset('iOS/touch-icon-iphone.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('iOS/touch-icon-ipad.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('iOS/touch-icon-iphone-retina.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('iOS/touch-icon-ipad-retina.png')}}">
        <link rel="apple-touch-startup-image" href="{{URL::asset('iOS/home.jpg')}}">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500" type="text/css"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/css/animsition.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-pink.min.css">  
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    <!-- /Extra CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/jquery.datetimepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vegas.min.css')}}"/>

    @include('sub_view/analytics')
  </head>

    <body>
        <div class="animsition white_bg" >
          <!-- Header -->
          <header class="mdl-layout__header mdl-layout__header--waterfall mdl-shadow--2dp transparent-header no-drawer">
            <div class="mdl-layout__header-row">
            @if(url()->current()==url('/signin'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Sign in
              </a>
            @endif
            @if(url()->current()==url('forgot_password'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Forgot Password
              </a>
            @endif

            @if(str_contains(url()->current(),'password-recovery/'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Recover Password
              </a>
            @endif

            @if(url()->current()==url('register/customer/verify-phone'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Verify Phone
              </a>
            @endif

            @if((url()->current()==url('/register/customer') || str_contains(url()->current(),'/register/customer/')) && url()->current()==url('register/customer/verify-phone'))
              <a href="{{url('/')}}" class="mdl-color-text--white bold">
                <i class="fa fa-arrow-left"> </i> Create your account
              </a>
            @endif
            </div>
          </header>

          <!-- Page Content -->
          <main class="mdl-layout__content login_bg valign-wrapper"> <!-- vertical align container -->
            <div class="valign mdl-color-text--white bold"> <!-- vertical align element -->
              <h2 class="italics"><a href="{{url('/')}}" class="mdl-color-text--white">Charmé</a></h2>
              <span class="mdl-color-text--white bold">beauty and wellness on demand.</span>
            </div>
          </main>
           <!-- header background -->        
              @yield('content')
         
    </div>
    
    <script src="{{URL::asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/3.5.2/js/jquery.animsition.min.js"></script>
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script> 
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>
    <script src="{{URL::asset('js/function.js')}}"></script>
    <!-- New plugins -->
    <script src="{{URL::asset('js/jquery.datetimepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/vegas.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/validate.min.js')}}"></script><!-- Form validator -->
    <script type="text/javascript" src="{{URL::asset('js/login_registerForms.js')}}"></script><!-- Form validator -->
    <script src="https://apis.google.com/js/api:client.js"></script>
    <script type="text/javascript">
      var google_url="{{url('signin/google')}}";
      var facebook_url="{{url('signin/facebook')}}";
      var services_url ="{{url('/services')}}";
    </script>

    @if(url()->current()==url('/register/customer'))
      <script src="{{URL::asset('js/facebook.js')}}"></script>
      <script type="text/javascript" src="{{URL::asset('js/google.js')}}"></script>
    @endif
    @if(url()->current()==url('/signin'))
      <script src="{{URL::asset('js/facebookSignin.js')}}"></script>
      <script type="text/javascript" src="{{URL::asset('js/googleSignin.js')}}"></script>
    @endif


  </body>
</html>
