<?php

namespace App\Http\Middleware;

use Closure;
use Session;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use charmeapi;

class LoggedinMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if (!$request->Session()->get('customer_id') || 
            !$request->Session()->get('customer_token') || 
            !$request->Session()->get('verified') ||
            !$request->Session()->get('customer_name')) {
            return redirect('/signin')->with('error','Kindly login');
        }
        
            $user_id=Session::get('customer_id');            
            $user_details = Controller::charmeapi()->request('GET', 'customers/'.$user_id)->getBody();
            $user_details = json_decode($user_details,false);
            $user_details= $user_details->data->Customer;
            $request->session()->put('Customer',$user_details);
            if (session('social_pic')) {
                $profile_pic = session('Customer')->pic_url;
                if (is_null($profile_pic)) {
                    //array_set(session('Customer'),'pic_url',session('social_pic'));
                    session('Customer')->pic_url=session('social_pic');
                }
            }
            //return $user_details->id;
            return $next($request);
    }
}
