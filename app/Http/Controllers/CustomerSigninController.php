<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class CustomerSigninController extends Controller
{   


    public function welcome(){
    	 return view('login.signin');
    }

    public function tour(){
         return view('modules.tour');
    }

    public function login_request(Request $request){
    	$data = array();
    	$input=$request->all();
    	/*Send username and password to API*/
    	$response = $this->charmeapi()->request('POST', 'customers/auth', ['form_params' => $input])->getBody();
		/*Convert response to Array*/
		$responseBody = (string) $response;
    	$responseBody = json_decode($responseBody, true);
    	/*Save response in variable*/
    	$data =array_add($data,'response', $responseBody);
    	/*Check for response status*/
    	
    	if($data['response']['status']==='ok'){

            if($data['response']['data']['Customer']['verified']==true){

                /*Saves id and token used in middleware for logged-in*/
                Session::put('customer_id',$data['response']['data']['Customer']['id']);
                Session::put('customer_token',$data['response']['data']['Customer']['token']);
                Session::put('customer_name', sprintf('%s %s', Arr::get($data, 'response.data.Customer.first_name'),
                    Arr::get($data, 'response.data.Customer.last_name')));
    			Session::put('verified',true);

                /*Clear registration data*/
                if ($request->session()->has('signup_id')) {            
                $request->session()->forget('signup_id');
                }

                if ($request->session()->has('signup_token')) {            
                $request->session()->forget('signup_token');
                }
	    		return redirect('/services')->with('data',$data); 
	    		/*Response status is Ok and account is verified*/
	    	}

            if($data['response']['data']['Customer']['verified']==false){
            /*Saves id and token used in middleware for unverified accounts*/
                Session::put('signup_id',$data['response']['data']['Customer']['id']);
                Session::put('signup_token',$data['response']['data']['Customer']['token']);
                Session::put('signup_name', sprintf('%s %s', Arr::get($data, 'response.data.Customer.first_name'),
                    Arr::get($data, 'response.data.Customer.last_name')));
                return redirect('register/customer/verify-phone')->with('error','Phone Number Not verified');
            }
            else return view('signin'); 
    	}

    	elseif ($data['response']['status']=='error') {
    		 return redirect('signin')->with('error',$data['response']['error']['msg']); 		
    	}

    }

    public function google_login(request $request){
        //return $request->all();
        $input = array('google_id' => $request->google_id,);
        //return $input;
        if ($request->pic_url!='') {
            session::put('social_pic',$request->pic_url);
        }
        $response = $this->charmeapi()->request('POST', 'customers/auth', ['form_params' => $input])->getBody();
        $response= $this->ArrayResponse($response);
        if($response['status']==='ok'){

            if($response['data']['Customer']['verified']==true){

                /*Saves id and token used in middleware for logged-in*/
                Session::put('customer_id',$response['data']['Customer']['id']);
                Session::put('customer_token',$response['data']['Customer']['token']);
                Session::put('verified',true);
            }
        }
        return $response;
    }

    public function facebook_login(request $request){
        //return $request->all();
        $input = array('facebook_id' => $request->fb_id,);
        //return $input;
        if ($request->pic_url!='') {
            session::put('social_pic',$request->pic_url);
        }
        $response = $this->charmeapi()->request('POST', 'customers/auth', ['form_params' => $input])->getBody();
        $response= $this->ArrayResponse($response);
        if($response['status']==='ok'){

            if($response['data']['Customer']['verified']==true){

                /*Saves id and token used in middleware for logged-in*/
                Session::put('customer_id',$response['data']['Customer']['id']);
                Session::put('customer_token',$response['data']['Customer']['token']);
                Session::put('verified',true);
            }
        }
        return $response;
        
    }

    public function forgot_password(Request $request){
    	return view('login.forgot_password');
    }

    public function reset_password(Request $request){
    	$email=$request->email;
    	//return $email;
    	$response = $this->charmeapi()->request('POST', 'customers/password-recovery', ['form_params' => ['email'=> $email]])->getBody();
    	$response = $this->ArrayResponse($response);
        if ($response['status']=='ok') {           
    	   return redirect('/forgot_password')->with('status','Email Sent');
        }
        else return redirect('/forgot_password')->with('error',$response['error']['msg']);
    }

    public function remember_password(Request $request){
        $reset_token=$request->reset_token;
        if ($request->password !==$request->confirm_password) {
            return redirect("/password-recovery/{$reset_token}")->with('error','Password does not match'); 
        }
        $password_change = array('token' => $reset_token, 'new_password'=>$request->password);
        $response = $this->charmeapi()->request('POST', 'reset-password', ['form_params' =>$password_change])->getBody();
        $response = $this->ArrayResponse($response);
        if ($response['status']=='ok') {           
           return redirect('/signin')->with('status','Password changed');
        }
        else return redirect("/password-recovery/{$reset_token}")->with('error',$response['error']['msg']);
    }

    public function resend_sms(Request $request){
        $id=Session::get('customer_id');
        $token=Session::get('customer_token');
        if ($id=='') {
            $id=session('signup_id');
        }
        if ($token=='') {
            $token=session('signup_token');
        }
        $data = array();
        /*Send verification SMS to user phone*/
        $verify_phone = $this->charmeapi()->request('GET', 'customers/'.$id.'/verify-phone?token='.$token)->getBody();
        $verify_phoneBody = (string) $verify_phone;
        $verify_phoneBody = json_decode($verify_phoneBody, true);
        $data= array_add($data,'verify-phone',$verify_phoneBody);
        //return $data;
            /*Check if verification code was successfully sent*/
            if($data['verify-phone']['status']==='ok' && !empty($data['verify-phone']['status'])){
                return redirect('register/customer/verify-phone')->with('status','SMS sent');
            }
            else
            $sms_error = array('error' => 'SMS verification sending failed.', 'id'=>$id, 'token'=>$token );
             return redirect('register/customer/verify-phone')->with('error','Error sending SMS');
        
    }
}
