<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class AppointmentController extends Controller
{	
	public function show_appointments(){
		$id=session('customer_id'); 
		$token=session('customer_token');
    	$response = $this->charmeapi()->request('GET', "customers/{$id}/appointments?token={$token}")->getBody();
    	$response= $this->ArrayResponse($response);
    	$past = $this->charmeapi()->request('GET', "customers/{$id}/appointments?token={$token}&type=past")->getBody();
    	$past= $this->ArrayResponse($past);
    	//return $past;
    	$response= array_add($response,'past',$past);
    	//return $response;    	
        return view('modules.appointments',['data'=>$response]);

	}


	public function cancel_appointment(request $request){
		$id=$request->id;
		//return $id;
		$token=session('customer_token');
        $cancel = array('token' => $token);
    	$response = $this->charmeapi()->request('POST', "appointments/{$id}/cancel?token={$token}",['form_params' => $cancel])->getBody();
        $response= $this->ArrayResponse($response);
    	//return $response;
    	if ($response['status']=='ok') {
    		return redirect('appointments')->with('status','Appointment has been cancelled');
    	}
    	if ($response['status']=='error') {
    		return redirect('appointments')->with('error','Appointment was not cancel');
    	}
	}

    public function reschedule_appointment(request $request){
    	$id=$request->id;
		$token=session('customer_token');
        $appointment_time=$request->appointment_time;
        //dump($request->all());
        $appointment_time= date_format(date_create($appointment_time),"Y-m-d H:i");
        //return $appointment_time;
        $reschedule = array('token' => $token,'start_at'=>$appointment_time);
    	$response = $this->charmeapi()->request('POST', "appointments/{$id}/reschedule?token={$token}",['form_params' => $reschedule])->getBody();
        $response= $this->ArrayResponse($response);
    	//return $response;
    	if ($response['status']=='ok') {
    		return redirect('appointments')->with('status','Appointment Successfully Rescheduled');
    	}
    	if ($response['status']=='error') {
    		return redirect('appointments')->with('error','Appointment reschedule failed');
    	}
    }

    public function confirm_service($id) {
            return view('modules.feedback_confirmed',['id'=>$id]);
    }

    public function send_end(request $request) {
        $id=session('customer_id');
        $comment=$request->comment;
        $appointment_id=$request->id;
        //return $comment;
        if ($comment=='') {
            $comment='ended';
        }
        if ($request->rating=='') {
            return redirect("appointments/completed/{$appointment_id}")->with('error','Kindly Rate Professional');
        }
        $token=session('customer_token');
        $appointment_id=$request->id;
        $end = array('id' => $request->id, 
            'comment'=> $comment,
            'rating'=>$request->rating,
            'token'=>$token,
            );
        $response = $this->charmeapi()->request('POST', "appointments/{$appointment_id}/rate?token={$token}",['form_params' => $end])->getBody();
        $response= $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {
            return redirect('/appointments')->with('status', 'Appointment ended, Thanks for choosing Charme');
        }
        if ($response['status']=='error') {
            return redirect('/appointments')->with('error', 'Something went wrong');        
        }
        else return redirect('/appointments');
    }


    
	public function show_chat(request $request){
		$appointment_id = $request->id;
		/*Save the appointment_id so it can be used to post messages*/
		session::put('appointment_id',$appointment_id);
		//return $appointment_id;
		$id=session('customer_id');
		$token=session('customer_token');

    	$response = $this->charmeapi()->request('GET', "appointments/{$appointment_id}/conversations?token={$token}")->getBody();
    	$response= $this->ArrayResponse($response);
    	//return $response;
    	if ($response['status']=='ok') {
			return view('modules.chat',['messages'=>$response['data']]);    		
    	}
    	elseif ($response['status']=='error') {
			return redirect('/appointments')->with('error',$response['error']['msg']);
    	}
    	else return redirect('/apoointments');
	}

    public function show_location(request $request){
        $appointment_id = $request->id;
        /*Save the appointment_id so it can be used to post messages*/
        session::put('appointment_id',$appointment_id);
        //return $appointment_id;
        $id=session('customer_id');
        $token=session('customer_token');

        $response = $this->charmeapi()->request('GET', "appointments/{$appointment_id}?token={$token}")->getBody();
        $response= $this->ArrayResponse($response);
        //return $response;
        if ($response['status']=='ok') {
            return view('modules.supplier_location',['data'=>$response['data']]);
                        
        }
        elseif ($response['status']=='error') {
            return redirect('/appointments')->with('error',$response['error']['msg']);
        }
        else return redirect('/apoointments');
    }
	public function send_chat(request $request){
		/*Get appointment_id and message*/
		$id=session('appointment_id');
		$message= $request->message;
		$token=session('customer_token');
		$sendchat = array('message' => $message,'token'=>$token);
		$response = $this->charmeapi()->request('POST', "appointments/{$id}/conversations?token={$token}",['form_params' => $sendchat])->getBody();
        $response= $this->ArrayResponse($response);
		return $response;
	}
}
