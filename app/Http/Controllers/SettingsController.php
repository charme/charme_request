<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class SettingsController extends Controller
{
    public function change_password(){    	
       return view('modules.change_password');
    }

    public function send_change_password(request $request){
    	if ($request->new_password !== $request->confirm_new_password) {
    		return redirect('/settings/password')->with('error','Password does not match');
    	}
    	/*Generate array to post*/
    	$password = array('old_password' =>$request->old_password ,'new_password'=> $request->new_password);
    	/*Get customer token and id*/
    	$customer_id=Session::get('customer_id');    	
        $customer_token=Session::get('customer_token');        
 		$response = $this->charmeapi()->request('POST', "customers/{$customer_id}/password?token={$customer_token}",["form_params"=>$password])->getBody();
 		$response = $this->ArrayResponse($response);
    	//return $response;
    	if ($response['status']=='ok') {
    		return redirect('/settings/password')->with('status','Password Successfully Changed');
    	}

    	if ($response['status']=='error') {
    		return redirect('/settings/password')->with('error',$response['error']['msg']);
    	}
    }
}
