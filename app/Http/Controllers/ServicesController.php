<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Carbon\Carbon;

    
class ServicesController extends Controller
{

    public function get_categories(){
        $response = $this->charmeapi()->request('GET', 'categories')->getBody();
        $responseBody = $this->ObjectResponse($response);
        return view('modules.services', ['response'=>$responseBody]);
    }

    public function get_services($id=1){ 
        
        Session::put('cat_id',$id);

        $response = $this->charmeapi()->request('GET', "categories/".$id)->getBody();
        $category_details = $this->ArrayResponse($response);
        $category_name = $category_details['data']['Category']['name'];
    	$response = $this->charmeapi()->request('GET', "categories/".$id."/services")->getBody();
		$responseBody = $this->ArrayResponse($response); 
		$responseBody = array_add($responseBody,'category_name',$category_name);
    	return view('modules.service_style', ['response'=>$responseBody]);
    }

    public function get_professionals(request $request) {
        //return $request->all();
        $service_id= $request->service_id;
        if ($service_id=='') {
            $service_id=session('service_id');
        }
        if (is_null($service_id)) {
            return redirect('services');
        }
        $appointment_time=$request->appointment_time;
        if ($appointment_time=='') {
            if (!empty(session('appointment_time'))) {
                $appointment_time=Session::get('appointment_time');
            }
            else $appointment_time=Carbon::now()->toDateTimeString();
        }
        $appointment_time= date_format(date_create($appointment_time),"Y-m-d H:i");
        Session::put('appointment_time',$appointment_time);        
        $user_details = $this->UserDetails();
        Session::put('service_id',$service_id);
        $response = $this->charmeapi()->request('GET', "services/{$service_id}/suppliers")->getBody();
        $available_professionals= $this->ArrayResponse($response);
        //return $available_professionals;
        
            return view('modules.service_professionals',[
                'available_professionals'=>$available_professionals['data']['Suppliers'],
                'user_details'=>$user_details,'page'=>$available_professionals['data']['Meta']]);
        }

    public function more_professionals(request $request){
        $url= $request->page;
        $response = $this->charmeapi()->request('GET', $url)->getBody();
        $available_professionals= $this->ArrayResponse($response);
        //return $available_professionals;

        return view('modules.more_professionals',[
            'available_professionals'=>$available_professionals['data']['Suppliers'],
            'page'=>$available_professionals['data']['Meta']]);
    }


    public function get_professional_details($id=null){
        if ($id!=null) {
        $professional_id=$id;
        }
        else {
            $professional_id=Session::get('app_supplier_id');
        }
        
        //return $professional_id;
        $service_id=Session::get('service_id');
        //return $service_id;

        /*Get Service Details*/
        $appointment_time=Session::get('appointment_time');
        /*Convert Date time string*/
        $date=date_create($appointment_time);
        $appointment_time =date_format($date,"jS M, g:i A");        
        //return $appointment_time;

        $service_details = $this->charmeapi()->request('GET', "services/{$service_id}")->getBody();
        $service_details= $this->ArrayResponse($service_details);
        $service_details=$service_details['data']['Service'];
        $service_details=array_add($service_details,'appointment_time',$appointment_time);
        //return $service_details;

        /*Get Category Details*/
        $category_id=$service_details['category_id'];
        $category_details = $this->charmeapi()->request('GET', "categories/{$category_id}")->getBody();
        $category_details = $this->ArrayResponse($category_details);
        $category_details = $category_details['data']['Category'];
        //return $category_details;

        /*Get Professional's details*/
        $response = $this->charmeapi()->request('GET', "suppliers/{$professional_id}")->getBody();
        $professional= $this->ArrayResponse($response);
        $professional_details=$professional['data']['Supplier'];
        session::put('app_supplier_id',$professional_id);
        //return $professional_details;

        /*Add Service Details, category details and professional's details together*/
        $data = array();
        $data=array_add($data,'service_details',$service_details);
        $data=array_add($data,'category_details',$category_details);
        $data=array_add($data,'professional_details',$professional_details);
        //return $data;

        return view('modules.service_professional_profile',['data'=>$data]);
    }
    /*Ok, this function has been replaced in save_details*/
    public function get_instructions(){
        return view('modules.instructions');
    }
    /*Ok, this function has been replaced in save_details*/
    public function save_instructions(request $request){
        $instructions = $request->instructions;
        session::put('instructions',$instructions);
        return redirect('services/location');
    }

    /*Returns view to add location and extra details*/
    public function get_location(request $request){
        //return $request->session()->all();
        $professional_id=session('app_supplier_id');
        $response = $this->charmeapi()->request('GET', "suppliers/{$professional_id}")->getBody();
        $professional= $this->ArrayResponse($response);
        $professional_details=$professional['data']['Supplier'];
        //return $professional_details;
        $data = array('Supplier' => $professional_details);
        //return $data;
        return view('modules.location',['data'=>$data]);
    }
    /*Saves details and loads summary view where coupon can be added*/
    public function save_details(request $request){
        //return $request->all();
        $instructions = $request->instructions;
        /*save instructions*/
        session::put('instructions',$instructions);
        /*save latlong*/
        $latlong = $request->location;
        session::put('latlong',$latlong);
        if (!empty($request->address_details)) {
        $address_details=$request->address.'('.$request->address_details.')';
        }
        else $address_details=$request->address;
        session::put('address_details',$address_details);            
        $service_id=session('service_id');
        $service_details = $this->charmeapi()->request('GET', "services/{$service_id}")->getBody();
        $service_details= $this->ArrayResponse($service_details);
        $service_details=$service_details['data']['Service'];
        //return $request->session()->all();
        return view('sub_view.payment_summary',['service_details'=> $service_details]);
    }

    /*Check if coupon entered is valid*/
    public function check_coupon(request $request){
        $coupon_code= $request->coupon_code;
        $token=session('customer_token');
        $coupon_response = $this->charmeapi()->request('GET', "coupons/verify?code={$coupon_code}&token={$token}")->getBody();
        $coupon_response= $this->ArrayResponse($coupon_response);
        return $coupon_response;
    }
    /*Save request and post to get payment gateway details*/
    public function save_request(request $request){
        /*Retrieve the coupon and check if it's valid*/
        $coupon=$request->coupon;
        $token=session('customer_token');
        $coupon_response = $this->charmeapi()->request('GET', "coupons/verify?code={$coupon}&token={$token}")->getBody();
        $coupon_response= $this->ArrayResponse($coupon_response);

        //return $coupon;
        if ($coupon_response['status']=='ok') {
            $coupon=$coupon_response['data']['Coupon']['code'];
        }
        else{
            $coupon=" ";
        }
        /*Get previous service details and store in an array to post*/
        $service_request=array(
            'customer_id'=>session('customer_id'),
            'supplier_id'=>(int) session('app_supplier_id'),
            'service_id'=>(int) session('service_id'),
            'start_at'=>session('appointment_time'),
            'additional_message'=>session('instructions'),
            /*if you notice, I'm adding the meetup_address afterwards*/
            'coupon'=>$coupon,
            'token'=>$token,
            );
        /*Add address details with address*/
        $address_details=session('address_details');
        /*Get LatLong*/
        $latlong = session('latlong');
        
       
        $latlong = trim($latlong,'(');
        $latlong = trim($latlong,')');
        $location_split= explode(",", $latlong);
        $lat=(float) trim($location_split[0]);
        $long=(float) $location_split[1];

        $meetup_address = array('latitude' => $lat,'longitude'=>$long, 'address_details'=> $address_details);
        /*Here, I add meet-up address details to the request to send*/
        $service_request = array_add($service_request,'meetup_address',json_encode($meetup_address));
        //return $service_request;
        $response = $this->charmeapi()->request('POST', "requests",['form_params' => $service_request])->getBody();
        $response = $this->ArrayResponse($response);
        $response['data'] = array_add($response['data'], 'Customer', ['id' => Session::get('customer_id'), 'name' => Session::get('customer_name')]);
        return $response;
    }

    /*Produces view after successful payment*/
    public function post_payment(request $request){
            //return $request->response;
            $response=$request->response;
            $response= $this->ArrayResponse($response);
            //return $response['status'];
            return view('modules.payment',['payment'=>$response]);
    }
}
