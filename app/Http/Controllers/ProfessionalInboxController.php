<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
class ProfessionalInboxController extends Controller
{
    
    public function get_inbox(){
    	$supplier_id=Session::get('supplier_id');
    	$token=session('supplier_token'); 
 		$response = $this->charmeapi()->request('GET', "suppliers/{$supplier_id}/inbox?token={$token}")->getBody();
 		$response = $this->ArrayResponse($response);
 		//return $response;
 		if ($response['status']=='ok') {
 			//print_r($response);
            return view('professional_modules.inbox',['inbox'=> $response['data']['Inboxes']]); 			
 		}
 		 else return view('professional_modules.inbox');

    }
}
